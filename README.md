# DOE JGI guideRNA Sequence Extraction Tool (gRNA-SeqRET)

This guideRNA design tool was developed at the DOE Joint Genome Institute to facilitate whole genome gRNA design.
            It utilizes the <a href="https://crispr.cos.uni-heidelberg.de/">CRISPR/Cas9 Target online predictor (CCTop)</a>
            writen by Juan L. Mateo, at the Center for Organismal Studies (COS) Heidelberg, supported by the European Research Council.
            
We use the open source (MIT license) stand alone version of
                <a href="https://bitbucket.org/juanlmateo/cctop_standalone/src/master/">CCTop</a> to find predicted guide RNA sites and offsite targets.
                The value added by DOE JGI gRNA-SeqRET, is the ability to use your genome and
                filter out the best gRNA sequences based on the CCTop scoring as well as the folding predictions of the gRNA and it's scaffold sequence.
                It is not limited to a select group of pre-configured organisms, though in this initial release,
                you are limited to prokaryotic organisms with a single chromosome that has a Genbank file with the annotated genome sequence available.
                Our tool also allows you to enter a list of gene names, or use every gene in the annotated genome,
                making it a whole genome guideRNA design tool. If you have technical questions please contact Lisa Simirenko lsimirenko@lbl.gov.
                
             
                
               
                       

gRNA-SeqRET Copyright (c) 2020, The Regents of the University of
California, through Lawrence Berkeley National Laboratory (subject
to receipt of any required approvals from the U.S. Dept. of Energy).
All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Intellectual Property Office at
IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the U.S. Department
of Energy and the U.S. Government consequently retains certain rights.  As
such, the U.S. Government has been granted for itself and others acting on
its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
Software to reproduce, distribute copies to the public, prepare derivative
works, and perform publicly and display publicly, and to permit others to do so.