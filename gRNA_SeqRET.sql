--
-- PostgreSQL database dump
--

-- Dumped from database version 10.17
-- Dumped by pg_dump version 10.17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: tbl_apps; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tbl_apps (
    app_id bigint NOT NULL,
    app_name character varying NOT NULL
);


--
-- Name: tbl_apps_app_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tbl_apps_app_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tbl_apps_app_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tbl_apps_app_id_seq OWNED BY public.tbl_apps.app_id;


--
-- Data for Name: tbl_apps; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.tbl_apps (app_id, app_name) FROM stdin;
1       BLiSS
2       BOOST
3       gRNADesigner
4       SynTrack
5       ORCA
7       SynBioTools
\.


--
-- Name: tbl_apps_app_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.tbl_apps_app_id_seq', 7, true);


--
-- Name: tbl_apps tbl_apps_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_apps
    ADD CONSTRAINT tbl_apps_pkey PRIMARY KEY (app_id);

--
-- Name: tbl_designs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tbl_designs (
    design_id integer NOT NULL,
    is_public boolean DEFAULT true NOT NULL,
    user_id bigint NOT NULL,
    job_id bigint NOT NULL,
    edit_date timestamp without time zone,
    job_uuid character varying(50) NOT NULL,
    genome_uuid character varying(50) NOT NULL,
    design_name character varying(150),
    phylum character varying(150),
    genus character varying(150),
    species character varying(150),
    genotype character varying(150),
    taxon_id character varying(50),
    crispr_application character varying(255),
    description character varying(255)
);


--
-- Name: tbl_designs_design_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tbl_designs_design_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tbl_designs_design_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tbl_designs_design_id_seq OWNED BY public.tbl_designs.design_id;


--
-- Name: tbl_genomes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tbl_genomes (
    genome_id integer NOT NULL,
    user_id bigint NOT NULL,
    genome_uuid character varying(255) NOT NULL,
    genome_userdefined_id character varying(255),
    genome_status character varying(255),
    annotation_file_type character varying(25),
    annotation_file_name character varying(255),
    saved_date timestamp without time zone DEFAULT now() NOT NULL,
    genome_type character varying(255)
);


--
-- Name: tbl_genomes_genome_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tbl_genomes_genome_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tbl_genomes_genome_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tbl_genomes_genome_id_seq OWNED BY public.tbl_genomes.genome_id;


--
-- Name: tbl_user_job_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tbl_user_job_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tbl_user_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tbl_user_jobs (
    job_id integer DEFAULT nextval('public.tbl_user_job_id_seq'::regclass) NOT NULL,
    job_uuid character varying(255) NOT NULL,
    job_userdefined_id character varying(255),
    job_request_json_filename character varying(255),
    job_submission_date timestamp without time zone,
    job_end_date timestamp without time zone,
    user_id bigint NOT NULL,
    app_id integer NOT NULL,
    job_type character varying(255),
    job_status character varying(255),
    is_public boolean DEFAULT false NOT NULL
);


--
-- Name: tbl_user_login_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tbl_user_login_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tbl_user_logins; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tbl_user_logins (
    login_id bigint DEFAULT nextval('public.tbl_user_login_id_seq'::regclass) NOT NULL,
    login_time timestamp without time zone,
    user_id bigint,
    app_id integer NOT NULL
);


--
-- Name: tbl_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tbl_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tbl_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tbl_users (
    user_id integer DEFAULT nextval('public.tbl_users_id_seq'::regclass) NOT NULL,
    iswhitelisted boolean,
    username character varying(255) NOT NULL,
    password character varying(100),
    email character varying(100)
);


--
-- Name: tbl_apps app_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_apps ALTER COLUMN app_id SET DEFAULT nextval('public.tbl_apps_app_id_seq'::regclass);


--
-- Name: tbl_designs design_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_designs ALTER COLUMN design_id SET DEFAULT nextval('public.tbl_designs_design_id_seq'::regclass);


--
-- Name: tbl_genomes genome_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_genomes ALTER COLUMN genome_id SET DEFAULT nextval('public.tbl_genomes_genome_id_seq'::regclass);


--
-- Name: tbl_apps tbl_apps_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_apps
    ADD CONSTRAINT tbl_apps_pkey PRIMARY KEY (app_id);


--
-- Name: tbl_user_jobs tbl_user_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_user_jobs
    ADD CONSTRAINT tbl_user_jobs_pkey PRIMARY KEY (job_id);


--
-- Name: tbl_user_logins tbl_user_logins_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_user_logins
    ADD CONSTRAINT tbl_user_logins_pkey PRIMARY KEY (login_id);


--
-- Name: tbl_users tbl_users_email_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_users
    ADD CONSTRAINT tbl_users_email_key UNIQUE (email);


--
-- Name: tbl_users tbl_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_users
    ADD CONSTRAINT tbl_users_pkey PRIMARY KEY (user_id);


--
-- Name: tbl_user_jobs fk_tbl_user_jobs_app_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_user_jobs
    ADD CONSTRAINT fk_tbl_user_jobs_app_id FOREIGN KEY (app_id) REFERENCES public.tbl_apps(app_id) NOT VALID;


--
-- Name: tbl_user_jobs fk_tbl_user_jobs_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_user_jobs
    ADD CONSTRAINT fk_tbl_user_jobs_user_id FOREIGN KEY (user_id) REFERENCES public.tbl_users(user_id);


--
-- Name: tbl_user_logins fk_tbl_user_logins_app_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_user_logins
    ADD CONSTRAINT fk_tbl_user_logins_app_id FOREIGN KEY (app_id) REFERENCES public.tbl_apps(app_id) NOT VALID;


--
-- Name: tbl_user_logins fk_tbl_user_logins_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tbl_user_logins
    ADD CONSTRAINT fk_tbl_user_logins_user_id FOREIGN KEY (user_id) REFERENCES public.tbl_users(user_id);


--
-- PostgreSQL database dump complete
--
