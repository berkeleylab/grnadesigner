import sys, psycopg2, json
from datetime import datetime


def run(uuid, dbname, dbhost, dbuser, dbpass, project_json, message, status, project_name):
    '''
    create a database connection
    get job row using uuid
    update it with the status and current time as end time
    also- update jason with status and message
    :param uuid:
    :param dbname:
    :param dbhost:
    :param dbuser:
    :param dbpass:
    :return:
    '''
    # Connect to database
    conn = psycopg2.connect(dbname=dbname, user=dbuser, host=dbhost, password=dbpass)

    # Open a cursor to perform database operations
    cur = conn.cursor()

    current_date = datetime.now()

    # update row
    cur.execute("""UPDATE tbl_user_jobs SET job_status=%s, job_end_date=%s WHERE job_uuid=%s;""",("Complete", current_date, uuid ))

    conn.commit()
    # close cur and
    cur.close()
    conn.close()

    # now update json
    with open(project_json) as f:
        data = json.load(f)

    data['status']=status
    data['message']=message
    # delete database credentials
    del data["DB_NAME"]
    del data["DB_PASS"]
    del data["DB_USER"]
    del data["DB_HOST"]

    # now overwrite the old file with the new json
    with open(project_name + "_final.json", 'w') as json_file:
        json.dump(data, json_file)






def main():
    '''
    this script takes json file and updates the synbio _apps database job row with the end time and status
    :return:
    '''
    if len(sys.argv) < 10:
        print(
            "Usage: python update_synbio_app_database.py uuid dbname dbhost dbuser dbpass project_json message status uuid")
    else:
        uuid = sys.argv[1]
        dbname = sys.argv[2]
        dbhost = sys.argv[3]
        dbuser = sys.argv[4]
        dbpass = sys.argv[5]
        project_json = sys.argv[6]
        message = sys.argv[7]
        status = sys.argv[8]
        uuid = sys.argv[9]

        run(uuid, dbname, dbhost, dbuser, dbpass, project_json, message, status, uuid)
        # print("done!")


if __name__ == "__main__":
    main()