import sqlite3
import gffutils
import json, os, sys
from Bio import SeqIO

def get_gffutils_db(db_file):
    '''return the db from gffutils'''
    db = gffutils.FeatureDB(db_file, keep_order=True)
    return db


def get_sqlite_db(db_file):
    ''' returns db connection object'''
    # get sqlite connection and cursor
    conn = sqlite3.connect(db_file)
    cur = conn.cursor()
    return cur


def get_strand_ch(gene, sqldb):
    '''return the strand and chomosome for the gene'''
    # first get the strand and chromosome that the gene is on
    print("getting strand for gene: "+gene )
    sqldb.execute("select strand, seqid from features where id=:gene", {"gene": gene})
    row = sqldb.fetchone()
    # TODO put in try/except in case there are errors in the gene names...though these should be vetted by the ui first
    if row[0]:
        strand = row[0]
    else:
        strand= "+"
        print("ERROR: no strand!")
    ch_name = row[1]
    return strand, ch_name


def get_feature_id(gene, ref_feature, strand, gffdb):
    # get reference feature particulars
    parts = ref_feature.split("_")
    feature_type = parts[0]
    if feature_type == 'gene':
        # special case, will only have 2
        s_or_e = parts[1]
        feature_id = gene
    else:
        s_or_e = parts[2]
        f_or_l = parts[1]
        # use gffutil db to get the id of the feature
        features = [f.id for f in gffdb.children(gene, featuretype=feature_type)]
        # the one we want is first or last
        # these should be in order on the "+" strand, opposite for "-" strand
        if len(features) > 0:
            if (f_or_l == "F" and strand == "+") or (f_or_l == "L" and strand == "-"):
                # get the first one
                feature_id = features[0]
            else:
                # (f_or_l == "L" and strand == "+") or (f_or_l == "F" and strand == "-"):
                # get last one
                features.reverse()
                feature_id = features[0]
        else:
            # if the features is an empty list then just use the gene itself
            # this will be the case when there are not exons or CDS for this gene
            feature_id = gene

    return feature_id


def get_feature_boundaries(feature_id, sqldb):
    sqldb.execute("select start, end from features where id=:feature", {"feature":feature_id})
    row = sqldb.fetchone()
    feature_start = row[0]
    feature_end = row[1]
    return feature_start, feature_end


def find_non_coding_start(end_point, ch_name, sqldb):
    # TODO - I am assuming that the annotation will have at least one of these featuretypes to indicate coding regions: gene, CDS, exon
    # given an end point, on a chromosome, find the next point just before another coding feature
    # in the - direction
    # this query will get the next CDS, exon or gene feature type end, on the same chromosome
    # in the plus direction - it doesn't matter the strand these are always from left to right
    # we add +1 so as not to be in the coding region
    print("end_point: "+str(end_point)+", ch: "+ch_name)
    sqldb.execute(
        "select end from features where end < :end_point and seqid = :ch_name and featuretype in ('CDS', 'exon', 'gene') order by end desc", {"end_point":end_point,"ch_name":ch_name})
    r = sqldb.fetchone()
    if r is None:
        start = 0
    else:
        start = r[0]
    print("got boundary start")
    if start is None or start < 0:
        print("boundary start is none or zero")
        # set to first base of chromosome in gff 1-based coordinate system
        start = 1
    else:
        print("boundry start is greater than zero")
        # add one base so as to not be in the coding region
        start = start + 1
    return start


def find_non_coding_end(start_point, ch_name, ch_length, sqldb):
    # TODO - I am assuming that the annotation will have at least one of these featuretypes to indicate coding regions: gene, CDS, exon
    # given an start point, on a chromosome, find the next point just before another coding feature
    # in the + direction
    # this query will get the next CDS, exon or gene feature type start, on the same chromosome
    # in the plus direction - it doesn't matter the strand these are always from left to right
    # we subtract 1 so as not to be in the coding region
    sqldb.execute(
        "select start from features where start > :start_point and seqid = :ch_name and featuretype in ('CDS', 'exon', 'gene') order by end ASC",  {"start_point":start_point,"ch_name":ch_name})
    r = sqldb.fetchone()
    end = r[0]
    if end is None or end > ch_length:
        # set to ch_length if not found or too far
        end = ch_length
    else:
        # subtract 1 so as not to be in the coding region
        end = end -1
    return end


def get_limit_boundaries(gene, start_feature, limit, strand, gffdb, ch_length, ch_name, sqldb):
    ''' based on the feature definition and feature attributes (strand, start and end)
     figure out a boundary of any extracted region'''
    print("checking boundaries")
    coding_features= ['CDS_F_0', 'CDS_F_$', 'CDS_L_0', 'CDS_L_$', 'exon_F_0', 'exon_F_$', 'exon_L_0', 'exon_L_$']
    if limit == "coding":
        print("limit to coding only")
        # the person only wants coding regions so if the start feature
        # is the first or last exon or CDS then only that exon or CDS can be used
        # if the it is the gene- must first see if there are CDS/exons,
        # and use either the first or last as indicated by gene_0 or gene_$, or use whole gene boundrie (prokaryote)
        if start_feature in coding_features:
            # the boundaries are of the feature itself
            f_id = get_feature_id(gene, start_feature,strand,gffdb)
            return get_feature_boundaries(f_id, sqldb)
        elif start_feature.startswith("gene"):
            # check if prokaryote or eukaryote:
            # This really just tells if has exons and CDS annotations or just gene annotations
            eukaryote = False
            feature_types =[]
            sqldb.execute("SELECT distinct(featuretype) from features")
            rows = sqldb.fetchall()
            for r in rows:
                feature_types.append(r)
                if r == "exon" or r == "CDS":
                    eukaryote = True

            if eukaryote:
                # start or end of gene?
                if start_feature[len(start_feature)] == "0":
                    # want start so first exon or CDS
                    if "CDS" in feature_types:
                        # get first CDS boundaries
                        f_id = get_feature_id(gene, "CDS_F_0", strand, gffdb)
                    else:
                        #get first exon boundaries
                        f_id = get_feature_id(gene, "exon_F_0", strand, gffdb)
                else:
                    # want end
                    if "CDS" in feature_types:
                        # get last CDS boundaries
                        f_id = get_feature_id(gene, "CDS_L_0", strand, gffdb)
                    else:
                        # get last exon boundaries
                        f_id = get_feature_id(gene, "exon_L_0", strand, gffdb)
                # now return start and end of feature
                return get_feature_boundaries(f_id, sqldb)

            else:
                #return the start and end of gene
                f_id = get_feature_id(gene, start_feature, strand, gffdb)
                return get_feature_boundaries(f_id, sqldb)
        else:
            # edge case, the start_feature is not coding?
            # this shouldn't happen - controlled by the UI
            # but if it does happen, print the error and reurn the start and end of the feature anyway
            print("ERROR: coding start_feature? "+start_feature)
            f_id = get_feature_id(gene, start_feature, strand, gffdb)
            return get_feature_boundaries(f_id, sqldb)

    elif limit == "non-coding":
        print("limit to non-code only")
        f_id = get_feature_id(gene, start_feature, strand, gffdb)
        f_start, f_end = get_feature_boundaries(f_id, sqldb)
        # if the user wants non-coding regions only, and then this is more complicated
        # if the start feature is the start of a gene then this limits you to
        # the region between the gene start and the previous gene
        # or more generalized if it is the start of any feature, it is between this feature and the next upstream feature (CDS, exon or gene)
        if start_feature in ["gene_0", "CDS_F_0", "exon_F_0", "CDS_L_0", "exon_L_0"]:
            print("must find previous gene or coding region...")
            # must find previous gene
            if strand == "+":
                boundary_end = f_start
                # find non-coding start
                try:
                    boundary_start = find_non_coding_start(boundary_end, ch_name, sqldb)
                except Exception as e:
                    print("ERROR(plus strand): "+str(e))

            else:
                boundary_start = f_end
                #find non-coding end
                try:
                    boundary_end = find_non_coding_end(boundary_start, ch_name, ch_length, sqldb)
                except Exception as e:
                    print("ERROR(minus strand): "+str(e))

        elif start_feature in ["gene_$", "CDS_L_$", "exon_L_$", "CDS_F_$", "exon_F_$"]:
            print("must find next gene or coding region...")
            #  this will be the region from the end of this gene/feature to the next gene
            # or mor generalized
            # if the starting reference point is at the end of a feature, the region is between that point and the next
            # downstream coding feature (CDS, exon, or gene)
            if strand == "+":
                boundary_start = f_end
                #find non-coding end
                try:
                    boundary_end = find_non_coding_end(boundary_start, ch_name, sqldb)
                except Exception as e:
                    print("ERROR(plus strand): "+str(e))
            else:
                boundary_end = f_start
                #find non-coding start
                try:
                    boundary_start = find_non_coding_start(boundary_end, ch_name, ch_length, sqldb)
                except Exception as e:
                    print("ERROR(minus strand): "+str(e))

        else:
            # some kind of default...???
            print("ERROR: non-coding start_feature? " + start_feature)
            f_id = get_feature_id(gene, start_feature, strand, gffdb)
            return get_feature_boundaries(f_id, sqldb)
        return boundary_start, boundary_end
    else:
        #limit == "none":
        print("there is no limit")
        # should probably return something meaningful, though this shouldn't be called if this is none
        # maybe entire chromosome?
        #TODO fix this? - should be the region selected?
        return 0, ch_length




def find_point(gene, ref_feature, padding, gffdb, sqldb, strand):
    ''' The ref_feature is from the fron end, it is going to be something like: gene_0, gene_$, exon_F_0, exon_L_$...
    where so you can split the feature on underscores, and get the feature type, first or last and start or stop
    example: gene_0 is the start point of the gene, exon_F_0 is the start of the first exon, and CDS_L_$ is the end of the last CDS

    will also need to know which chromosome it is on...
    '''
    print("getting point for: "+gene+", "+ref_feature+", "+str(padding)+", strand = "+strand)
    # get feature id
    feature_id = get_feature_id(gene, ref_feature,strand,gffdb)
    print("feature id: "+feature_id)
    # now get start, end and strand of the feature:
    feature_start, feature_end = get_feature_boundaries(feature_id, sqldb)
    print("feature start, end: "+str(feature_start)+", "+str(feature_end))
    # set ref_point, if plus strand: s_or_e = "0" its the start, if s_or_e = "$" its the end
    # else the opposite
    # also set the start and end
    s_or_e = ref_feature[len(ref_feature) - 1]
    if strand == "+" and s_or_e == "0":
        ref_point = feature_start
    elif strand == "+" and s_or_e == "$":
        ref_point = feature_end
    elif strand == "-" and s_or_e == "0":
        ref_point = feature_end
    else:
        #strand == "-" and s_or_e == "$":
        ref_point = feature_start

    # now get the point - this is the ref_point plus the padding if it is on the "+" strand
    # if on the "-" strand then it is minus the padding
    if strand == "+":
        point = ref_point + padding
    else:
        point = ref_point - padding
    print("point found: "+str(point))
    return point


def get_region_seq(gene, start_feature, start_padding, end_feature, end_padding, gffdb, sqldb, genome_dict, limit, log):
    ''' get the region sequence and return the sequence'''
    #first get strand and chromosome for the gene
    strand, ch_name = get_strand_ch(gene, sqldb)
    log.write("Chromosome/scaffold: "+ ch_name+"\n")
    log.write("Strand: "+strand+"\n")

    # find start point
    s_point = find_point(gene, start_feature, start_padding, gffdb, sqldb, strand)
    # find end point
    e_point = find_point(gene, end_feature, end_padding, gffdb, sqldb, strand)

    # set region_start and region_end points
    # since GFF is 1 based and python is 0 based, subtract 1 from each start and end point
    if strand == "+":
        region_start = s_point - 1
        region_end = e_point - 1
    else:
        region_start = e_point - 1
        region_end = s_point - 1
    print("region start = "+str(region_start))
    print("region end = "+str(region_end))
    log.write("in zero base coordinates:\n")
    log.write("region start = " + str(region_start)+"\n")
    log.write("region end = " + str(region_end)+"\n")

    print("will test boundaries and edge cases next")
    ch_length = len(genome_dict[ch_name].seq)

    # TODO - check that the region complies with "limit" desires...coding/non coding
    print("limit: "+limit)
    log.write("testing boundaries for limit: " + limit + "\n")
    boundary_start, boundary_end = get_limit_boundaries(gene, start_feature, limit, strand, gffdb, ch_length, ch_name, sqldb)
    # make boundaries same 0-base counting as python
    boundary_start = boundary_start -1
    boundary_end = boundary_end -1
    print("boundary_start = "+str(boundary_start))
    print("boundary_end = "+str(boundary_end))
    log.write("boundary start = "+str(boundary_start)+"\n")
    log.write("boundary end = " + str(boundary_end) + "\n")
    # compare start and end points to the limitation boundary
    if region_start < boundary_start:
        print(gene + ": region start < limit boundary was adjusted from "+str(region_start)+" to "+str(boundary_start))
        log.write("region start < limit boundary was adjusted from "+str(region_start)+" to "+str(boundary_start)+"\n")
        region_start = boundary_start
    if region_end > boundary_end:
        print(gene + ": region end > than  than limit boundary was adjusted from "+str(region_end)+" to "+str(boundary_end))
        log.write("region end > than  than limit boundary was adjusted from "+str(region_end)+" to "+str(boundary_end)+"\n")
        region_end = boundary_end
    print("coordinates: chromosome = "+ch_name+", start = "+str(region_start)+", end = "+str(region_end))
    log.write(
        "coordinates: chromosome = " + ch_name + ", start = " + str(region_start) + ", end = " + str(
            region_end) + "\n")
    # since python slices do not include the end point
    # add 1 to end point so that it is included always
    region_end +=1
    # this means that if the ref feature is the start of a gene and the end padding = 0, the "A" of ATG will be included

    # TODO check for edge cases:
    # what is region start is < 0?
    if region_start < 0:
        # TODO- create log file for these things to be reported
        print("ERROR: the region for gene " + gene + " start was less than zero: " + str(
            region_start) + ", region end: " + str(region_end))
        log.write("Edge case: start was less than zero")
        region_start = 0
    # What if region end is greater than the length of the chromosome?
    if region_end > ch_length:
        print("ERROR: the region for gene " + gene + " end was greater than the chomosome length: " + str(
            ch_length) + ", region start: " + str(region_start))
        log.write("Edge case: end was greater than the length of the chromosome")
        region_end = ch_length
    # get sequence region
    try:
        seq = genome_dict[ch_name].seq[region_start:region_end]
        log.write("final sequence length: " + str(len(seq)) + "\n")
        log.write("final sequence: " + str(seq)+"\n")
    except Exception as e:
        print("error: " + str(e))
        seq = "exception..."
    return str(seq)



def process_genes(log_file, new_fasta, genome_fasta, gffdb_file, start_feature, start_padding, end_feature, end_padding, limit, names):
    ''' generate the fasta file with desired sequence regions for genes/loci '''
    # first load up database and get db cursor
    sql_cursor= get_sqlite_db(gffdb_file)

    # check if we have a list of gene names,
    # if not create one with all the genes in the genome
    if len(names) == 0:
        print("names = 0, get all loci!")
        number_of_loci = "all"
        # TODO: check if genes can either be "genes" or "CDS"? may need to check featuretypes fi
        # just use the genes.json!

        sql_cursor.execute("SELECT distinct(id) from features where featuretype = 'gene'")
        rows = sql_cursor.fetchall()
        if rows is None:
            print("check CDS features...")
            # when getting loci ids we look for either gene or CDS - so if not gene, CDS
            sql_cursor.execute("SELECT distinct(id) from features where featuretype = 'CDS'")
            rows = sql_cursor.fetchall()
        if rows is not None:
            for r in rows:
                if r[0] == "None":
                    print("skipping the None named gene...")
                else:
                    print('name: '+r[0])
                    names.append(r[0])

    else:
        number_of_loci = str(len(names))
    print("number of loci: "+str(len(names)))

    # get gffutils db
    gffdb = get_gffutils_db(gffdb_file)

    # load genome fasta into biopython
    genome_dict = SeqIO.to_dict(SeqIO.parse(genome_fasta, "fasta"))

    # list for genes with no sequence found
    no_seq = []

    with open(log_file, "w") as log:
        log.write("Extracting regions for "+number_of_loci+" loci\n")
        # want to loop through each gene wanted, for each gene get the region and write into the new fasta
        # will need to make the new fasta
        count = 0
        with open(new_fasta, "w") as fa:
            # loop through gene names and get each region...
            for g in names:
                count+=1
                print("will get region for gene "+g)
                log.write("Getting region for: "+g+"\n")
                seq = get_region_seq(g, start_feature, start_padding, end_feature, end_padding, gffdb, sql_cursor, genome_dict, limit, log)
                # write to fasta
                if seq is not None and seq is not "":
                    fa.write(">"+g+'\n'+seq+'\n')
                else:
                    # add some feedback about gene g having no sequence for the criteria selected....
                    log.write("There was no sequence for gene: "+g)
                    # add gene to no_seq list
                    no_seq.append(g)
        print("got "+str(count)+ " regions")
    # return the list of genes with no sequences for feedback in the UI
    return no_seq


def get_custom_limit_boundaries(name, region_start, region_end, limit, strand, ch_length, ch_name, sqldb):
    ''' does this even make sense???'''
    print("get custom limit boundaries...")



def get_custom_point(ref_feature, padding, c_start, c_end, strand):
    ''' returns the point from the custom feature '''
    s_or_e = ref_feature[len(ref_feature) - 1]
    if s_or_e == "0" and strand == "+":
        # this is the start of the feature so strand is "+" the ref_point is
        start_point = c_start + padding
    elif s_or_e == "0" and strand == "-":
        start_point = c_end - padding
    elif s_or_e == "$" and strand == "+":
        start_point = c_end + padding
    else:
        # last option is s_or_e == "$" and strand == "-"
        start_point = c_start - padding



def get_custom_seq_region(name, c_start, c_end, strand, ch_name, start_feature, start_padding, end_feature, end_padding, gffdb, sqldb, genome_dict, limit, log):
    print("getting region for "+name)
    # TODO we assume that the custom features are in 1 based coordinates - like the gff
    #the start_feature and end feature can only be "Custom_0" and "Custom_$"
    # indicating the start or end of the custom feature

    # first get the starting point
    start_point = get_custom_point(start_feature, start_padding,c_start,c_end,strand)

    # now get end point
    end_point = get_custom_point(end_feature, end_padding,c_start,c_end,strand)

    # TODO now test and assign region start and end based on strand - constrain the input to make sense?
    # for a negative strand the start will be less  than the end
    # also adjust for o based counting in python slice
    if start_point < end_point:
        region_start = start_point -1
        region_end = end_point -1
    else:
        # since a python slice does not include the end , if it is on the negative strand, you want don't want to subtract 1
        region_start = end_point - 1
        region_end = start_point -1

    log.write("in zero base coordinates:\n")
    log.write("region start = " + str(region_start)+"\n")
    log.write("region end = " + str(region_end)+"\n")

    # now check edge cases and limits
    print("will test boundaries and edge cases next?")
    # now check limits
    # TODO - check that the region complies with "limit" desires...coding/non coding
    # does this even make sense here???
    '''
    print("limit: " + limit)
    boundary_start, boundary_end = get_custom_limit_boundaries(name, region_start, region_end, limit, strand, ch_length, ch_name, sqldb)
    # make boundaries same 0-base counting as python
    boundary_start = boundary_start - 1
    boundary_end = boundary_end - 1
    print("boundary_start = " + str(boundary_start))
    print("boundary_end = " + str(boundary_end))
    # compare start and end points to the limitation boundary
    if region_start < boundary_start:
        print(name + ": region start < limit boundary was adjusted from " + str(region_start) + " to " + str(
            boundary_start))
        region_start = boundary_start
    if region_end > boundary_end:
        print(name + ": region end > than  than limit boundary was adjusted from " + str(region_end) + " to " + str(
            boundary_end))
        region_end = boundary_end
    '''
    print("final coordinates: chromosome = " + ch_name + ", start = " + str(region_start) + ", end = " + str(
        region_end))
    log.write(
        "coordinates: chromosome = " + ch_name + ", start = " + str(region_start) + ", end = " + str(
            region_end) + "\n")
    # now, since python slices do not include the end point,
    # if the strand is "-" add 1 to the start and end
    if strand == "-":
        region_end += 1
        region_start += 1
        print("Negative strand: slice[" + str(region_start) + ":" + str(region_end) + "]")
        log.write("Negative strand: slice[" + str(region_start) + ":" + str(region_end) + "]\n")

    # TODO check for edge cases:
    # what is region start is < 0?
    if region_start < 0:
        # TODO- create log file for these things to be reported
        print("ERROR: the region for gene " + name + " start was less than zero: " + str(
            region_start) + ", region end: " + str(region_end))
        log.write("Edge case: start was less than zero")
        region_start = 0
    # What if region end is greater than the length of the chromosome?
    ch_length = len(genome_dict[ch_name].seq)
    if region_end > ch_length:
        print("ERROR: the region for gene " + name + " end was greater than the chomosome length: " + str(
            ch_length) + ", region start: " + str(region_start))
        log.write("Edge case: end was greater than the length of the chromosome")
        region_end = ch_length

    # get sequence region (add one to the end so that that bas is included)
    try:
        seq = genome_dict[ch_name].seq[region_start:region_end]
        log.write("final sequence length: " + str(len(seq)) + "\n")
        log.write("final sequence: " + str(seq)+"\n")
    except Exception as e:
        print("error: "+str(e))
        seq = "exception..."
    return str(seq)



def process_custom(log_file, new_fasta, genome_fasta, gffdb_file, start_feature, start_padding, end_feature, end_padding, limit, custom_file):
    ''' generate the fasta file with desired sequence regions for custom features in custom_file '''

    # first load up database and get db cursor
    sql_cursor= get_sqlite_db(gffdb_file)

    # get gffutils db
    gffdb = get_gffutils_db(gffdb_file)

    # load genome fasta into biopython
    genome_dict = SeqIO.to_dict(SeqIO.parse(genome_fasta, "fasta"))
    # list for genes with no sequence found
    no_seq = []
    with open(log_file, "w") as log:
        log.write("Extracting regions for custom features\n")
        # want to loop through each gene wanted, for each gene get the region and write into the new fasta
        # will need to make the new fasta
        with open(new_fasta, "w") as fa:
            # loop through each feature in the custom file
            with open(custom_file, "r") as features:
                for line in features:
                    print("looking at line: "+line)
                    # todo skip lines starting with "#" those will be headers
                    if not line.startswith("#"):
                        # TODO decide what the line will be: name, sequence, PAM, start, end, strand, chromosome
                        # TODO above is what we have now plus the chromosome name at the end we can change the parsing later
                        # instead would like: name, chromosome, start, end, strand, sequence, pam
                        # then we can ignore the last 2 columns so any file with just the first 5 columns would work
                        parts = line.split(",")
                        #check if there are enough parts
                        if len(parts) >= 5:
                            name = parts[0]
                            # sequence = parts[5]
                            # pam = parts[6]
                            c_start = int(parts[2])
                            c_end = int(parts[3])
                            strand = parts[4]
                            ch_name = parts[1]
                            log.write("Extracting sequence region for "+name+"\n")
                            log.write("Chromosome/scaffold: " + ch_name + "\n")
                            log.write("Strand: " + strand + "\n")
                            #seq = get_region_seq(g, start_feature, start_padding, end_feature, end_padding, gffdb, sql_cursor, genome_dict, limit)
                            seq = get_custom_seq_region(name, c_start, c_end, strand, ch_name, start_feature, start_padding, end_feature, end_padding, gffdb, sql_cursor, genome_dict, limit, log)
                            # write to fasta
                            if seq is not None and seq is not "":
                                fa.write(">" + name + '\n' + seq + '\n')
                            else:
                                # add some feedback about gene g having no sequence for the criteria selected....
                                log.write("There was no sequence for gene: " + g)
                                # add gene to no_seq list
                                no_seq.append(name)
    # return no_seq list
    return no_seq


def main():
    '''
    this script will take the parameters entered by the user, and create a fasta file wth the sequence regions selected
    for gene centric selection - another python script will handel custom features - will have a different set of features

    if all genes names are missing, then all genes are assumed

    new_fasta_file is  the new file name/path that will be generated
    genome_fasta_file is the genome fasta
    gffdb_file is the sqlite db file generated from the gff needed for feature info
    then will need 4 parameters:
    start_feature
    start_padding
    end_feature
    end_padding
     a "-" number for the padding indicates upstream, a "+" number indicates downstream form the feature starting point
    limit regions can be [coding | non-coding | none] to check that regions are in the areas wanted depending of length of genes and parametes they may need to be truncated

    :return:
    '''
    if len(sys.argv) < 10:
        print(
            "Usage: python extract_seq_regions.py new_fasta_file genome_fasta_file gffdb_file start_feature start_padding end_feature end_padding limit_regions log_file name1 name2....nameN ")
    else:
        new_fasta = sys.argv[1]
        genome_fasta = sys.argv[2]
        gffdb_file = sys.argv[3]
        start_feature = sys.argv[4]
        start_padding = int(sys.argv[5])
        end_feature = sys.argv[6]
        end_padding = int(sys.argv[7])
        limit = sys.argv[8]
        log_file = sys.argv[9]

        # get optional names
        names = sys.argv[10: len(sys.argv)]

        process_genes(log_file, new_fasta, genome_fasta, gffdb_file, start_feature, start_padding, end_feature, end_padding, limit, names)
        print("done!")


if __name__ == "__main__":
    main()