import sqlite3
import gffutils
import subprocess
import json, os, sys, psycopg2


def create_bowtie_indexes(data_path, uuid, fasta_file, bowtie_path):
    ''' Run bowtie-build, moved this here because can take a long time...'''
    print("will create indexes using bowtie ")
    try:
        # cmnd: bowtie-build -r -f ${uuid}.fa ${uuid}.index
        input = os.path.join(data_path, uuid + '.fa')
        output = os.path.join(data_path, uuid + '.index')
        process = subprocess.Popen([bowtie_path+'/bowtie-build',
                                    '-r',
                                    '-f', input, output]).wait()
        output, error = process.communicate()
        if process.returncode != 0:
            print("bowtie failed %d %s %s" % (process.returncode, output, error))
        print("bowtie is running...")
    except:
        # ok = False
        # msg = msg + " Could not create index files."
        # TODO catch exception, make message for end user
        print("Caught exception -  when running bowtie build - ignoring")


def create_genes_json(data_path, uuid, db_file):
    ''' Create a json file with the genes available for the genome - will be used by front end
        will need the gene or name/ID,
        TODO if there are multiple proteins - indicate also? for now just focus on genes, as bare minimum
        To get all the gene ID's  will use sqlite
        also, get what the feature types are in a list
        '''

    # get sqlite connection and cursor
    conn = sqlite3.connect(db_file)
    cur = conn.cursor()

    # get the features
    print("getting feature types")
    featuretypes = []
    cur.execute("SELECT distinct(featuretype) from features")
    rows = cur.fetchall()
    for r in rows:
        # each row is actually a list of 1 string
        # TODO - in the GFF3 standard, these can be codes-
        #  create a dict that maps the codes to the words - store it the way it is her? have another for display purposes for UI?
        featuretypes.append(r[0])

    # get gene ids - make sure there are "genes" in the feature type- else use CDS (maybe the case for GB files?)
    print("getting genes")
    genes = []
    if 'gene' in featuretypes:
        # use the "ORDER BY id COLLATE NOCASE ASC" so that
        # upper and lower case names are grouped together when viewed in UI
        cur.execute("SELECT distinct(id) from features where featuretype = 'gene' ORDER BY id COLLATE NOCASE ASC")
    else:
        cur.execute("SELECT distinct(id) from features where featuretype = 'CDS' ORDER BY id COLLATE NOCASE ASC")
    rows = cur.fetchall()
    print("number of genes: "+ str(len(rows)))
    for r in rows:
        # each row is actually a list of 1 string
        # check to make sure the gene is not called "None"
        # The gb to gff converter calls any gene it cannot figure out "None" we want to exclude these
        g = r[0]
        if g != "None":
            genes.append(g)

    # create dict that we will make the json from
    gff_dict = {'genes': genes}

    # create genes json file
    print("creating genes json file")
    json_pth = os.path.join(data_path, "genes.json")
    with open(json_pth, 'w') as json_file:
        json.dump(gff_dict, json_file)

    # now create features.json - use array so that the js front end will parse in order written
    features = []
    cur.execute("select distinct(featuretype) from features")
    rows = cur.fetchall()
    for r in rows:
        if r[0] == 'gene':
            features.append(["gene_0", "Gene Starting Base"])
            features.append(["gene_$","Gene Ending Base"])
        elif r[0] == 'CDS':
            features.append(["CDS_F_0","First CDS Starting Base"])
            features.append(["CDS_F_$","First CDS Ending Base"])
            features.append(["CDS_L_0","Last CDS Starting Base"])
            features.append(["CDS_L_$","Last CDS Ending Base"])
        elif r[0] == 'exon':
            features.append(["exon_F_0","First Exon Starting Base"])
            features.append(["exon_F_$","First Exon Ending Base"])
            features.append(["exon_L_0","Last Exon Starting Base"])
            features.append(["exon_L_$","Last Exon Ending Base"])
    # create the json file
    feature_dict = {"featuretypes": featuretypes, "features": features}
    features_json_pth = os.path.join(data_path, "features.json")
    with open(features_json_pth, 'w') as json_file:
        json.dump(feature_dict, json_file)


def update_database(uuid, dbname, dbhost, dbuser, dbpass, status):
    # Connect to database
    conn = psycopg2.connect(dbname=dbname, user=dbuser, host=dbhost, password=dbpass)

    # Open a cursor to perform database operations
    cur = conn.cursor()

    # update row
    cur.execute("""UPDATE tbl_genomes SET genome_status=%s WHERE genome_uuid=%s;""",
                (status, uuid))
    conn.commit()
    # close cur and
    cur.close()
    conn.close()


def process(data_path, uuid, ann_file, dbname, dbhost, dbuser, dbpass, fasta_file, bowtie_path ):
    # create the db file
    #ok = True
    try:
        #db_filename = uuid + ".db"
        input = os.path.join(data_path, uuid + '.gff3')
        output = os.path.join(data_path, uuid + '.db')

        db = gffutils.create_db(input, dbfn=output, force=True, keep_order=True, merge_strategy='merge',
                                sort_attribute_values=True)
        create_genes_json(data_path, uuid, output)
        create_bowtie_indexes(data_path, uuid, fasta_file, bowtie_path)
        status= "Complete"

    except Exception as e:
        #ok = False
        #msg = " Could not create sql lite db."
        # TODO catch exception, make message for end user - not being caught properly!!!
        print("ERROR: when converting gff3 to sqlite: " + str(e))
        status ="Failed"

    update_database(uuid, dbname, dbhost, dbuser, dbpass, status)


def main():
    '''
    this script uses gffutils to create a sqlite db file, which can take a long time
    once the db file is made, it creates a <uuid>json file with a list of feature types an a list of genes (ids)
    :return:
    '''
    if len(sys.argv) < 10:
        print("Usage: python create_genome_annotation_db.py data_path uuid annotation_filename dbname dbhost dbuser dbpass genome_fa_file bowtie_path")
    else:
        data_path =sys.argv[1]
        uuid = sys.argv[2]
        ann_file = sys.argv[3]
        dbname = sys.argv[4]
        dbhost = sys.argv[5]
        dbuser = sys.argv[6]
        dbpass = sys.argv[7]
        fasta_file = sys.argv[8]
        bowtie_path = sys.argv[9]
        process(data_path, uuid, ann_file, dbname, dbhost, dbuser, dbpass, fasta_file, bowtie_path)
        print("done!")


if __name__ == "__main__":
    main()