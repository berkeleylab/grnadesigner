import sys
from Bio import SeqIO


def get_full_genes(newfile, genes, names, pam, genome_sequence):
    ''' make fasta with full gene regions..'''
    # open new file to write the new fasta
    with open(newfile + "_input.fa", "w") as output:
        for name in names:
            gene = genes[name]
            output.write(">" + name + "\n")
            # adjust indexes for slicing string
            start = gene['start'] - 1
            end = gene['end'] - 1
            # adjust to add the length of the PAM sequence plus 3 bases to the start of the gene sequence
            adjustment = len(pam)+3
            if gene['strand'] == "+":
                start = start - adjustment
            else:
                end = end + adjustment
            output.write(genome_sequence[start:end]+ "\n")

def get_region(newfile, genes, names, genome_sequence, region_start, region_end):
    '''  make fasta with specified regions '''
    with open(newfile + "_input.fa", "w") as output:
        for name in names:
            gene = genes[name]
            output.write(">" + name + "\n")
            if gene['strand'] == "+":
                # adjust indexes for slicing string
                # get the start point
                # bug fix 11/30/2020 - for -50 to 100 only giving 50 bp, for -200 to 0 none
                if region_start > 0:
                    # this was original code with no conditions
                    #TODO- shouldn't this be + region_start?
                    start = gene['start'] + region_start -1
                elif region_start == 0:
                    start = gene['start'] - 1
                else:
                    start = gene['start'] + region_start -1
                # get end point index
                if region_end > 0:
                    # this was original code with no conditions
                    end = gene['start'] + region_end - 1
                elif region_end == 0:
                    end = gene['start'] -1
                else:
                    # this is the same as if it is a positive number so a little redundant, but I will keep this for clarity
                    end = gene['start'] + region_end - 1

            else:
                # if on the negative strand
                # adjust indexes for slicing string
                if region_start > 0:
                    # this was original code with no conditions
                    start = gene['end'] - region_start - 1
                elif region_start == 0:
                    start = gene['end'] -1
                else:
                    #if it is negative
                    #TODO- minus a negative number is adding that number- this is the negative strand
                    start = gene['end'] - region_start -1

                # get end point - the gene [end is alwasys the start on the opposite strand]
                if region_end > 0:
                    # this was original code with no conditions
                    end = gene['end'] - region_end - 1
                elif region_end == 0:
                    end = gene['end'] -1
                else:
                    # this is the same as if it is a positive number so a little redundant, but I will keep this for clarity
                    end = gene['end'] - region_end -1

            # check edge cases:
            if start < 0:
                start = 0
            if end > len(genome_sequence):
                end = len(genome_sequence)

            output.write(genome_sequence[start:end]+ "\n")


def run(newfile, fasta, bed, full_gene, seq_start, seq_end, names, pam):
    print("getting sequence regions with full_gene=" + full_gene + ", seq_start=" + str(seq_start) + ", seq_end=" + str(
        seq_end))
    # if there are no names, we assume that we will get all
    all = False
    if len(names) == 0:
        all = True
        print("getting regions for all genes")

    # open fasta file and get sequence
    # this should be the entire sequence of the genome - assuming prokaryote genome
    # TODO - only works for single chromosome
    fasta = list(SeqIO.parse(fasta, "fasta"))
    seq = fasta[0]
    genome_sequence = str(seq.seq)

    # now open bed to parse all the lines into a dict
    # the key will be the name, and the value will be a dict with chromosome, start, end and strand
    genes = {}
    with open(bed) as f:
        for line in f:
            # chromosome start end name score strand ....
            parts = line.split("\t")
            if len(parts) > 6:
                genes[parts[3]] = {'ch': parts[0], 'start': int(parts[1]), 'end': int(parts[2]), 'strand': parts[5]}
    f.close()

    # now list through names or all, and get sequence, write to fasts file
    if all:
        names = genes.keys()
    else:
        bad_names = []
        # check to make sure all the names are present -
        for name in names:
            if name not in genes.keys():
                bad_names.append(name)
                print("ALERT bad gene: " + name)

    if full_gene == 'True':
        print("getting full genes...")
        get_full_genes(newfile, genes, names, pam, genome_sequence)
    else:
        print("getting regions...")
        get_region(newfile, genes, names, genome_sequence, seq_start, seq_end)

    print("done!")


def main():
    '''
    this script takes a fasta file and bed file, and creates a new fasta that has either all the genes (or just the named genes)
    and just the sequence specified relative to the start codon (ATG - where A is at the +1 position) sith the parameters direction [+ | -], start and end
    :return:
    '''
    if len(sys.argv) < 8:
        print(
            "Usage: python get_sequence_regions.py newname fastafile bedfile PAM full_gene start end name1 name2....nameN ")
    else:
        newname = sys.argv[1]
        fasta = sys.argv[2]
        bed = sys.argv[3]
        pam = sys.argv[4]
        full_gene = sys.argv[5]
        start = int(sys.argv[6])
        end = int(sys.argv[7])

        # get optional names
        names = sys.argv[8: len(sys.argv)]

        run(newname, fasta, bed, full_gene, start, end, names, pam)
        # print("done!")


if __name__ == "__main__":
    main()
