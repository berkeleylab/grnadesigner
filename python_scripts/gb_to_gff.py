#!/usr/bin/env python
"""
Convert data from Genbank format to GFF.

Usage:
python gbk_to_gff.py in.gbk > out.gff

Requirements:
    BioPython:- http://biopython.org/
    helper.py:- https://github.com/vipints/GFFtools-GX/blob/master/helper.py

Copyright (C)
    2009-2012 Friedrich Miescher Laboratory of the Max Planck Society, Tubingen, Germany.
    2012-2015 Memorial Sloan Kettering Cancer Center New York City, USA.

Modified by Lisa Simirenko June 9, 2021:
    Removed python2 print statements and sys.exit() call
    updated for BioPython SeqRecord feature locations
    moved helper.open_file() function to this script so that that dependency is not needed
    Put in try/except for instances where genes are annotated without CDS annotations, which was causing an error
    TODO This means that if a gene does not have a CDS then the GFF will not have a corresponding CDS feature or exon (does this matter?)

    also changed so it writes to a file instead of standard out - you give it the gff_filename as the second parameter
"""

import os
import re
import sys
import bz2
import gzip
import collections
from Bio import SeqIO

def open_file(fname):
    """
    Open the file (supports .gz .bz2) and returns the handler
    @args fname: input file name for reading
    @type fname: str
    """

    if os.path.isfile(fname):
        file_prefx, ext = os.path.splitext(fname)
    else:
        exit("error: the provided file %s is not available to read. Please check!" % fname)

    try:
        if ext == ".gz":
            FH = gzip.open(fname, 'rb')
        elif ext == ".bz2":
            FH = bz2.BZ2File(fname, 'rb')
        else:
            FH = open(fname, 'r')
    except Exception as error:
        sys.exit(error)

    return FH


def feature_table(chr_id, source, orient, genes, transcripts, cds, exons, unk, gff_file):
    """
    Write the feature information
    """
    for gname, ginfo in genes.items():
        line = [str(chr_id),
                'gbk2gff',
                ginfo[3],
                str(ginfo[0]),
                str(ginfo[1]),
                '.',
                ginfo[2],
                '.',
                'ID=%s;Name=%s' % (str(gname), str(gname))]
        gff_file.write('\t'.join(line) + "\n")
        ## construct the transcript line is not defined in the original file
        t_line = [str(chr_id), 'gbk2gff', source, '0', '1', '.', ginfo[2], '.']

        if not transcripts:
            try:
                t_line.append('ID=Transcript:%s;Parent=%s' % (str(gname), str(gname)))

                if exons:  ## get the entire transcript region  from the defined feature
                    t_line[3] = str(exons[gname][0][0])
                    t_line[4] = str(exons[gname][0][-1])
                elif cds:
                    # There is an index out of range error happening here so put in try/except
                    t_line[3] = str(cds[gname][0][0])
                    t_line[4] = str(cds[gname][0][-1])

                if not cds:
                    t_line[2] = 'transcript'
                else:
                    t_line[2] = 'mRNA'
                gff_file.write('\t'.join(t_line) + "\n")
            except:
                # do nothing, an error occurs if there is a gene annotated,
                # but it does not have an annotated CDS, so we just wan to skip these
                lisa = 0

            if exons:
                exon_line_print(t_line, exons[gname], 'Transcript:' + str(gname), 'exon')

            if cds:
                exon_line_print(t_line, cds[gname], 'Transcript:' + str(gname), 'CDS', gff_file)
                if not exons:
                    exon_line_print(t_line, cds[gname], 'Transcript:' + str(gname), 'exon', gff_file)

        else:  ## transcript is defined
            for idx in transcripts[gname]:
                t_line[2] = idx[3]
                t_line[3] = str(idx[0])
                t_line[4] = str(idx[1])
                t_line.append('ID=' + str(idx[2]) + ';Parent=' + str(gname))
                gff_file.write('\t'.join(t_line) + "\n")

                ## feature line print call
                if exons:
                    exon_line_print(t_line, exons[gname], str(idx[2]), 'exon', gff_file)
                if cds:
                    exon_line_print(t_line, cds[gname], str(idx[2]), 'CDS', gff_file)
                    if not exons:
                        exon_line_print(t_line, cds[gname], str(idx[2]), 'exon', gff_file)

    if len(genes) == 0:  ## feature entry with fragment information

        line = [str(chr_id), 'gbk2gff', source, 0, 1, '.', orient, '.']
        fStart = fStop = None

        for eid, ex in cds.items():
            fStart = ex[0][0]
            fStop = ex[0][-1]

        for eid, ex in exons.items():
            fStart = ex[0][0]
            fStop = ex[0][-1]

        if fStart or fStart:

            line[2] = 'gene'
            line[3] = str(fStart)
            line[4] = str(fStop)
            line.append('ID=Unknown_Gene_' + str(unk) + ';Name=Unknown_Gene_' + str(unk))
            gff_file.write('\t'.join(line) + "\n")

            if not cds:
                line[2] = 'transcript'
            else:
                line[2] = 'mRNA'

            line[8] = 'ID=Unknown_Transcript_' + str(unk) + ';Parent=Unknown_Gene_' + str(unk)
            gff_file.write('\t'.join(line) + "\n")

            if exons:
                exon_line_print(line, cds[None], 'Unknown_Transcript_' + str(unk), 'exon', gff_file)

            if cds:
                exon_line_print(line, cds[None], 'Unknown_Transcript_' + str(unk), 'CDS', gff_file)
                if not exons:
                    exon_line_print(line, cds[None], 'Unknown_Transcript_' + str(unk), 'exon', gff_file)

            unk += 1

    return unk


def exon_line_print(temp_line, trx_exons, parent, ftype, gff_file):
    """
    Print the EXON feature line
    """
    for ex in trx_exons:
        temp_line[2] = ftype
        temp_line[3] = str(ex[0])
        temp_line[4] = str(ex[1])
        temp_line[8] = 'Parent=%s' % parent
        gff_file.write('\t'.join(temp_line) + "\n")


def gbk_parse(fname, gff_name):
    """
    Extract genome annotation recods from genbank format

    @args fname: gbk file name
    @type fname: str
    """
    fhand = open_file(gbkfname)
    unk = 1
    
    with open(gff_name, "w") as gff_file:

        for record in SeqIO.parse(fhand, "genbank"):
            gene_tags = dict()
            tx_tags = collections.defaultdict(list)
            exon = collections.defaultdict(list)
            cds = collections.defaultdict(list)
            mol_type, chr_id = None, None
    
            for rec in record.features:
    
                if rec.type == 'source':
                    try:
                        mol_type = rec.qualifiers['mol_type'][0]
                    except:
                        mol_type = '.'
                        pass
                    try:
                        chr_id = rec.qualifiers['chromosome'][0]
                    except:
                        #chr_id = record.name
                        # want record id instead so that it matches the chromosome name in the fasta file
                        chr_id = record.id
                    continue
    
                strand = '-'
                strand = '+' if rec.strand > 0 else strand
    
                fid = None
                try:
                    fid = rec.qualifiers['gene'][0]
                except:
                    pass
    
                transcript_id = None
                try:
                    transcript_id = rec.qualifiers['transcript_id'][0]
                except:
                    pass
    
                if re.search(r'gene', rec.type):
                    gene_tags[fid] = (rec.location.start.position + 1,
                                      rec.location.end.position,
                                      strand,
                                      rec.type
                                      )
                elif rec.type == 'exon':
                    exon[fid].append((rec.location.start.position + 1,
                                      rec.location.end.position))
                elif rec.type == 'CDS':
                    cds[fid].append((rec.location.start.position + 1,
                                     rec.location.end.position))
                else:
                    # get all transcripts
                    if transcript_id:
                        tx_tags[fid].append((rec.location.start.position + 1,
                                             rec.location.end.position,
                                             transcript_id,
                                             rec.type))
            # record extracted, generate feature table
            unk = feature_table(chr_id, mol_type, strand, gene_tags, tx_tags, cds, exon, unk, gff_file)
    
        fhand.close()


if __name__ == '__main__':

    try:
        gbkfname = sys.argv[1]
        gff_name  = sys.argv[2]
    except:
        print("Usage: python gbk_to_gff.py gb_file gff_file_name")

    ## extract gbk records
    gbk_parse(gbkfname, gff_name)
