from Bio import SeqIO
import sys
import subprocess


def make_bed_file(gb_file, output_name, acc):
    outf = open(output_name + '.bed', 'w')
    for record in SeqIO.parse(open(gb_file, "rU"), "genbank"):
        for feature in record.features:
            # if feature.type == 'gene': <- this included ncRNAs so using CDS instead
            if feature.type == 'CDS':
                start = feature.location.start.position
                stop = feature.location.end.position
                try:
                    name = feature.qualifiers['gene'][0]
                except:
                    # some features only have a locus tag
                    name = feature.qualifiers['locus_tag'][0]
                if feature.strand < 0:
                    strand = "-"
                else:
                    strand = "+"
                # if the name has a single quote in it, CCTop will not process it,
                # so I am replacing ' with _prime_ here
                name = name.replace("'", "_prime_")
                bed_line = acc+"\t{0}\t{1}\t{2}\t1000\t{3}\t{0}\t{1}\t65,105,225\n".format(start, stop, name, strand)
                outf.write(bed_line)
    outf.close()


def process(gb_file, name):
    ''' process gbfile for CCTop Genome'''
    # first create a fasta file, an also grab the acc.ver number
    gb = open(gb_file, "rU")
    sequences = SeqIO.parse(gb, "genbank")

    # and it's crude, parse the first  line from the fasta file to be used in the bed file
    fasta = open(name+".fa", "w")
    count = SeqIO.write(sequences, fasta, "fasta")
    fasta.close()

    print("fasta has "+str(count)+" sequences")

    with open(name+".fa") as f:
        for first_line in f:
            # split on  white space, and get the first token
            acc_ver = first_line.split(" ")[0]
            # remove the ">"
            acc_ver = acc_ver[1:len(acc_ver)]
            print("acc.ver: "+acc_ver)
            break

    # now make bed file
    make_bed_file(gb_file, name, acc_ver)

    # TODO - still need to run bowtie to make the index, but will do that on the back end

    print("done!")


def main():
    '''
    this script takes a genbank file and creates a fasta file and bed file
    right now just takes one GB file per genome - the files will be called new_file_name.fa and new_file_name.bed
    :return:
    '''
    if len(sys.argv) < 3:
        print("Usage: python process_gb_for_CCTop.py gbfile new_files_name")
    else:
        gbfile =sys.argv[1]
        name = sys.argv[2]
        process(gbfile, name)
        #print("done!")


if __name__ == "__main__":
    main()
