import sys
import subprocess

def make_beds(gff_file, output_name):
    # first parse the gff into 2 dicts - one for cds, one for genes
    genes = []
    exons = []
    with open(gff_file) as gff:
        for line in gff:
            parts = line.split('\t')
            # make sure there are enough parts
            if len(parts) >= 9:
                # get name
                name = 'unknown'
                id = 0
                # split on ";"
                # will make the gene name be the protein or transcriptId | name from the name column
                #  the same as in the Nanoce1779_2_GeneCatalog_CDS_201801119.fasta
                bits = parts[8].split(";")
                for b in bits:
                    b = b.strip()
                    if b.startswith("name"):
                        name = b.split(' ')[1]
                        # remove the quotes around the name
                        name = name[1:len(name)-1]
                    elif b.startswith("proteinId") or b.startswith("transcriptId"):
                        id = b.split(' ')[1]
                # make dict
                dict = {'ch':parts[0], 'start':parts[3], 'end':parts[4], 'score':parts[5], 'strand':parts[6], 'name': str(id)+ "|"+name}
                # now put dict in proper list
                if parts[2] == 'CDS':
                    genes.append(dict)
                elif parts[2] == 'exon':
                    exons.append(dict)

    # now create our bed files
    if len(genes) > 0:
        with open(output_name + '-genes.bed', 'w') as f:
            for item in genes:
                f.write(item['ch']+"\t"+item['start']+"\t"+item['end']+"\t"+item['name']+"\t"+item['score']+"\t"+item['strand']+"\t"+item['start']+"\t"+item['end']+"\t"+"65,105,225\n")
        print("Genes .bed was created")
    else:
        print("There were no CDS lines found in the gff provided")

    if len(exons) > 0:
        with open(output_name + '-exons.bed', 'w') as f:
            for item in exons:
                f.write(item['ch']+"\t"+item['start']+"\t"+item['end']+"\t"+item['name']+"\t"+item['score']+"\t"+item['strand']+"\t"+item['start']+"\t"+item['end']+"\t"+"65,105,225\n")
        print("Exons .bed was created")
    else:
        print("There were no exon lines found in the gff provided")

    print("done!")


def main():
    '''
    this script takes a gff file and creates 2 bed files - one with exons and one with CDSs
     the files will be called new_file_name-exons.fa and new_file-name_gene.bed

     caveates: this only works for gffs that have a xxxId in the 9th column (name) like "transcriptId 587995" for exons or "proteinId 587909" for CDSs
     This is used as the gene name in the bed files
    :return:
    '''
    if len(sys.argv) < 3:
        print("Usage: python gff2beds.py gff_file new_files_name")
    else:
        gff_file =sys.argv[1]
        name = sys.argv[2]
        make_beds(gff_file, name)
        #print("done!")


if __name__ == "__main__":
    main()