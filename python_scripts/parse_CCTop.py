__author__ = 'simi'
#!/usr/bin/env python
import sys

def parse(name):
    '''parse the .xls output from CCTop - return a list of dicts
    each dict describes a potential gRNA
    '''
    # open the file
    all_gRNAs = []
    off_targets = {} # the key will be the id ("T1"...etc), and the value will be a list of dicts for that protospacer

    try:
        with open(name+".xls", 'rU') as f:
            print("xls file was opened...")
            new_gRNA = False
            # first we need to skip the header lines
            for line in f:
                for _ in range(8):
                    next(f)
                for line in f:
                    print("line: "+line)
                    print("len line striped: "+str(len(line.strip())))
                    if len(line.strip()) == 0:
                        #elif not line.strip():
                        print("empty line...length of off target list = "+str(len(new_off_target_list)))
                        # if line is an empty string, then it is in between the next protospacer
                        off_targets[id]=new_off_target_list
                    # each new gRNA block starts with TN, where N is an interger count starting at 1
                    elif line.startswith("T"):
                        print("parsing new gRNA: " + line)
                        # this is a new gRNA
                        new_gRNA = True
                        new_off_target_list = []
                        # parse the id (TN), the sequence, the CCTop_score, and the CRISPRater score
                        parts = line.split("\t")
                        # there should be 5 parts
                        if len(parts) == 5:
                            print("there were 5 parts..")
                            # further parse the CRIPRater score it consists of 2 parts, the float score and "HIGH", MEDIUM or LOW
                            CRISPRater = parts[4]
                            cr_score = float(CRISPRater[0:4])

                            # create dict of info
                            id = parts[0]
                            info = {"id":parts[0], "sequence_plus_PAM":parts[1], "CCTop_score":int(parts[2]), "CRISPRater_score":cr_score, "CRISPRator": CRISPRater}
                    elif new_gRNA and not line.startswith("Oligo") and not line.startswith("Chromosome"):
                        # parse the chromosome positioning information into the info dict
                        # will only want the first line so also set the new_gRNA boolean to False
                        print("parsing position info: " + line)
                        new_gRNA = False
                        # parse line
                        parts = line.split("\t")
                        # there should be 12 parts
                        if len(parts) == 12:
                            print("there were 12 parts...")
                            info["chromosome"] = parts[0]
                            info["start"] = parts[1]
                            info["end"] = parts[2]
                            info["strand"] = parts[3]
                            info["MM"] = parts[4]
                            info["PAM"] = parts[6]
                            info["target_seq"] = parts[5] # this is the gRNA sequence without the PAM
                            info['seq'] = parts[5]
                            info["alignment"] = parts[7]
                            info["distance"] = parts[8]
                            info["position"] = parts[9]
                            info["gene_name"] = parts[10]
                            info["gene_id"] = parts[11]
                        # now add to list
                        all_gRNAs.append(info)
                    # get the off target hits...
                    elif not new_gRNA and not line.startswith("Oligo") and not line.startswith("Chromosome"):
                        print("getting off target: "+line)
                        # get the off targets and put in new_off_target_list
                        # parse line
                        new_parts = line.split("\t")
                        print("length of parts: "+str(len(new_parts)))
                        # there should be 12 parts
                        new_info = {}
                        if len(new_parts) == 12:
                            print("getting parts"+new_parts[0])
                            new_info["chromosome"] = new_parts[0]
                            new_info["start"] = new_parts[1]
                            new_info["end"] = new_parts[2]
                            new_info["strand"] = new_parts[3]
                            new_info["MM"] = new_parts[4]
                            new_info["PAM"] = new_parts[6]
                            new_info["target_seq"] = new_parts[5]  # this is the gRNA sequence without the PAM
                            new_info['seq'] = new_parts[5]
                            new_info["alignment"] = new_parts[7]
                            new_info["distance"] = new_parts[8]
                            new_info["position"] = new_parts[9]
                            new_info["gene_name"] = new_parts[10]
                            new_info["gene_id"] = new_parts[11]
                            print("done parsing")
                            # now add to list
                            new_off_target_list.append(new_info)
                        print("done putting in list...")


    except:
        print("there was no CCTop output for gene: "+name)

    return all_gRNAs, off_targets


def main():
    '''
    will parse the geneName.xls file from the current directory...this is one of the outputs from CCTop
    '''
    if len(sys.argv) < 2:
        print("Usage: python parse_CCTop.py geneName")
    else:
        name = sys.argv[1]
        #directory = sys.argv[2]
        gRNA_list = parse(name)
        print(gRNA_list)
        print("processed "+str(len(gRNA_list))+" gRNA's")


if __name__ == "__main__":
    main()