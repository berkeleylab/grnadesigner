{
    "workflow_failure_mode": "ContinueWhilePossible",
    "final_workflow_outputs_dir": "${DATA_DIRECTORY}/${uuid}",
    "use_relative_output_paths": true
}