__author__ = 'simi'
#!/usr/bin/env python
import sys
from operator import itemgetter
import json

import RNA
from Bio import SeqIO

import parse_CCTop as CCTop
import eval_folding as fold



def get_gene_info(json_file):
    ''' get a dict key = gene name, value is the strand (+ or -) from a json file
    '''
    gene_info = {}
    with open(json_file) as f:
        gene_info = json.load(f)
    return gene_info


def filter(fasta, project_json, scaffold, protospacer_end, number_wanted, project_name, bed_file, strand_wanted):

    # TODO - this won't work for custom input? - get gene_info
    gene_info = get_gene_info(bed_file)
    #create lists to collect gene names when not enough spacers were found
    genes_no_gRNAs = []
    genes_not_enough_gRNAs = []

    # open file for human readable reporting - will be put into the current
    with open(project_name+"_report.txt", "w") as report:
        # write header line
        report.write("loci name, number gRNAs found\n")
        # open file for -output csv
        with open(project_name+".csv", "w") as output:
            # output = open(directory+"/"+project_name+".csv", "w")
            # print header
            # instead would like: name, chromosome, start, end, strand, sequence, pam
            # output.write("#name,protospacer,PAM,start,end,strand\n")
            # start with # so parser will ignore header
            output.write("#name, chromosome, start, end, strand, protospacer, PAM\n")
            # first open the fasta file and get a list of gene names
            genes = []
            fasta = list(SeqIO.parse(fasta, "fasta"))
            for g in fasta:
                # for each gene get the name and put in list
                genes.append(g.id)

            # now for each gene in the list - process
            for g in genes:
                print("\n\nfor gene "+g)
                # first parse the CCTop output to get the list of possible gRNAs

                # gRNAs, off_target_dict = CCTop.parse(g, directory) <- not using directory in cromwell - will be in current directory
                gRNAs, off_target_dict = CCTop.parse(g)
                # check if list is empty
                if len(gRNAs) < 1:
                    print("ALERT: "+ g+" - no protospacers were found")
                    report.write(g+",0\n")
                    genes_no_gRNAs.append(g)
                else:
                    for gRNA in gRNAs:
                        print("\nevaluating folding for "+gRNA['id'])
                        # evaluate the protospacers for RNA folding
                        protospacer = gRNA['seq']
                        full_seq, structure, folding_score, delta_g, = fold.evaluate(protospacer, scaffold, protospacer_end)
                        # add to gRNA dict
                        gRNA['rna_sequence'] = full_seq
                        gRNA['rna_structure'] = structure
                        gRNA['folding_score'] = folding_score
                        gRNA['delta_g'] = delta_g


                        # before scoring check that there are no "0" scores - force to 1, except for CRISPERator scor, st that to 0.001
                        if gRNA['CCTop_score'] == 0:
                            print("CCTop score was zero")
                            gRNA['CCTop_score'] = 1
                        if gRNA['folding_score'] == 0:
                            print("folding score was zero")
                            gRNA['folding_score'] = 1
                        if gRNA['delta_g'] == 0:
                            print("delta G score was zero")
                            gRNA['delta_g'] = 1.0
                        if gRNA['CRISPRater_score'] == 0:
                            print("CRISPRater score was zero")
                            gRNA['CRISPRater_score'] = 0.001

                        # now create the score for the gRNA
                        print("for gRNA " + gRNA['id'] + ": CCTop score: " + str(
                                gRNA['CCTop_score']) + ", CRISPERater score: " + str(
                                gRNA['CRISPRater_score']) + ", folding score: " + str(
                                gRNA['folding_score']) + ", delta G: " + str(gRNA['delta_g']))
                        print("folding: ")
                        print(gRNA['rna_structure'])

                        # this will just be the prduct of the CCTop_score, CRISPERater_score, the folding_score (removed the delta_g - the negative number is skewing the score score)
                        gRNA['score'] = gRNA['CCTop_score'] * gRNA['CRISPRater_score'] * folding_score
                        print("score: " + str(gRNA['score']))

                        # now return the list sorted by score (descending)
                    print("sorting list... ")
                    sorted_list =  sorted(gRNAs, key=itemgetter('score'), reverse=True)

                    # now check if there are at least as many as "wanted"
                    if strand_wanted == "both":
                        if len(sorted_list) < number_wanted:
                            print("ALERT: " + g + " - only "+str(len(sorted_list))+" protospacers were found")
                            report.write( g + ","+str(len(sorted_list))+"\n")
                            genes_not_enough_gRNAs.append(g)

                            # print  to output
                            for x in sorted_list:
                                # the name of the protospacer will be the gene name+ "_" and the CCTop id (i.e. T1...TN)
                                output.write(g + "_" + str(x['id']) + "," + x['chromosome'] + "," + str(x['start']) + "," + str(x['end']) + "," + str(x['strand']) + "," + x['seq'] + "," + x['PAM']+"\n")
                        else:
                            # write only the first number_wanted to the output
                            for x in sorted_list[0:number_wanted]:
                                # the name of the protospacer will be the gene name+ "_" and the CCTop id (i.e. T1...TN)
                                output.write(g + "_" + str(x['id']) + "," + x['chromosome']  +","+str(x['start'])+","+str(x['end'])+","+str(x['strand'])+ "," + x['seq'] + "," + x['PAM'] +"\n")
                    else:
                        # will check stand of gRNA and strand wanted
                        # TODO- this is where we will filter for strand wanted
                        count = 0
                        for x in sorted_list:
                            if count < number_wanted:

                                if (strand_wanted == "coding" and gene_info[g] == x['strand']) or (strand_wanted == "non-coding" and gene_info[g] != x['strand']):
                                    print("good - strand wanted: "+strand_wanted+", gene strand: "+gene_info[g]+", gRNA strand: "+ x['strand']+ " for "+g)
                                    output.write(
                                        g + "_" + str(x['id']) + ","+x['chromosome'] + "," + str(x['start']) + "," +
                                        str(x['end']) + "," + str(x['strand']) + "," + x['seq'] + "," + x['PAM']+"\n")
                                    count +=1
                            else:
                                break

                        #check if got the number wanted, and report if not
                        if count != number_wanted:
                            print("ALERT: " + g + " - only " + str(count) + " protospacers were found")
                            report.write(g + "," + str(count) + "\n")
                            genes_not_enough_gRNAs.append(g)

    # now write the json file that will be used by the UI to display the results
    # first open the existing json file, and read it into a dict, then add the extra items,
    # change the message and write it back to the file
    # with open(project_name + ".json") as f:
    with open(project_json) as f:
        data = json.load(f)

    data['no_protospacers']=genes_no_gRNAs
    data['not_enough_protospacers']=genes_not_enough_gRNAs
    data['message']="Your design is finished"
    data['status']="done"

    # now overwrite the old file with the new json
    with open("project.json", 'w') as json_file:
        json.dump(data, json_file)


def main():
    '''
    For each gene in the fasta file - parse the CCTop output, score each gRNA and then prep the output
    make a csv file with the name and sequence of each protospacer/gRNA selected
    The name will be the geneName_TN where TN is the id of the CCTop gRNA in it's output file(genename.xls)
    Will also create a Report file - that just reports exceptions- for example if a gRNA could not be found
    or if not as many as wanted for a particular gene

    use full path to json file - this has to have the name of the locus a the key, and the strand as the value
    strand wanted can be [both | coding | non-coding] if coding use same strand as gene, non-coding use opposite


    '''
    if len(sys.argv) < 9:
        print("Usage: python filter_CCTop_v2.py fasta project_json scaffold_sequence protospacer_end_fused number_wanted project_name locus_info_json strand_wanted")
    else:
        fasta = sys.argv[1]
        project_json = sys.argv[2]
        scaffold = sys.argv[3]
        protospacer_end = sys.argv[4]
        number_wanted = int(sys.argv[5])
        project_name = sys.argv[6]
        json_file = sys.argv[7]
        strand_wanted = sys.argv[8]
        filter(fasta, project_json, scaffold, protospacer_end, number_wanted, project_name, json_file, strand_wanted)
        print("done!")


if __name__ == "__main__":
    main()