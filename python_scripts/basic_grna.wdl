workflow basic_grna {

     File genome_fasta_file
     File genome_bed_file
     File project_json
     String script_path
     String bowtie_path
     String cctop_path
     String results_path
     String uuid
     String target_size
     String PAM
     String total_MM
     String scaffold_sequence
     String protospacer_end_fused
     String number_wanted
     String region_start
     String region_end
     String full_gene
     String gene_names
     String dbname
     String dbhost
     String dbuser
     String dbpass



    # prep - get input regions for genes wanted
    call get_input_regions{
         input: genome_fasta_file=genome_fasta_file,
                genome_bed_file=genome_bed_file,
                uuid=uuid,
                script_path=script_path,
                results_path = results_path,
                PAM=PAM,
                region_start=region_start,
                region_end=region_end,
                full_gene=full_gene,
                gene_names=gene_names
    }

    call run_cctop{
         input: input_fasta = get_input_regions.input_fasta,
                genome_fasta_file = genome_fasta_file,
                genome_bed_file = genome_bed_file,
                project_json = project_json,
                script_path = script_path,
                bowtie_path = bowtie_path,
                cctop_path = cctop_path,
                results_path = results_path,
                uuid=uuid,
                PAM=PAM,
                target_size = target_size,
                total_MM = total_MM,
                scaffold_sequence = scaffold_sequence,
                protospacer_end_fused = protospacer_end_fused,
                number_wanted = number_wanted,
                dbname = dbname,
                dbhost = dbhost,
                dbuser = dbuser,
                dbpass = dbpass
    }

}
##############################################
######          TASKS              ###########
##############################################

# 1st: get the input sequence regions for CCTop run
task get_input_regions{
    File genome_fasta_file
    File genome_bed_file
    String uuid
    String script_path
    String results_path
    String PAM
    String region_start
    String region_end
    String full_gene
    String gene_names

    command
    {
        python ${script_path}/get_sequence_regions.py ${uuid} ${genome_fasta_file} ${genome_bed_file} ${PAM} ${full_gene} ${region_start} ${region_end} ${gene_names}

        # copy the resulting input.fa back to the results directory
        cp ${uuid}_input.fa ${results_path}
    }
    output {
        File input_fasta = "${uuid}_input.fa"
    }

}



# run CCTop using the input.fa from step 1
task run_cctop{

     File input_fasta
     File genome_fasta_file
     File genome_bed_file
     File project_json
     String script_path
     String bowtie_path
     String cctop_path
     String results_path
     String uuid
     String target_size
     String PAM
     String total_MM
     String scaffold_sequence
     String protospacer_end_fused
     String number_wanted
     String dbname
     String dbhost
     String dbuser
     String dbpass

     runtime {
        continueOnReturnCode: true
     }

    command
    {

        # run bowtie
        bowtie-build -r -f ${genome_fasta_file} ${uuid}.index

        # now run ccTop
        python ${cctop_path}/CCTop.py --input ${input_fasta} --index ${uuid}.index --bowtie ${bowtie_path} --targetSize ${target_size} --pam ${PAM} --totalMM ${total_MM} --output ./ --genesFile ${genome_bed_file} --exonsFile ${genome_bed_file}

        # then run scoring script
        # this needs to be in the same task as the CCTop command above, because the CCTop output files generated varie by gene names selected
        # by being in the same wdl task, they are executed in the same cromwell directory, so will be accessabble to the filter_CCTop.py script
        python ${script_path}/filter_CCTop.py ${input_fasta} ${project_json} ${scaffold_sequence} ${protospacer_end_fused} ${number_wanted}  ${uuid} > scoring.log

        # update the job in the synbio_apps database - add end_time, and update status
        # TODO - add error handling in wdl to catpture failures and set status and message according. Here we assume that it is complete with no errors if it has gotten this far
        python ${script_path}/update_synbio_app_database.py ${uuid} ${dbname} ${dbhost} ${dbuser} ${dbpass} ${project_json} "This gRNA design is finished." Complete ${uuid}

        # cp the input.fa here to put in the output
        # otherwise the entire path to the execution directory is also in the tar, not just the file
        # Even with this, cromwell v48 throws a
        # WorkflowManagerActor Workflow b510da14-fecc-4657-bad8-408f72d82eba failed (during FinalizingWorkflowState): java.nio.file.FileAlreadyExistsException: /path/to/uploads/<uuid>_input.fa
        # and the workflow is in a failed state - but everything else seems to be transfered properly
        mv ${input_fasta} ${uuid}_regions.fa

        # tar it all
        tar czf ${uuid}.tar.gz scoring.log ${uuid}_regions.fa ${uuid}.csv ${uuid}_report.txt ${uuid}_final.json *.bed *.fasta *.xls


    }

    output {
       File tar_file = "${uuid}.tar.gz"
       File csv_file = "${uuid}.csv"
       File report_file = "${uuid}_report.txt"
       File json_file = "${uuid}_final.json"
    }


}


