'''
Created on Mar 15, 2019

@author: Ernst Oberortner, Lisa Simirenko
'''

import config
import requests
from urllib.parse import unquote


def get_username(jgi_session_id):
    """
    sends a GET request to the Caliban/SSO API to retrieve the username

    if the GET request does not result in a 200 code or the session is expired, then
    get_username returns None.
    Otherwise it returns the login information of the registered user.
    """

    ## build the URL
    ## /api/sessions/<session_id>.json
    url = '{}{}.json'.format(config.SSO_URL, unquote(jgi_session_id))
    print('[SSO get_username] url: ', url)

    ## send the request
    r = requests.get(url)
    print('[SSO get_username] r.status_code: ', r.status_code)

    ## something's wrong, hence the session is not valid
    if r.status_code != 200: return None

    print('[SSO get_username] r.text: ', r.text)
    print('[SSO get_username] r.json: ', r.json())

    ## get the username out the returned JSON
    return r.json()['user']['login'] if 'user' in r.json() and 'login' in r.json()['user'] else None


if __name__ == '__main__':
    pass