'''
Created on Mar 15, 2019

@author: Ernst Oberortner, Lisa Simirenko
'''

import datetime
import config
import jwt

def get_jwt(username):
    """
    returns the JWT (as string) for a given username
    """

    if username is None or len(username.strip()) == 0: return None

    try:
        jwt_dict = {'exp': datetime.datetime.utcnow() + datetime.timedelta(days=365),
                    'username': username}
        # on svm module 'jwt' has no attribute 'encode'
        encoded_jwt = jwt.encode(jwt_dict, config.JWT_SECRET, algorithm='HS256')
        print('encoded_jwt: {}'.format(type(encoded_jwt)))
        #print('encoded_jwt: {}'.format(encoded_jwt.decode("utf-8")))
    except Exception as e:
        print("ERROR: " + str(e))
    # with current libraries (2021-AUG), the encode_jwt variable is already a string- there is no decode() method
    return encoded_jwt #.decode("utf-8")


def get_username(encoded_jwt):
    """
    returns the username extracted from a given jwt
    """

    if encoded_jwt is None or len(encoded_jwt.strip()) == 0: return None

    decoded_jwt = jwt.decode(encoded_jwt, config.JWT_SECRET, algorithms='HS256')
    print('decoded_jwt: {}'.format(type(decoded_jwt)))
    print('decoded_jwt: {}'.format(str(decoded_jwt)))
    return decoded_jwt['username']


if __name__ == '__main__':
    pass
