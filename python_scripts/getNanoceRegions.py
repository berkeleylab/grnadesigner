import sys
from Bio import SeqIO

# 23 genes do not follow the expected patter for cds/1st exon calculation
# this is a work around - the key is a string for the gene number, and the value is the start position of the exon needed
exceptions = {}
exceptions['652434']=457843
exceptions['609171']=43188
exceptions['590263']=459485
exceptions['591107']=190608
exceptions['592816']=561742
exceptions['629439']=629320
exceptions['582189']=307413
exceptions['628325']=765247
exceptions['442917']=596575
exceptions['669251']=717521
exceptions['581884']=43891
# no...make sure that the cds is "inside" the exon....


def create_input_file(newname, info_dict, genelist, scaffold, cds2, cds3, inhibit):
    # parse fasta sequences using biopython
    sequences = list(SeqIO.parse(scaffold, "fasta"))
    # parse into a dict where the key is the scaffold, and the value is the sequence
    genome = {}
    for s in sequences:
        # get sequence object
        seq = s.seq
        s_size = len(seq)
        # get scaffold name
        scaffold = s.id
        genome[scaffold] = str(seq)
    print("******** making fasta *******")
    short = 0
    # count how many are not found...
    bad = []
    # now go through the gene list and for each get the correct sequence and write to the new file
    with open(newname, "w+") as f:
        for gene in genelist:
            # get info
            if gene in info_dict:
                info = info_dict[gene]
                # if inhibiting, just grab the entire sequence - start-end for the scaffold, strand doesn't matter
                # TODO - this is the hard part,
                # if activating want -136 to -36 upstream from the start (if "+") or end (if "-")...hopefully will be on same scaffold...
                print("Looking at gene "+gene+", start: "+str(info['start'])+", end: "+str(info['end'])+", strand: "+str(info['strand']))
                # get start and ends you want
                if inhibit:
                    # for inhibition - want full cds, strand doesn't matter
                    start = info['start']
                    end = info['end']
                else:
                    # for activation want -130 to -30, upstream of start TODO trying -10 to -400
                    if info['strand']=="+":
                        start = info['start'] - 130
                        end = info['start'] - 30
                        print("activating- gene " + gene + " on + strand " + str(start) + " - " + str(end))
                    else:
                        start = info['end'] + 30
                        end = info['end'] + 130
                        print("activating- gene " + gene + " on - strand " + str(start) + " - " + str(end))
                # check- make sure start and end are not out of bounds for the scaffold
                scaffold_sequence = genome[info['scaffold']]
                if start < 1:
                    print("ERROR: start is "+str(start)+" for gene: "+gene+"|"+info['name']+" - could not retrieve input sequence")
                    break
                if end > len(scaffold_sequence):
                    print("ERROR: end is " + str(end) + ", and scaffold length is "+str(len(scaffold_sequence))+" for gene: " + gene + "|" + info[
                        'name'] + " could not retrieve input  sequence")
                    break
                # get the sequence substring
                seq = scaffold_sequence[start:end]
                # test sequence length
                # TODO changed from 60 as the minimum length to always add the 2nd CDS - leave...seams to work, unless too short...
                if len(seq) < 60:
                #if True:
                    short += 1
                    print(gene + "|" + info['name'] +" is short: "+str(len(seq)))
                    # now get larger area if doing inhibition
                    if inhibit and gene in cds2:
                        # TODO - want the start to be the start of the first exon, end is the end of the 2nd exon
                        # assumption: these should always be contiguous
                        cds2_info = cds2[gene]
                        # make sure that cds2 is on same scaffold as cds1
                        if cds2_info['scaffold'] == info['scaffold']:
                            # start is the same , end is extended,
                            # for this gff, this depends on the strand
                            if cds2_info['strand'] == "+":
                                end = cds2_info['end']
                            else:
                                start = cds2_info['start']
                            seq = scaffold_sequence[start:end]
                            print("Using cds2, sequence span is: "+str(start)+" - "+str(end)+", length: "+str(len(seq)))
                            # test sequence length
                            if len(seq) < 60:
                                print("ERROR: the selected region is still too short! ("+str(len(seq))+")")
                                # 6 of the 11 short ones passed, but there are still 5 more left
                                # now make the end the end of the 3rd cds
                                if gene in cds3:
                                    cds3_info = cds3[gene]
                                    if cds3_info['strand'] == "+":
                                        end = cds3_info['end']
                                    else:
                                        start = cds3_info['start']

                                    seq = scaffold_sequence[start:end]
                                    # test sequence length
                                    if len(seq) < 60:
                                        print("ERROR: the selected region is still too short! (" + str(len(seq)) + ")")
                                else:
                                    print("ERROR 3: this gene does not have a  3rd CDS ("+gene+")")

                        else:
                            print("ERROR: the 1st and 2nd CDSs are on differnt scaffolds!")


                # print the name in the file: number|name same as in the CDS fasta file
                f.write(">" + gene + "_" + info['name'] +"_"+info['scaffold']+ "\n")
                f.write(seq+"\n")
            else:
                bad.append(gene)
    print("there were " + str(short) + " short sequences")
    print("there were " + str(len(bad)) +" bad sequences: "+str(bad))



def run(newname, scaffold, gff, genelist, cds, inhibit):
    # need to map between gene number in Transcriptional_factors_in_1779v2.cvs (genelist) and the gene name in (cds)
    # first parse cds into a dict - with the key = gene_name, value = gene_number
    all_cds = {}
    sequences = list(SeqIO.parse(cds, "fasta"))
    for s in sequences:
        full_name = s.id
        print("cds id: "+ full_name)
        parts = full_name.split("|")
        num = parts[0]
        name = parts[1]
        all_cds[name] = num

    # now parse the genelist
    genes = [] # just a list of the gene numbers we need to get sequence for
    with open(genelist) as glist:
        for line in glist:
            # split line
            parts = line.split(",")
            genes.append(parts[0])
    # now get scaffold, start, end and strand from gff for both first firs cds and first exon for each gene from the gff
    # will have to parse the gff twice - first to gte the first CDS, then to get the first exon
    # The first exon will have the same end as the first cds if it is on the '+' strand,
    # or the same start as the cds if it is on the '-' strand

    # put each into a dict
    exons = {} # the key will be the gene number, and the value will be a dict of the othere values
    cds = {}
    cds2 = {}
    cds3 = {}
    print("parsing 1st and 2nd CDSs from gff")
    with open(gff) as f:
        for line in f:
            parts = line.split("\t")
            # check what type it is cds, exon - we don't care about the rest
            if parts[2] == "CDS":
                # for CDS lines, we only want the line that has "exonNumber 1" in it's name column
                desc = parts[8]
                #print("parsing: "+desc)
                if (desc.find("exonNumber 1;") != -1):
                    # parse desc
                    #print("is first cds")
                    d_parts = desc.split(";")
                    for p in d_parts:
                        p = p.strip()
                        if p.startswith("name"):
                            name = p.split(' ')[1]
                            # remove the quotes around the name
                            name = name[1:len(name) - 1]
                            # put in dict - key will be gene number mapped by all_cds dict
                            ####  not all genes in the gff file are in the geneCatalog file(or cds file) - so first check if the gene name is in the all_cds dict
                            # if not, just skip it, it won't be in the gene list because that was created with the all cds list genes only
                            if name in all_cds:
                                print("adding first cds for gene "+all_cds[name]+", start: "+parts[3])
                                cds[all_cds[name]] = {'scaffold': parts[0], 'start': int(parts[3]), 'end': int(parts[4]), 'strand': parts[6], 'name': name}
                elif (desc.find("exonNumber 2;") != -1):
                        # same as above but put in the cds2 dict
                        # parse desc
                        #print("is first cds")
                        d_parts = desc.split(";")
                        for p in d_parts:
                            p = p.strip()
                            if p.startswith("name"):
                                name = p.split(' ')[1]
                                # remove the quotes around the name
                                name = name[1:len(name) - 1]
                                # put in dict - key will be gene number mapped by all_cds dict
                                ####  not all genes in the gff file are in the geneCatalog file(or cds file) - so first check if the gene name is in the all_cds dict
                                # if not, just skip it, it won't be in the gene list because that was created with the all cds list genes only
                                if name in all_cds:
                                    cds2[all_cds[name]] = {'scaffold': parts[0], 'start': int(parts[3]), 'end': int(parts[4]), 'strand': parts[6], 'name': name}
                elif (desc.find("exonNumber 3;") != -1):
                        # same as above but put in the cds2 dict
                        # parse desc
                        #print("is first cds")
                        d_parts = desc.split(";")
                        for p in d_parts:
                            p = p.strip()
                            if p.startswith("name"):
                                name = p.split(' ')[1]
                                # remove the quotes around the name
                                name = name[1:len(name) - 1]
                                # put in dict - key will be gene number mapped by all_cds dict
                                ####  not all genes in the gff file are in the geneCatalog file(or cds file) - so first check if the gene name is in the all_cds dict
                                # if not, just skip it, it won't be in the gene list because that was created with the all cds list genes only
                                if name in all_cds:
                                    cds3[all_cds[name]] = {'scaffold': parts[0], 'start': int(parts[3]), 'end': int(parts[4]), 'strand': parts[6], 'name': name}

    # now, if activating (not inhibit), parse a second time to get the first exons
    if inhibit == 'False':
        print("getting exon info...")
        #first parse the gff again to get the first exons
        with open(gff) as f:
            for line in f:
                parts = line.split("\t")
                # check what type it is cds, exon - we don't care about the rest
                if parts[2] == "exon":
                    # check if is the first exon...
                    # get name from name column
                    desc = parts[8]
                    # parse desc
                    d_parts = desc.split(";")
                    for p in d_parts:
                        p = p.strip()
                        if p.startswith("name"):
                            name = p.split(' ')[1]
                            name = name[1:len(name) - 1]
                            # now get the strand, start and end fro the cds dict
                            #cds_dict = cds[name]
                            #### TODO - since not all CDSs in gff are in all_cds if not in and the gene list was created from all_cds list, just skip if not found
                            if  name in all_cds:
                                cds_dict = cds[all_cds[name]]
                                cds_start = cds_dict['start']
                                cds_end = cds_dict['end']
                                cds_strand = cds_dict['strand']
                                # now check if its the first exon
                                # The first exon will have the same end as the first cds if it is on the '+' strand,
                                # or the same start as the cds if it is on the '-' strand
                                #if ((parts[6] == "+") and (int(parts[4])==cds_end)) or ((parts[6] == "-") and (int(parts[3])==cds_start)):
                                #    print("gene: " + all_cds[name] + "|" + name)
                                #    # put in dict - key will be gene number mapped by all_cds dict
                                #   exons[all_cds[name]] = {'scaffold': parts[0], 'start': int(parts[3]), 'end': int(parts[4]), 'strand': parts[6], 'name': name}
                                if int(parts[3]) <= cds_start and int(parts[4]) >= cds_end:
                                    # this is to catch the 23 etra genes where the end points of the first CDS and first exon don't match
                                    # in these carses the first CDS should be contained by the first exon
                                    print("gene: " + all_cds[name] + "|" + name + " -  cds_start: "+str(cds_start)+", exon start: "+parts[3])
                                    # put in dict - key will be gene number mapped by all_cds dict
                                    exons[all_cds[name]] = {'scaffold': parts[0], 'start': int(parts[3]),
                                                            'end': int(parts[4]), 'strand': parts[6], 'name': name}
        #print("test: "+exons['652434'])
        # now get the sequence and create the input file
        create_input_file(newname, exons, genes, scaffold, cds2, cds3, False)

    else:
        # if inhibiting, get the first CDS info for each gene from the gff - so nothing else needs to be done
        create_input_file(newname, cds, genes, scaffold, cds2, cds3, True)

    # now go through the list and of genes and create the fasta - getting the sequence from the scaffold fasta file



def main():
    '''
    Creates input files for ccTop
    this script is specifically for Yando's project
    input is:
        newname - is the base name for the output file it will be called newname.fa
        scaffold_fa is the fasta file with the scaffolds: Nanoce1779_2_AssemblyScaffolds.fasta
        gff file is : Nanoce1779_2_all_genes_20180119.gff
        genelist_file is the csv of the file Yando sent us: Transcriptional_factors_in_1779v2.cvs
        CDS_fasta is the file Yando used to extract the TF gene numbers: Nanoce1779_2_GeneCatalog_CDS_20180119.fasta
        inhibit is a boolean - True for inhibiting gRNA, False for activating gRNA

        For inhibiting: for each gene, use the sequence associated with the first CDS region - if shorter than 60bp, from the sart of the first CDS to the end of the 2nd CDS
        For activating: for each gene, use the sequence -136 to -36 upstream (5') to the first exon

    :return:
    '''
    if len(sys.argv) < 7:
        print(
            "Usage: python getNanoceRegions.py newname scaffold_fa gff_file genelist_file CDS_fasta inhibit \n Note the last argumanet must be True or False ")
    else:
        newname = sys.argv[1]
        scaffold = sys.argv[2]
        gff = sys.argv[3]
        genelist = sys.argv[4]
        cds = sys.argv[5]
        inhibit = sys.argv[6]

        run(newname, scaffold, gff, genelist, cds, inhibit)
        # print("done!")


if __name__ == "__main__":
    main()