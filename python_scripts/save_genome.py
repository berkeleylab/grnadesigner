from Bio import SeqIO
import sys
import os
import shutil
import subprocess
import gzip
import zipfile
import python_scripts.gff2bedFiles as gff2beds


def process_fa(data_path, uuid, seq_file, ann_file):
    ''' First validate that the fasta is usable, then create the bed file from the annotation file'''
    #1. open fasta in biopython - get a list of the ids - chromosome names. If can't open in biopython- error
    #2. open gff - make sure that the first column only includes a refrence to the first part of the fa ids...(chromosome names)
    print("processing fasta")
    msg = ""
    ok = True
    # Step 1. open fasta with biopython, get a list of chromosome names:
    chromosomes =[]
    try:
        pth = os.path.join(data_path, seq_file)
        for seq_record in SeqIO.parse(pth, "fasta"):
            print(seq_record.id)
            chromosomes.append(seq_record.id)
        # if all is good, cp the seq file to <uuid>.fa
        shutil.copyfile(os.path.join(data_path, seq_file), os.path.join(data_path, uuid + ".fa"))
        print("The fasta file was good")
        # cp the gff file to <uuid>.gff3
        shutil.copyfile(os.path.join(data_path, ann_file), os.path.join(data_path, uuid + ".gff3"))
        print("Copied gff")
    except:
        ok = False
        msg = "Invalid Fasta. "
        print("ERROR: opening fasta file...")


    # Step 2. validate GFF, and Fasta files- do they go together?
    # if they do there should be the same chromosomes in the GFF (first column) as the fasta file
    # TODO- using the arabidopsis genome the chromosomes in the fasta are[1,2,3,4,5,mitochondira, chloroplast]
    ## but the gff has [Chr1, Chr2, Chr3, Chr4, Chr5, ChrM, ChrC]
    ## so am skipping this now - may want to create the gff with the sequence in it,
    ## or change the <uuid>.fa to match the gff... will see what I need for tools used to etract sequence
    '''
    if ok:
        validated = True
        try:
            pth = os.path.join(data_path, ann_file)
            with open(pth) as gff:
                for line in gff:
                    # ignore lines starting with #
                    if not line.startswith("#"):
                        p = line.split('    ')
                        chr = p[0]
                        # check if in chromosomes:
                        if chr not in chromosomes:
                            validated = False
            print("fa/gff validated: " + str(validated))
            # check if validated
            if not validated:
                ok = False
                msg = msg + "Chromosome names in GFF3 did not match chromosome names in FASTA. "
        except:
            ok = False
            msg = msg + " Error parsing GFF3. "
            print("ERROR: parsing GFF3")
    '''
    # Step 3: TODO create bed file from GFF - now this will be done from the process function for both gb and fa using the gff
    # return boolean, ok, and msg (empty string if all is well)
    return ok, msg


def fix_gb_locus_line(dir_path, uuid, seq_file):
        ''' try fixing the locus line and overwriting the gb file - return the name of the new file'''
        # create new file
        print("creating file with fixed locus line for biopython")
        default_first29 = "LOCUS       DEFAULT          "
        # example_locus_line = "LOCUS       DEFAULTXXX              1000 bp    DNA     circular     01-JAN-2021"
        newfilename = uuid +".gb"
        new = os.path.join(dir_path, newfilename)
        with open(new, 'w') as fixed:
            # open file and open 2nd file
            orig = os.path.join(dir_path, seq_file)
            first_line = True
            with open(orig, 'r') as old:
                for line in old:
                    if first_line:
                        # write default locus line instead of the one from the file
                        # TODO- warning: since I am creating a new file and don't need the locus line,
                        # I am simply adding a "dummy" locus line that will not cause biopython to throw an exception
                        # The original "bad" genbank file will be included in the submission zip file
                        # need to parse length of sequence - should be third part
                        parts = line.split()
                        l = "1000"
                        if len(parts) > 2:
                            l = parts[2]
                            print("got seq length")
                        else:
                            print("could not parse seq length")
                        # add spaces to front of l until len(l) is 11 (will be spaces 30-40 in locu line)
                        r = 11 - len(l)
                        for n in range(r):
                            l = " " + l
                        # create default locus line with correct sequence length
                        default_locus_line = default_first29 + l + " bp    DNA     circular     01-JAN-2021\n"
                        # write to fixed file
                        fixed.write(default_locus_line)
                        first_line = False
                    else:
                        fixed.write(line)
        return newfilename


def gb2fa(data_path, uuid, seq_file):
    ''' open the genbank file, and write the fasta file as uuid.fa'''
    print("creating fasta from gb")
    gb = open(os.path.join(data_path, seq_file), "rU")
    sequences = SeqIO.parse(gb, "genbank")

    # and it's crude, parse the first  line from the fasta file to be used in the bed file
    fa_pth = os.path.join(data_path, uuid + ".fa")
    fasta = open(fa_pth, "w")
    count = SeqIO.write(sequences, fasta, "fasta")
    fasta.close()

    print("fasta has " + str(count) + " sequences")
    return fa_pth


def process_gb(data_path, uuid, seq_file):
    '''first create a fasta file, an also grab the acc.ver number, then create bed file '''

    # Step 1 - get fasta file, and ensure that we can use biopython on the genbank (biocode uses biopython to convert to gff)
    # try opening gb with boipython - if this fails try fixing the locus line and trying again
    # if it still fails throw an error- this gb file cannot be processed
    print("processing gb")
    ok = True
    msg = ""
    locus_fixed = False
    try:
        fa_file = gb2fa(data_path, uuid, seq_file)

        # if all goes well, we will copy the gb file in the directory so there is a <uuid>.gb that is either the fixed filr or the original
        # in anycase we will know that the <uuid>.gb file is biopython safe
        # if there is an exception, this file will be created by the fix_gb_locus_line method
        shutil.copyfile(os.path.join(data_path, seq_file), os.path.join(data_path, uuid+".gb"))
        print("The GenBank file was good")
    except:
        print("bad GenBank, will try to fix LOCUS line")
        # probably a bad gb file - try fixing the locus line and try again
        fixed_filename = fix_gb_locus_line(data_path, uuid, seq_file)
        try:
            fa_file = gb2fa(data_path, uuid, fixed_filename)
            # log the issue
            print("There was a problem with the LOCUS line in the Genbank file, but this was fixed.")
            locus_fixed = True
        except Exception as e:
            ok = False
            print("Error trying to get fasta from fixed file: "+str(e))
            msg = msg + " Could not get open GenBank file for pre-processing."

    # Step 2. make a gff3 file from the genbank file
    if ok:
        print("step 2, create gff")
        try:
            input = os.path.join(data_path, uuid + '.gb')
            output = os.path.join(data_path, uuid + '.gff3')
            process = subprocess.Popen(['python',
                                        'python_scripts/gb_to_gff.py',
                                        input,
                                        output,]).wait()
        except Exception as e:
            ok = False
            msg = msg + " Could not convert Genbank to GFF3"
            # TODO catch exception, make message for end user
            print("ERROR: when converting gb to gff3: "+str(e))

    return ok, msg


def make_bed_file_from_gb(gb_file, output_name, acc):
    ''' WARNING: this method only saves CDS features to the bed file

     can try biocode/convert_genbank_to_gff3.py then biocode.convert_gff3_to_bed.py
     But the gb->gff is only tested on prokaryote, non spliced genes'''
    #TODO - this is old code- only works for prokayotes and gb with CDS only- not using
    print("creating bed from genbank")
    outf = open(output_name + '.bed', 'w')
    for record in SeqIO.parse(open(gb_file, "rU"), "genbank"):
        for feature in record.features:
            # if feature.type == 'gene': <- this included ncRNAs so using CDS instead
            if feature.type == 'CDS':
                start = feature.location.start.position
                stop = feature.location.end.position
                try:
                    name = feature.qualifiers['gene'][0]
                except:
                    # some features only have a locus tag
                    name = feature.qualifiers['locus_tag'][0]
                if feature.strand < 0:
                    strand = "-"
                else:
                    strand = "+"
                # if the name has a single quote in it, CCTop will not process it,
                # so I am replacing ' with _prime_ here
                name = name.replace("'", "_prime_")
                bed_line = acc + "\t{0}\t{1}\t{2}\t1000\t{3}\t{0}\t{1}\t65,105,225\n".format(start, stop, name,
                                                                                             strand)
                outf.write(bed_line)
    outf.close()

def gunzip_file(path, file):
    ''' gunzip files with .gz extension '''
    ok = True
    print("gunzip file: " + file)
    out_file_name = file[0:len(file) - 3]
    print("outputfile: "+out_file_name)
    print("path: "+path)
    o_pth = os.path.join(path, out_file_name)
    # op = open(o_pth, "w")

    ip = os.path.join(path, file)
    try:
        # Using gzip fails with large files at least on Mac OS
        # with [Errno 22] Invalid argument
        # - the subprocess works ok
        #
        # with gzip.open(ip, "rb") as ip_byte:
        #     op.write(ip_byte.read().decode("utf-8"))
        #
        # op.close()
        if not os.path.exists(o_pth):
            process = subprocess.Popen(['gunzip', ip]).wait()
    except Exception as e:
        ok = False
        out_file_name = str(e)

    return ok, out_file_name


def unzip_file(path, file):
    ''' unzip files with .zip extension '''
    print("unzip file: "+file)
    out_file_name = file[0:len(file) - 4]
    pth = os.path.join(path, file)
    with zipfile.ZipFile(pth, "r") as zip_ref:
        zip_ref.extractall(path)

    return out_file_name


def process(data_path, uuid, seq_file, ann_file, dbname, dbhost, dbuser, dbpass, script_path, bowtie_path):
    '''process and validate uploaded files'''
    print("processing genome files...")
    ####################################
    #### Uncompress files if needed ####
    ####################################

    # first un-compress files if needed
    # TODO- handel if there was an issue un-compressing
    if seq_file.lower().endswith(".gz"):
        print("sequence file is compressed with gz")
        # gunzip this file, and rename seq_fil to point to the unziped version
        ok, seq_file = gunzip_file(data_path, seq_file)
        print("is OK? "+ str(ok)+", "+seq_file)
    elif seq_file.lower().endswith(".zip"):
        print("sequence file is compressed with zip")
        # unzip this file
        seq_file = unzip_file(data_path, seq_file)
    # check annotation file
    if ann_file.lower().endswith(".gz"):
        print("annotation file is compressed with gz")
        # gunzip this file, and rename seq_fil to point to the unziped version
        ok, ann_file = gunzip_file(data_path, ann_file)
        print("is OK? " + str(ok))
    elif ann_file.lower().endswith(".zip"):
        print("annotation file is compressed with zip")
        # unzip this file
        ann_file = unzip_file(data_path, ann_file)

    ###############################################
    #### process based on annotation file type ####
    ###############################################
    # First see what the original file type should be
    # if there is no annotation file, it is a genbank
    print("check annotation file type...")
    orig_file_type = "GFF3" # might be gff3
    if ann_file == "NA":
        orig_file_type = "GenBank"

    print("ann file type: "+orig_file_type)
    # todo - error catching and messages for user feedback needed
    # if genbank - get fasta file and create bed -
    if orig_file_type == "GenBank":
        ok, msg = process_gb(data_path, uuid, seq_file)
    else:
        ok, msg = process_fa(data_path, uuid, seq_file, ann_file)

    ###################################################
    #### create genes and exons bed files from gff ####
    ###################################################

    print("everything good? "+str(ok))
    # now create bed file from gff - using code fro CCTop but moved into python scripts
    if ok:
        print("looks good so far- will create bed from gff ")
        try:
            gff2beds.create_files(os.path.join(data_path,uuid+".gff3"), uuid, data_path)

        except Exception as e:
            ok = False
            msg = msg + " Could not convert GFF3 to BED files."
            # TODO catch exception, make message for end user - not being caught properly!!!
            print("ERROR: when converting gff3 to bed: "+str(e))



    ###########################################################
    #### Run Bowtie build to make indexes needed for CCTop ####
    ###########################################################
    ### this has been moved to create_genome_annotation_db.py so that it will be done asynchronously ####
    # now run bowtie on fasta files to create indexes needed by CCTop
    # if ok:
    #     print("looks good so far- will create indexes using bowtie ")
    #     try:
    #         # cmnd: bowtie-build -r -f ${uuid}.fa ${uuid}.index
    #         input = os.path.join(data_path, uuid + '.fa')
    #         output = os.path.join(data_path, uuid + '.index')
    #         process = subprocess.Popen(['bowtie-build',
    #                                     '-r',
    #                                     '-f', input, output]).wait()
    #         output, error = process.communicate()
    #         if process.returncode != 0:
    #             print("bowtie failed %d %s %s" % (process.returncode, output, error))
    #         print("bowtie is running...")
    #     except:
    #         #ok = False
    #         #msg = msg + " Could not create index files."
    #         # catch exception, make message for end user
    #         print("Caught exception -  when running bowtie build - ignoring")

    ############################################################################
    #### Create sqlite database from gff and create gene/feature json files ####
    ############################################################################
    if ok:
        print("looks good, will create sqlite db from gff")
        try:
            # TODO - no hard paths - must do in subprocess because takes a long time to generate the database
            # script_path = '/Users/simi/Projects/gRNA/grna/python_scripts/'
            ann_db_script = os.path.join(script_path, 'create_genome_annotation_db.py' )
            # db_filename = uuid+".db"
            input = os.path.join(data_path, uuid + '.gff3')
            output = os.path.join(data_path, uuid + '.db')
            fasta_file = os.path.join(data_path, uuid + '.fa')
            print("starting job to create sqlite db and json files...")
            process1 = subprocess.Popen(['python',
                                         ann_db_script,
                                         data_path, uuid, uuid + ".gff3", dbname, dbhost, dbuser, dbpass, fasta_file, bowtie_path ])
            print("creating genome annotation db and ui files")
        except Exception as e:
            ok = False
            msg = msg + " Could not create sql lite db."
            # TODO catch exception, make message for end user - not being caught properly!!!
            print("ERROR: when converting gff3 to sqlite: " + str(e))

    return ok, msg, orig_file_type


def main():
    '''
    this script takes the sequence file and (if not using gb) annotation file (put the string "NA" in place of ann file if not provided, and creates a fasta file and bed file
    the data_path tells it what directory to store everything and where the files are uploaded, the uuid will be used to name the files
    the files will be called uuid.fa and uuid.bed
    also, run bowtie on the fa to get the indexes CCTop will need
    if any errors return an error message, also return original file type for database entry
    :return:
    '''
    if len(sys.argv) < 11:
        print("Usage: python save_genome.py data_path uuid seq_filename annotation_filename dbname dbhost dbuser dbpass script_path bowtie_path")
    else:
        data_path =sys.argv[1]
        uuid = sys.argv[2]
        seq_file = sys.argv[3]
        ann_file = sys.argv[4]
        dbname = sys.argv[5]
        dbhost = sys.argv[6]
        dbuser = sys.argv[7]
        dbpass = sys.argv[8]
        script_path = sys.argv[9]
        bowtie_path = sys.argv[10]
        process(data_path, uuid, seq_file, ann_file, dbname, dbhost, dbuser, dbpass, script_path, bowtie_path)
        print("done!")


if __name__ == "__main__":
    main()