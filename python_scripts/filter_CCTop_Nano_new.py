__author__ = 'simi'
#!/usr/bin/env python
import sys
from operator import itemgetter
import json

import RNA
from Bio import SeqIO

import parse_CCTop as CCTop
import eval_folding as fold


def filter(fasta, directory, scaffold, protospacer_end, number_wanted, project_name, inhibit, stringency):

    # open file for human readable reporting - will be put into output directory
    with open(directory+"/"+project_name+"_report.txt", "w") as report:
        genes_no_gRNAs=[]
        genes_not_enough_gRNAs=[]
        # open file for -output csv
        with open(directory+"/"+project_name+".csv", "w") as output:
            #output = open(directory+"/"+project_name+".csv", "w")
            # print header
            output.write("name,protospacer,PAM,start,end,strand\n")
            # first open the fasta file and get a list of gene names
            genes = []
            fasta = list(SeqIO.parse(directory + "/" + fasta, "fasta"))
            for g in fasta:
                # for each gene get the name and put in list
                # ### TODO ***** because CCTop changes the name by removing "#" hash signs, we will do this here ****
                identifier = g.id.replace("#", "")
                genes.append(identifier)

            # now for each gene in the list - process
            for g in genes:
                print("\n\nfor gene "+g)
                # TODO get the gene number, name and scaffold from gene name
                # first get scaffold - the last bit of the name
                s_start = g.find("scaffold_")
                g_scaffold = g[s_start: len(g)]
                # the number is the first part of the  name
                parts = g.split("_")
                g_num = parts[0]
                # the name is the part between num_*****_scaffold_N <- there may be underscores...
                g_name = g[len(g_num)+1:s_start - 1]
                print("num: "+g_num+", name: "+g_name+", scaffold: "+g_scaffold)

                # first parse the CCTop output to get the list of possible gRNAs
                gRNAs, off_targets = CCTop.parse(g, directory)
                # check if list is empty
                if len(gRNAs) < 1:
                    print("ALERT: "+ g+" - no protospacers were found by ccTop")
                    report.write("ALERT: "+ g+" - no protospacers were found by ccTop\n")
                    genes_no_gRNAs.append(g)
                else:
                    bad_count = 0
                    bad_protospacers = [] # list of ids that are bad....
                    for gRNA in gRNAs:
                        print("\nevaluating folding for "+gRNA['id'])
                        good = False
                        print ("############################")
                        print ("### checking position ###")
                        print ("############################")
                        # TODO fist check if criteria for activation or inhibition are satisfied
                        if inhibit == "True":

                            print("check: " + gRNA['gene_id'] + ", should be: " + g_num + "|" + g_name)
                            gene_id = gRNA['gene_id'].strip()
                            print("name same? "+str(gene_id == g_num.strip() +"|"+g_name.strip()))
                            print("scaffold same? "+str(gRNA['chromosome'].strip() == g_scaffold.strip()))
                            print("position Exonic? "+ str(gRNA['position'].strip() == 'Exonic'))

                            # should be on same chromosome/scaffold, be position=exonic and in same gene id
                            # evaluate the protospacers for RNA folding
                            # TODO - since may be taking cds1-cds2, might have intergenic region, also there are some genes anotated inside other genes...
                            #if gRNA['chromosome'].strip() == g_scaffold.strip():
                            # for strict, check gene id, exonic and scaffold, otherwise just check the scaffold
                            if (stringency == "strict" and (gene_id == g_num.strip() +"|"+g_name.strip()) and (gRNA['chromosome'].strip() == g_scaffold.strip()) and (gRNA['position'].strip() == 'Exonic'))  or (stringency != "strict" and  gRNA['chromosome'].strip() == g_scaffold.strip()):
                                # this is good
                                good = True
                                print("position good")
                            else:
                                bad_count += 1
                                gRNA['score'] = -1000
                                # add to bad protospacers list
                                bad_protospacers.append(gRNA)
                                print("position bad")

                        else:
                            # this means that we are trying to activate -
                            # so shound be on same scaffold, and be Intergeninc, otherwise will be inhibiting a different gene
                            # TODO - requiring these to be in Intergenic regions may be too stringent
                            # if gRNA['chromosome'].strip() == g_scaffold.strip():
                            #if (gRNA['chromosome'].strip() == g_scaffold.strip()) and (gRNA['position'].strip() == 'Intergenic'):
                            # if strict, check if Intergenic and proper chomosome, otherwise just check the chromosome
                            if (stringency == "strict" and gRNA['chromosome'].strip() == g_scaffold.strip()) and (gRNA['position'].strip() == 'Intergenic') or (stringency != "strict" and gRNA['chromosome'].strip() == g_scaffold.strip()):
                                # this is good
                                good = True
                                print("position good")
                            else:
                                bad_count += 1
                                # need to remove from list? or just mark score as -1
                                gRNA['score'] = -1000
                                # add to bad protospacers list
                                bad_protospacers.append(gRNA)
                                print("position bad")

                        # now check the mismatches of the gRNA
                        # you want it to have 3 or less mismatches, and only 1 proximal to the PAM site
                        # the more mismatches proximal to the PAM site, the less likely it is to bind
                        if good:
                            print ("############################")
                            print ("### checking mismatches ###")
                            print ("############################")
                            mm = int(gRNA['MM'])
                            if mm > 3:
                                # this is bad - won't bind
                                bad_count += 1
                                good = False
                                gRNA['score'] = -1000
                                # add to bad protospacers list
                                bad_protospacers.append(gRNA)
                                print("bad mismatches - "+gRNA['MM'])
                            elif mm == 3:
                                # check where the mismatches are
                                # at least one must be proximal to the PAM site - within 10 bases
                                # the fewer that are distal to the PAM site, the better
                                # need the alignment, and the protospacer_end
                                alignment = gRNA['alignment']
                                # the alignment looks like this:|||-||||[||||||||||||]PAM
                                # where "_" are mismatches - so need to check for mismatches in between []
                                proximal = alignment[alignment.find("[") + 1:alignment.find("]")]
                                if proximal.count("-") < 1:
                                    #  this means that only one or less of the mismatches are proximal to the PAM site
                                    print("good mismatches -  only 1 or less proximal to the PAM" + gRNA['MM'])
                                else:
                                    bad_count += 1
                                    good = False
                                    gRNA['score'] = -1000
                                    # add to bad protospacers list
                                    bad_protospacers.append(gRNA)
                                    print("bad mismatches - " + gRNA['MM']+ " at least 2 are proximal to the PAM site")
                            else:
                                print("mismatches ok - "+ gRNA['MM'])

                        # now check if all its off-site targets are ok
                        # if any are not - reject the gRNA
                        # for strict & medium: if there are 3 or less mismatches - 2 must be proximal to the - less than 3 missmatches are rejected
                        # for loose: if there are 3 mm, 1 must be proximal to the PAM, if there are 2 mm they both must be proximal to the PAM
                        prox_3mm_min = 2
                        if stringency == "loose":
                            prox_3mm_min = 1
                        if good:
                            print ("############################")
                            print ("### checking off targets ###")
                            print ("############################")
                            # get list of off targets for this gRNA
                            off_target_list = off_targets[gRNA['id']]
                            for off_target in off_target_list:
                                # for the gRNA to be good, all it's offsite targets must be non-functional
                                # so they all must have either more than 3 MM
                                # and if it has 3 mismatches, all must be distal to the PAM site,
                                mm = int(off_target['MM'])
                                if mm < 2:
                                    bad_count += 1
                                    good = False
                                    gRNA['score'] = -1000
                                    # add to bad protospacers list
                                    bad_protospacers.append(gRNA)
                                    print("bad off target site: mismatches - " + off_target['MM'])
                                    break
                                elif mm == 3:
                                    alignment = off_target['alignment']
                                    # the alignment looks like this:|||-||||[||||||||||||]PAM
                                    # where "_" are mismatches - so need to check for mismatches in between []
                                    proximal = alignment[alignment.find("[") + 1:alignment.find("]")]
                                    # for strict/medum stringency, prox_3mm_min will be 2
                                    # for loose prox_3mm_min will be 1
                                    if proximal.count("-") < prox_3mm_min:
                                        # this is bad
                                        bad_count += 1
                                        good = False
                                        gRNA['score'] = -1000
                                        # add to bad protospacers list
                                        bad_protospacers.append(gRNA)
                                        count = proximal.count("-")
                                        print("proximal: "+proximal+", count: "+str(count)+" ? "+str(proximal.count('-') < 1))
                                        print("bad off target site: mismatches - " + off_target['MM']+", with at less than 2  proximal to the PAM site")
                                        break
                                elif mm == 2:
                                    if stringency != "loose":
                                        # this is bad
                                        bad_count += 1
                                        good = False
                                        gRNA['score'] = -1000
                                        # add to bad protospacers list
                                        bad_protospacers.append(gRNA)
                                        print("bad off target site: mismatches - " + off_target['MM'] )
                                        break
                                    else:
                                        alignment = off_target['alignment']
                                        # the alignment looks like this:|||-||||[||||||||||||]PAM
                                        # where "_" are mismatches - so need to check for mismatches in between []
                                        proximal = alignment[alignment.find("[") + 1:alignment.find("]")]
                                        if proximal.count("-") < 2:
                                            # this is bad - w for off site targets with 2 mismatches both must be proximal
                                            bad_count += 1
                                            good = False
                                            gRNA['score'] = -1000
                                            # add to bad protospacers list
                                            bad_protospacers.append(gRNA)
                                            print("bad off target site: mismatches - " + off_target[
                                                'MM'] + ", with less than 2 proximal to the PAM site")
                                            break

                        if good:
                            print ("############################")
                            print ("#### evaluating folding ####")
                            print ("############################")
                            # do scoring
                            protospacer = gRNA['seq']
                            full_seq, structure, folding_score, delta_g, = fold.evaluate(protospacer, scaffold, protospacer_end)
                            # add to gRNA dict
                            gRNA['rna_sequence'] = full_seq
                            gRNA['rna_structure'] = structure
                            gRNA['folding_score'] = folding_score
                            gRNA['delta_g'] = delta_g

                            print ("############################")
                            print ("######## scoring ###########")
                            print ("############################")
                            # before scoring check that there are no "0" scores - force to 1, except for CRISPERator scor, st that to 0.001
                            if gRNA['CCTop_score'] == 0:
                                print("CCTop score was zero")
                                gRNA['CCTop_score'] = 1
                            if gRNA['folding_score'] == 0:
                                print("folding score was zero")
                                gRNA['folding_score'] = 1
                            if gRNA['delta_g'] == 0:
                                print("delta G score was zero")
                                gRNA['delta_g'] = 1.0
                            if gRNA['CRISPRater_score'] == 0:
                                print("CRISPRater score was zero")
                                gRNA['CRISPRater_score'] = 0.001

                            # now create the score for the gRNA
                            print("for gRNA " + gRNA['id'] + ": CCTop score: " + str(
                                    gRNA['CCTop_score']) + ", CRISPERater score: " + str(
                                    gRNA['CRISPRater_score']) + ", folding score: " + str(
                                    gRNA['folding_score']) + ", delta G: " + str(gRNA['delta_g']))
                            print("folding: ")
                            print(gRNA['rna_structure'])

                            # this will just be the prduct of the CCTop_score, CRISPERater_score, the folding_score (removed the delta_g - the negative number is skewing the score score)
                            gRNA['score'] = gRNA['CCTop_score'] * gRNA['CRISPRater_score'] * folding_score
                            print("score: " + str(gRNA['score']))

                    # now return the list sorted by score (descending)
                    print("sorting list... ")
                    sorted_list =  sorted(gRNAs, key=itemgetter('score'), reverse=True)

                    # write output
                    # now check if there are at least as many "good" ones as "wanted"
                    good_found = len(sorted_list) - bad_count
                    if good_found < number_wanted:
                        print("ALERT: " + g + " - only "+str(good_found)+"  good protospacers were found")
                        report.write("ALERT: " + g + " - only "+str(good_found)+" good protospacers were found\n")
                        genes_not_enough_gRNAs.append(g)
                        # print  to output
                        for x in sorted_list:
                            # check if has score higher than -1000
                            if x['score'] > -1000:
                                # the name of the protospacer will be the gene name+ "_" and the CCTop id (i.e. T1...TN)
                                output.write(g + "_" + x['id'] + "," + x['seq'] + "," + x['PAM'] + "," + x['start'] + "," + x['end'] + "," + x['strand'] + "\n")
                    else:
                        # write only the first number_wanted to the output
                        # for x in sorted_list[0:number_wanted]:
                        #     # the name of the protospacer will be the gene name+ "_" and the CCTop id (i.e. T1...TN)
                        #     output.write(g + "_" + x['id'] + "," + x['seq'] + "," + x['PAM'] +","+x['start']+","+x['end']+","+x['strand']+"\n")
                        # ToDo - check each with bad list, and off target list first...
                        num_accepted = 0
                        for x in sorted_list:
                            if num_accepted < number_wanted and x['score'] > -1000:
                                # now if there were no bad off targets, keep this one
                                num_accepted +=1
                                output.write(g + "_" + x['id'] + "," + x['seq'] + "," + x['PAM'] + "," + x['start'] + "," + x['end'] + "," + x['strand'] + "\n")

    # now write the json file that will be used by the UI to display the results
    # first open the existing json file, and read it into a dict, then add the extra items,
    # change the message and write it back to the file
    # with open(directory + "/" + project_name + ".json") as f:
    #     data = json.load(f)
    #
    # data['no_protospacers']=genes_no_gRNAs
    # data['not_enough_protospacers']=genes_not_enough_gRNAs
    # data['message']="Your design is finished"
    # data['status']="done"
    #
    # # now overwrite the old file with the new json
    # with open(directory + "/" + project_name + ".json", 'w') as json_file:
    #     json.dump(data, json_file)


def main():
    '''
    For each gene in the fasta file - parse the CCTop output, score each gRNA and then prep the output
    make a csv file with the name and sequence of each protospacer/gRNA selected
    The name will be the geneName_TN where TN is the id of the CCTop gRNA in it's output file(genename.xls)
    Will also create a Report file - that just reports exceptions- for example if a gRNA could not be found
    or if not as many as wanted for a particular gene

    ***** if you want to re-filter for faild one - change the fasta for just those genes, and change the project_name
    so that a new report and csv file will be generated without overwriting the last one
     NOTE the fasta just has to be the correct format, and only the sequence names are used, so you can make a dummy

    '''
    if len(sys.argv) < 8:
        print("Usage: python filter_CCTopNanoce.py fasta CCTop_output_directory scaffold_sequence protospacer_end_fused number_wanted project_name inhibit stingency\n note: stringency can be [strict | medium | loose]")
    else:
        fasta = sys.argv[1]
        directory = sys.argv[2]
        scaffold = sys.argv[3]
        protospacer_end = sys.argv[4]
        number_wanted = int(sys.argv[5])
        project_name = sys.argv[6]
        inhibit = sys.argv[7]
        strict = sys.argv[8]
        filter(fasta, directory, scaffold, protospacer_end, number_wanted, project_name, inhibit, strict)
        print("done!")


if __name__ == "__main__":
    main()