__author__ = 'simi'
#!/usr/bin/env python
import sys
import RNA

def evaluate(protospacer, scaffold, end):
    ''' using ViennaRNA, get the folding structure and the delta G and the folding score for the fused sequence of the protospacer and the scaffold.
    If the end is 5, then the scaffold is placed in front of the protosapcer (like for Cpf1), otherwise it is put at the 3 prime end.
    The "folding score" is simply the count of how many unpaired bases are in the protospacer region.'''
    if end == "5":
        full_sequence = scaffold+protospacer
        # get indexes of first and last base of protospacer in full sequence for slicing later
        protospacer_start = len(scaffold)
        protospacer_end = len(full_sequence)
        scaffold_start = 0
        scaffold_end = len(scaffold)
    else:
        full_sequence = protospacer+scaffold
        # get indexes of first and last base of protospacer in full sequence for slicing later
        protospacer_start = 0
        protospacer_end = len(protospacer)
        scaffold_start = len(protospacer)
        scaffold_end = len(full_sequence)

    # now run RNAFold on the full sequence
    # compute minimum free energy (MFE) and corresponding structure
    (ss, mfe) = RNA.fold(full_sequence)

    # run RNAFold on just the scaffold
    (s_ss, s_mfe) = RNA.fold(scaffold)

    # run RNAFold on just the protospacer
    (p_ss, p_mfe) = RNA.fold(protospacer)

    # to compare the scaffold folding and the scaffold part of full folding
    s_portion_full_ss = ss[scaffold_start:scaffold_end]

    # the ss is the structure in ViennaRNA dot format - in this fomat each base is represented
    # calculate the fold score
    # I simply want to know how many "." there are in the substring representing the protospacer
    # this is a simple count of how many bases are unpaired - or unfolded
    folding_score = ss[protospacer_start:protospacer_end].count(".")
    if (s_ss == s_portion_full_ss):
        print("structure of the scaffold is unchanged")
        folding_score = folding_score * 2
    print("full sequence : "+full_sequence)
    print("full sequence : "+ss)
    print("gRNA alone    : "+protospacer)
    print("gRNA alone    : "+p_ss)
    print("from full seq : "+ss[protospacer_start:protospacer_end])
    print("scaffold alone: "+scaffold)
    print("scaffold alone: "+s_ss)
    print("from full seq : "+s_portion_full_ss)
    print("score: "+str(folding_score))


    # now return everything
    return full_sequence, ss, folding_score, mfe


def main():
    '''
    Evaluate the RNA folding of the scaffold and the protospacer. This will return the (1) full sequence processed
    (with the protospacer and scaffold supplied fused to the end indicated ( 5 for the 5 prime end, all others will be fused to the 3 prime end),
    (2) the structure string, (3) the folding score (which is simply how many un paired bases are in the protospacer segment of the full sequene,
    and (4) the delta G - free energy of folding

    '''
    if len(sys.argv) < 4:
        print("Usage: python eval_folding.py protospacer_sequence scafold_sequence protospacer_end_fused \n "
              "where the last parameter is ether 5 or 3 to indicate where the scaffold is fused to the protospacer at either the 5' or 3' end")
    else:
        protospacer = sys.argv[1]
        scaffold = sys.argv[2]
        end = sys.argv[3]
        full_seq,structure,folding_score,delta_g, = evaluate(protospacer, scaffold, end)
        print("processed full seq: "+full_seq+", structure: "+structure+",folding score: "+folding_score+" delta G: "+str(delta_g))


if __name__ == "__main__":
    main()