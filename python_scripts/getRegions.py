import sys
from Bio import SeqIO

# This script was made for the nano project (eukaryote- with scaffolds and gff annotation)
# It will return a fasta file with the regions specified for each gene in the defs_file, on the proper scaffold, start-end


def run(newname, defs_file, genome_fa):
    # first parse the def_file into a list of dicts
    genes = []
    first = True
    with open(defs_file) as glist:
        for line in glist:
            if first:
                #skip the header line
                first = False
            else:
                # split line
                parts = line.split(",")
                genes.append({"id": parts[0].strip(), "name":parts[1].strip(), "scaffold":parts[2].strip(), "start":parts[3].strip(), "end":parts[4].strip()})

    # parse genome fasta sequences using biopython
    sequences = list(SeqIO.parse(genome_fa, "fasta"))
    # parse into a dict where the key is the scaffold, and the value is the sequence
    genome = {}
    for s in sequences:
        # get sequence object
        seq = s.seq
        s_size = len(seq)
        # get scaffold name
        scaffold = s.id
        genome[scaffold] = str(seq)
    print("******** making fasta *******")
    # now open fasta with new name for writing
    with open(newname, "w+") as f:
        for info in genes:
            print(info)
            # each info is dict with id, name, scaffold, start and end
            start = int(info['start'])
            end = int(info['end'])

            # get the scaffold sequence
            scaffold_sequence = genome[info['scaffold']]
            # get the sequence substring
            seq = scaffold_sequence[start:end]

            # print the name in the file: number|name same as in the CDS fasta file
            f.write(">" + info['id'] + "_" + info['name'] + "_" + info['scaffold'] + "\n")
            f.write(seq + "\n")



def main():
    '''
    Creates input files for ccTop
    this script is specifically for Yando's project
    input is:
        newname - is the base name for the output file it will be called newname.fa
        def_file - is a csv file with these columns:
        id, name, scaffold, start, end
    :return:
    '''
    if len(sys.argv) < 4:
        print(
            "Usage: python getRegions.py newname def_file genome_fa \n Note: the def_file needs to be a csv with these columns(same order): id, name, scaffold, start, end ")
    else:
        newname = sys.argv[1]
        def_file = sys.argv[2]
        genome_fa = sys.argv[3]


        run(newname, def_file, genome_fa)
        print("done!")


if __name__ == "__main__":
    main()