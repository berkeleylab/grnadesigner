import sqlite3
import json


def get_sqlite_db(db_file):
    ''' returns db connection object'''
    # get sqlite connection and cursor
    conn = sqlite3.connect(db_file)
    cur = conn.cursor()
    return cur


def get_strand(gene, sqldb):
    '''return the strand and chomosome for the gene'''
    # first get the strand and chromosome that the gene is on
    print("getting strand for gene: "+gene )
    sqldb.execute("select strand from features where id=:gene", {"gene": gene})
    row = sqldb.fetchone()
    # TODO put in try/except in case there are errors in the gene names...though these should be vetted by the ui first
    if row[0]:
        strand = row[0]
    else:
        strand= "+"
        print("ERROR: no strand!")
    return strand


def get_all_genes(cur):
    names = []
    # TODO: check if genes can either be "genes" or "CDS"? may need to check featuretypes first?
    cur.execute("SELECT distinct(id) from features where featuretype = 'gene'")
    rows = cur.fetchall()
    for r in rows:
        names.append(r[0])
    return names


def process_genes(new_file, db_file, genes):
    ''' for gene features,
    new_file is the path/name for the json file generated
    db_file is the sqlite database file
    genes is the list of gene names, if it is an empty list, will get all loci
    '''
    # get db cursor
    cur = get_sqlite_db(db_file)
     # check if genes is empty
    if len(genes) < 1:
        # get list of all genes
        genes = get_all_genes(cur)
    # for each gene - create the dict
    gene_info = {}
    for g in genes:
        gene_info[g] = get_strand(g, cur)

    # write to file
    with open(new_file, 'w') as json_file:
        json.dump(gene_info, json_file)


def process_custom(new_file, custom_features_file):
    ''' this creates a json file with a dict
    where the feature name (first column) is the key, and the strand (column 5) '''
    locus_info = {}
    with open(custom_features_file, "r") as features:
        for line in features:
            print("looking at line: " + line)
            # todo skip lines starting with "#" those will be headers
            if not line.startswith("#"):
                # TODO decide what the line will be: name, sequence, PAM, start, end, strand, chromosome
                # TODO above is what we have now plus the chromosome name at the end we can change the parsing later
                # instead would like: name, chromosome, start, end, strand, sequence, pam
                # then we can ignore the last 2 columns so any file with just the first 5 columns would work
                parts = line.split(",")
                # check if there are enough parts
                if len(parts) >= 5:
                    name = parts[0]
                    strand = parts[4]
                    locus_info[name]=strand
    # write to file
    with open(new_file, 'w') as json_file:
        json.dump(locus_info, json_file)

