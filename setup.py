from setuptools import setup

setup(
    name='gRNADesigner',
    packages=['gRNADesigner'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)