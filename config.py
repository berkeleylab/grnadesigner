# configuration paths and variables
# most of these will change for each installation

# New - eventually remove above settings
BASE_URL="localhost:5000"
EMAIL_SENDER = "noreply@example.com"
ADMIN_EMAIL = "admin@example.com"

SECRET_KEY='ChangeMeIAmNoGood'
MAIL_SERVER="localhost"
MAIL_PORT="25"
MAIL_USE_SSL=False

# Database
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Database info used in the wdl
DB_NAME="grna"
DB_HOST="localhost"
DB_USER="grnasuer"
DB_PASS="******"

# the URI is in the form: <driver>://<user>:<password>@<host>/<database>
#SQLALCHEMY_DATABASE_URI = 'postgresql://username:password@localhost/dbname'
SQLALCHEMY_DATABASE_URI = 'postgresql://'+DB_USER+':'+DB_PASS+'@'+DB_HOST+'/'+DB_NAME

# Paths for the wdl input and options json files
# where the uploads file is on the system
DATA_DIRECTORY="/Users/simi/Projects/gRNA/grna/uploads"
# full path where genomes are stored
GENOME_DIRECTORY="/Users/simi/Projects/gRNA/grna/genomes"
# full path where public designs are stored
PUBLIC_DIRECTORY="/Users/simi/Projects/gRNA/grna/public"
# path to bowtie executables
BOWTIE_PATH="/global/dna/projectdirs/RD/synbio/gRNADesigner/bowtie/bowtie-1.2.2-linux-x86_64"
# Path for python scripts - on cori
SCRIPT_PATH="/opt/www/synbio/gRNADesigner/grna-dev.jgi.doe.gov/grnadesigner/python_scripts"
#"/global/dna/projectdirs/RD/synbio/gRNADesigner/repo/python_scripts"
# Path for CCTop software
CCTOP_PATH="/global/dna/projectdirs/RD/synbio/gRNADesigner/CCTop/juanlmateo-cctop_standalone-95ea199ba2b6"

# Cromwell
# Path/executable for cromwell jar
CROMWELL="/opt/www/synbio/gRNADesigner/grna-dev.jgi.doe.gov/grnadesigner/tools/cromwell-48.jar"
