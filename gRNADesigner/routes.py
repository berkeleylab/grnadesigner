from gRNADesigner import app, mail
import re
import shutil
import python_scripts.process_gb_for_CCTop as CCTop
import python_scripts.save_genome as genome
import python_scripts.extract_gene_seq_regions as extract_gene_seq_regions
import python_scripts.create_locus_info_json_file as create_locus_json
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

from flask import request, render_template, send_file, jsonify, abort, session, redirect, url_for, flash
import flask
### on local got error:ImportError: cannot import name 'secure_filename' from 'werkzeug'
#from werkzeug import secure_filename
# the fix is https://stackoverflow.com/questions/65853520/cannot-import-name-secure-filename-from-werkzeug
from werkzeug.utils import secure_filename
from werkzeug.security import generate_password_hash, check_password_hash

import python_scripts.auth.JWTHandler as jwt
import python_scripts.auth.SSOAdapter as sso

from flask_login import LoginManager, login_user, logout_user, login_required, current_user
from flask_mail import Message

import os
from datetime import datetime, timedelta
import uuid as uuid_creator
from string import Template
import json
import subprocess
from .models import db, App, User, Login, Job, Genome, Design
import config


##
## HELPER FUNCTIONS
##
def get_username():
    """
    get username from flask-login current_user
    """
    username = current_user.username
    return username

def get_email():
    """
    get email from flask-login current_user
    """
    email = current_user.email
    return email



def get_app():
    """
    get the id of the gRNADesigner app from the synbio_apps database
    :return: int app_id
    """
    app = App.query.filter_by(app_name='gRNADesigner').first()
    return app


def get_user(name):
    """
    get user id from synbio apps database
    using their caliban/user name
    if the user is not in the db, add it, and get the id

    :return: user
    """
    # try getting the users id
    user = User.query.filter_by(username=name).first()
    if user is None:
        # add to the database
        user = User(name, True)
        db.session.add(user)
        db.session.commit()
        user_id = user.user_id
    return user


def get_genome(uuid):
    genome = Genome.query.filter_by(genome_uuid=uuid).first()
    return genome


def insert_job(user_id, app_id, job_type, job_name, uuid, status, start_time):
    """
    insert job into the database
    :param user_id:
    :param app_id
    :param job_type:
    :param job_name:
    :param uuid:
    :param status:
    :param start_time:
    :return:
    """
    job = Job(user_id, app_id, job_type, job_name, uuid, status, start_time)
    db.session.add(job)
    db.session.commit()


def update_job(uuid, status, end_time):
    job = Job.query.filter_by(job_uuid=uuid).first()
    job.job_status = status
    job.job_end_date = end_time
    db.session.add(job)
    db.session.commit()


def update_genome(uuid, status):
    genome = Genome.query.filter_by(genome_uuid=uuid).first()
    genome.job_status = status
    db.session.add(genome)
    db.session.commit()


def monthdelta(date, delta):
    m, y = (date.month+delta) % 12, date.year + ((date.month)+delta-1) // 12
    if not m: m = 12
    d = min(date.day, [31,
        29 if y%4==0 and not y%400==0 else 28,31,30,31,30,31,31,30,31,30,31][m-1])
    return date.replace(day=d,month=m, year=y)


def insert_genome(user, genome_uuid, genome_userdefined_id, annotation_file_type, annotation_file_name, status, saved_date, genome_type):
    genome = Genome(user, genome_uuid, genome_userdefined_id, annotation_file_type, annotation_file_name, status, saved_date, genome_type)
    db.session.add(genome)
    db.session.commit()

def check_password_strength(password):
    ''' Check to make sure the user's selected password is strong enough
    It should be at least 8 charater's long,
    include one upper and one lower case letter,
    at least 1 number and not contain white space'''
    # check the length
    flag = 0
    while True:
        if (len(password) <= 8):
            flag = -1
            break
        elif not re.search("[a-z]", password):
            flag = -1
            break
        elif not re.search("[A-Z]", password):
            flag = -1
            break
        elif not re.search("[0-9]", password):
            flag = -1
            break
        elif re.search("\s", password):
            flag = -1
            break
        else:
            flag = 0
            print("Valid Password")
            break
    return flag


def validate_email(email):
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    if (re.fullmatch(regex, email)):
        return True
    else:
        return False
##
## Routes
##

@app.route('/<page>')
@login_required
def show_page(page):
    print("going to page: "+page)
    # if going to create_new.html - get all saved genomes for the user
    if page == 'create_step1.html':
        print('go to create_step1 form')
        name = get_email()
        user = current_user
        print("user name: " + name)
        # get list of saved genomes for this user, that are complete, and order with most recent first
        genomes = Genome.query.filter(Genome.user_id == user.user_id, Genome.genome_status=="Complete").order_by(Genome.genome_id.desc()).all()
        data = {}
        print("number of genomes: " + str(len(genomes)))
        return render_template("create_step1.html", genomes=genomes, data=data, username=name)
    elif page == 'save_genome.html':
        print('go to save_genome form')
        name = get_username()
        user = current_user
        print("user name: " + name)
        # get list of saved genomes for this user
        #genomes = Genome.query.filter(Genome.user_id == user.user_id).all()
        data = {}
        return render_template("save_genome.html", data=data, username=name)
    else:
        return render_template(page)


@app.route('/', methods = ['GET', 'POST'])
def index_page():
    apps = App.query.all()
    # check if logged in, - else forward to grna.html
    try:
        if current_user.is_authenticated():
            return render_template("grna.html", apps=apps)
        else:
            return render_template("index.html", apps=apps)
    except:
        return render_template("index.html", apps=apps)


@app.route('/tutorial', methods = ['GET', 'POST'])
@login_required
def tutorial():
    apps = App.query.all()
    return render_template("tutorial.html", admin_email=app.config['ADMIN_EMAIL'], apps=apps)


@app.route('/uploader', methods = ['GET', 'POST'])
@login_required
def upload_file():

    print('[upload_file]')

    if request.method == 'POST':

        uploaded_file = request.files['genome']
        uploaded_filename = secure_filename(uploaded_file.filename)

        uploaded_file.save(os.path.join(app.config['UPLOAD_FOLDER'], uploaded_filename))

    return 'file uploaded successfully'


@app.route('/save_genome', methods = ['GET', 'POST'])
@login_required
def save_genome():
    ''' This is used when saving a new genome '''
    print('select target genome')
    username = get_username()
    user = current_user
    print("user name: " + username)
    email = get_email().replace("@", "__")


    if request.method == 'POST':
        # get user defined name/id
        genome_name = request.form['genome_name']
        #get type
        genome_type = request.form['genome_type']

        # save newly uploaded genome and process files
        # make a uuid for this genome- this will be the name of the directory
        uuid = str(uuid_creator.uuid4())
        # create directory named with UUID under username : ./genomes/<email>/<uuid>
        # in the "genomes" directory for the new design, and put the gb file there
        # check if user has directory in genomes yet
        user_dir = os.path.join(app.config['GENOME_FOLDER'], email)
        if not os.path.isdir(user_dir):
            os.mkdir(user_dir)
        new_dir = os.path.join(app.config['GENOME_FOLDER'], email, uuid)
        os.mkdir(new_dir)

        uploaded_seq_file = request.files['genome_sequence']
        uploaded_seq_filename = secure_filename(uploaded_seq_file.filename)
        print("uploaded seq filename: " + uploaded_seq_filename)

        # put genome seq file in new directory
        seq_file = os.path.join(app.config['GENOME_FOLDER'], email, uuid, uploaded_seq_filename)
        data_path = os.path.join(app.config['GENOME_FOLDER'], email, uuid)
        print("saving seq file...")
        uploaded_seq_file.save(seq_file)

        # need orig filename for the database
        orig_ann_file_name = uploaded_seq_filename
        if request.files['genome_annotation']:
            # also uploaded the annotation file
            uploaded_ann_file = request.files['genome_annotation']
            uploaded_ann_filename = secure_filename(uploaded_ann_file.filename)
            print("uploaded ann filename: " + uploaded_ann_filename)

            # put genome annotation file in new directory
            ann_file = os.path.join(app.config['GENOME_FOLDER'], email, uuid, uploaded_ann_filename)
            print("saving ann file...")
            uploaded_ann_file.save(ann_file)
            orig_ann_file_name = uploaded_ann_filename
        else:
            # set ann_file to "NA" for not applicable
            uploaded_ann_filename = "NA"

        # now that we have the genome_name, and 1-2 genome files, we  must process them
        # todo - if there is a problem - return error message
        d = {}
        try:
            dbname = app.config["DB_NAME"]
            dbhost = app.config["DB_HOST"]
            dbuser = app.config["DB_USER"]
            dbpass = app.config["DB_PASS"]
            print("Upload OK, processing genome")
            ok, message, orig_file_format, = genome.process(data_path, uuid, uploaded_seq_filename, uploaded_ann_filename, dbname, dbhost, dbuser, dbpass, config.SCRIPT_PATH, config.BOWTIE_PATH)
            # TODO - now put the genome in the database if ok
            if ok:
                print("processing OK, putting in database")
                # put in database
                status = "Processing"
                saved_date = datetime.now()
                insert_genome(user, uuid, genome_name, orig_file_format, orig_ann_file_name, status, saved_date, genome_type)
                d['genome_name']=genome_name
                d['genome_uuid']=uuid
                d['status'] = status
                d['saved_date'] = saved_date
                d['genome_type'] = genome_type

                return render_template("genome.html", data=d)

            else:
                #TODO - if not ok, delete files and tell the user...
                print("saving genome failed for user: "+username+", uuid: "+uuid)
                d['error'] = "Your genome could not be saved: "+message
                return render_template("save_genome.html", data=d)
        except Exception as e:
            # TODO what if there is an error here?
            print('There was a problem with the genome file...'+str(e))
            d['error'] = "Your genome could not be saved. Error: "+str(e)
            return render_template("save_genome.html", data=d, username=username)


@app.route('/seq_ret', methods = ['GET', 'POST'])
@login_required
def seq_ret():
    # This is for version 2
    print('Extract sequences... ')
    message = ""
    name = get_email().replace("@", "__")
    user = current_user
    print("user name: "+name)
    app_entry = get_app()
    print("app_id: "+str(app_entry))

    if request.method == 'POST':
        ok = True
        # need uuid for the design
        uuid = str(uuid_creator.uuid4())
        # create directory named with UUID
        # in the "upload directory for the new design, and put the gb file there
        os.mkdir(app.config['UPLOAD_FOLDER'] + uuid)
        data_path = os.path.join(app.config['UPLOAD_FOLDER'], uuid)

        # need uuid of selected genome:
        genome_uuid = request.form['select_genome']

        # need target region parameters:
        feature_type = request.form['feature_type']

        # check if doing grna design
        want_grna = False
        if request.form['design_grna'] == "yes":
            print("want gRNA designed")
            want_grna = True
            job_type = "gRNA"
        else:
            job_type = "Seq_RE"
        # build job type description
        # may want to see the frequency which people use loci features or custom ones, and some genes or all
        if feature_type == 'gene_features':
            if request.form['target_genes']:
                job_type = job_type + "-loci-some"
            else:
                job_type = job_type + "-loci-all"
        else:
            job_type = job_type + "-custom"

        # add to database
        try:
            status = "Running"
            insert_job(user, app_entry, job_type, request.form['design_name'], uuid, status, datetime.now())
        except Exception as e:
            print("DB exception: "+str(e))

        region_fasta = os.path.join(app.config['UPLOAD_FOLDER'], uuid, uuid + "_regions.fa")
        genome_fasta = os.path.join(app.config['GENOME_DIRECTORY'], name, genome_uuid, genome_uuid + ".fa")
        gffdb_file = os.path.join(app.config['GENOME_DIRECTORY'], name, genome_uuid, genome_uuid + ".db")
        locus_json_file = os.path.join(app.config['UPLOAD_FOLDER'], uuid, "locus_info.json")
        extraction_log_file =os.path.join(app.config['UPLOAD_FOLDER'], uuid, "extraction_log.txt")

        if feature_type == 'gene_features':
            # get list of genes in genome from json
            genome_genes_json = os.path.join(app.config['GENOME_DIRECTORY'], name, genome_uuid, "genes.json")
            print("getting genes.json: "+genome_genes_json)
            with open(genome_genes_json) as f:
                g_dict = json.load(f)
            g_genes = g_dict["genes"]
            print("number of genes in genome: "+str(len(g_genes)))
            # get gene list
            genes = []
            if request.form['target_genes']:
                gene_list= request.form['target_genes'].split()
                # make new list to remove any duplicates
                # and make sure that all are in the genome
                genes =[]
                print("checking for duplicates and validating genes")
                for g in gene_list:
                    if(g in g_genes) and (g not in genes):
                        genes.append(g)

            print("There were "+str(len(genes))+" specified as targets")

            # if doing gRNA will need locus info json created
            if want_grna:
                create_locus_json.process_genes(locus_json_file, gffdb_file, genes)

            # TODO - will run here, for testing- for all genes, may need to
            #  run asynchronously
            print("genes: "+str(genes))
            no_seq = []
            # if there are zero genes selected, replace the genes list with the g_genes list of all in the genome
            if len(genes) == 0:
                genes = g_genes
            try:
                # this returns a list of gene names that had no sequences for feedback
                no_seq = extract_gene_seq_regions.process_genes(extraction_log_file, region_fasta, genome_fasta, gffdb_file, request.form['start_origin'], int(request.form['start_padding']),
                                         request.form['end_origin'], int(request.form['end_padding']), request.form['limit_regions'], genes)
            except:
                print("Error extracting sequence regions for "+uuid)
                message = "There was an error extracting target sequence regions from loci features"
                status = "Failed"
                ok = False
        else:
            # will use custom features
            # get uploaded file
            uploaded_file = request.files['custom_features']
            uploaded_filename = secure_filename(uploaded_file.filename)
            print("file_name: " + uploaded_filename)

            # put design directory
            # TODO - may want to rename the file to something generic?
            features_file = os.path.join(app.config['UPLOAD_FOLDER'], uuid + "/", uploaded_filename)

            # if doing gRNA will need locus info json created
            if want_grna:
                create_locus_json.process_custom(locus_json_file, features_file)

            # uploaded_file.save(os.path.join(app.config['UPLOAD_FOLDER'],uuid+"/", uploaded_filename))
            print("saving custom features file...")
            uploaded_file.save(features_file)
            # TODO - run the extraction script - not tested yet!
            # TODO - will run here, for testing- for all genes, may need to
            # run asynchronously?
            print("custom file: " + uploaded_filename)
            try:
                no_seq = extract_gene_seq_regions.process_custom(extraction_log_file, region_fasta, genome_fasta, gffdb_file, request.form['start_origin'],
                                                   int(request.form['start_padding']),
                                                   request.form['end_origin'], int(request.form['end_padding']),
                                                   request.form['limit_regions'], uploaded_filename)
            except:
                print("Error extracting sequence regions for "+uuid)
                message = "There was an error extracting target sequence regions from custom features"
                status = "Failed"
                ok = False
                # in this case the job has failed so set the job failed in db
                update_job(uuid, status, datetime.now())

        # the request.form is an ImmutableMultiDict
        # create a mutable dict so can add values for further processing
        form_data = request.form.to_dict()
        form_data["user_name"]=name
        form_data["uuid"] = uuid

        if ok:
            message = "Target sequence regions successfully extracted."
            # check if want to run CCTop to create gRNA library
            if want_grna:
                message = message+" Generating gRNA Library."
                status = "Running"
                # run CCTop asynchronously
                print("will run CCTop")
                # need a locus_info_json created above file that is a dict with the key = the locus name, and value = the strand for that feature
                pam = request.form['PAM']
                # TODO  validate that the scaffold sequence is a valid sequence
                # scaffold_seq = request.form['scaffold_seq']

                # all scaffolds will be fused to the 3' end of the protospacer, except when using Cpf1
                protospacer_end_fused = '3'
                if pam == "TTTN":
                    protospacer_end_fused = '5'

                form_data['fused_end']=protospacer_end_fused
                form_data['results_path']=data_path
                form_data["no_protospacers"]=[]
                form_data["not_enough_protospacers"]=[]
                form_data["status"]= "Running"
                form_data["message"]=""
                # add no_seq for feedback when sequence regions were not found
                form_data["no_seq"] = no_seq

                # create the wdl input
                # for this we need to set a couple other values in the dict d from the config.py
                form_data["GENOME_DIRECTORY"] = app.config["GENOME_DIRECTORY"]
                form_data["DATA_DIRECTORY"] = app.config["DATA_DIRECTORY"]
                form_data["BOWTIE_PATH"] = app.config["BOWTIE_PATH"]
                form_data["SCRIPT_PATH"] = app.config["SCRIPT_PATH"]
                form_data["CCTOP_PATH"] = app.config["CCTOP_PATH"]
                form_data["DB_NAME"] = app.config["DB_NAME"]
                form_data["DB_HOST"] = app.config["DB_HOST"]
                form_data["DB_USER"] = app.config["DB_USER"]
                form_data["DB_PASS"] = app.config["DB_PASS"]

                # use input_json_v2_template.txt as template
                # open the file
                filein = open("./python_scripts/input_json_v2_template.txt")
                # read it
                src = Template(filein.read())
                # do the substitution
                new_contents = src.substitute(form_data)

                # create new input.json with proper parameters
                input_json_file = open(data_path + "/input.json", "w")
                input_json_file.write(new_contents)
                input_json_file.close()

                # also need to create options.json form the template
                filein = open("./python_scripts/options_json_template.txt")
                # read it
                src = Template(filein.read())
                # do the substitution
                new_contents = src.substitute(form_data)

                # create new input.json with proper parameters
                options_json_file = open(data_path + "/options.json", "w")
                options_json_file.write(new_contents)
                options_json_file.close()

                # create json place holder for results
                with open(data_path + "/" + uuid + ".json", 'w') as json_file:
                    json.dump(form_data, json_file)

                # make system call to cromwell to run the wdl
                # this should not wait for it to return anything
                # java -jar /opt/www/synbio/gRNADesigner/grna-dev.jgi.doe.gov/grnadesigner/tools/cromwell-48.jar run /opt/www/synbio/gRNADesigner/grna-dev.jgi.doe.gov/grnadesigner/python_scripts/basic_grna.wdl  -i input.json -o options.json
                #java -jar /opt/synbio/dependancies/cromwell/cromwell-48.jar run /opt/synbio/repo/grnadesigner/python_scripts/grna_version2.wdl -i ./input.json -o ./options.json
                process = subprocess.Popen(['java',
                                            '-jar',
                                            app.config["CROMWELL"],
                                            'run', app.config["SCRIPT_PATH"] + '/grna_version2.wdl',
                                            '-i', app.config["DATA_DIRECTORY"] + '/' + uuid + '/input.json',
                                            '-o', app.config["DATA_DIRECTORY"] + '/' + uuid + '/options.json'],
                                           cwd=app.config["DATA_DIRECTORY"] + '/' + uuid)
            else:
                # not running gRNA design tool
                status = "Complete"
                form_data["status"] = status
                form_data["message"] = message
                form_data["no_seq"] = no_seq

                # create the uuid json from the form data
                with open(data_path + "/" + uuid + ".json", 'w') as json_file:
                    json.dump(form_data, json_file)

                # in this case the job is complete so set the job complete in db
                update_job(uuid, status, datetime.now())
        else:
            # this is if the extraction failed and ok == False
            # need to finish up the form_data dict and json to be displayed in results
            status = "Failed"
            form_data["status"] = status
            form_data["message"] = message
            form_data["no_seq"] = no_seq

            # create the uuid json from the form data
            with open(data_path + "/" + uuid + ".json", 'w') as json_file:
                json.dump(form_data, json_file)

            # in this case the job is complete so set the job complete in db
            update_job(uuid, status, datetime.now())

        # return user to results page - redirect instead of rendering should fix the resubmit if the user reloads the results page
        # return render_template("extract_result.html", data=form_data)
        return redirect(url_for('show_extraction_results', uuid=uuid))


@app.route('/results/<page>', methods = ['GET', 'POST'])
@login_required
def show_result(page):
    # get json for project
    # get job object from database - page is the uuid
    job = Job.query.filter(Job.job_uuid == page).first()
    try:
        # first try loading the uuid_final.json - this is created when the cromwell wdl is done
        data_path = os.path.join(app.config['UPLOAD_FOLDER'], page)
        with open(data_path+"/"+page+"_final.json") as f:
            data = json.load(f)

    except:
        # if that is not found then open uuid.json  - this means that cromwell hasn't finished
        # or CCTop was not run
        try:
            # first try loading the uuid_final.json
            data_path = os.path.join(app.config['UPLOAD_FOLDER'], page)
            with open(data_path+"/"+page+".json") as f:
                data = json.load(f)

        except:
            return "there was an error retrieving "+page

    return render_template("new_design_result.html", data=data, job=job)


@app.route('/genome/<uuid>', methods = ['GET', 'POST'])
@login_required
def show_genome(uuid):
    name = get_username()
    email= get_email().replace('@', '__')
    # get genome object from uuid
    genome = get_genome(uuid)
    d={"genome_uuid":uuid, "status":genome.genome_status, "genome_name":genome.genome_userdefined_id, "saved_date": genome.saved_date, "annotation_file_name": genome.annotation_file_name, "genome_type":genome.genome_type }
    return render_template("genome.html", data=d,username=name, email=email)


@app.route("/<task_id>/<file_name>", methods=['GET', 'POST'])
@login_required
def getFile(task_id, file_name):
    path = app.root_path.rsplit("/",1)[0]
    print("path: "+ path)
    return send_file(os.path.join(path,"uploads",str(task_id),file_name), as_attachment=True)

@app.route("/extract_result/<uuid>", methods=['GET', 'POST'])
@login_required
def show_extraction_results(uuid):
    # get json file
    data_path = os.path.join(app.config['UPLOAD_FOLDER'], uuid)
    with open(data_path + "/" + uuid + ".json") as f:
        data = json.load(f)
    return render_template("extract_result.html", data=data)

@app.route("/genome_download/<username>/<uuid>/<file_name>", methods=['GET', 'POST'])
@login_required
def getGenomeFile(username, uuid, file_name):
    path = app.root_path.rsplit("/",1)[0]
    print("path: "+ path)
    #path_to_file = os.path.join(path,"genomes", username.replace('@', '__'), str(uuid),file_name )
    path_to_file = os.path.join(app.config["GEMOME_DIRECTORY"], username.replace('@', '__'), str(uuid), file_name)
    return send_file(path_to_file, as_attachment=True)


## Show design list for the user ##
@app.route('/design_list', methods = ['GET'])
@login_required
def show_designs():
    print('show designs for')
    name = get_username()
    user = current_user
    print("user name: "+name)
    app_entry = get_app()

    # only want jobs that were submitted 3 months ago, or earlier
    try:
        current_time = datetime.now()
        three_months = current_time - timedelta(days=90)
        print("3 months: " + str(three_months))

        # get list of jobs for this user, and this app, that were submitted in the last 3 months
        jobs = Job.query.filter(Job.user_id == user.user_id, Job.app_id == app_entry.app_id,
                                Job.job_end_date > three_months).order_by(Job.job_id.desc()).all()
    except Exception as e:
        print("ERROR: " + str(e))

    # get list of jobs for this user, and this app, that were submitted in the last 3 months
    jobs = Job.query.filter(Job.user_id==user.user_id, Job.app_id==app_entry.app_id, Job.job_end_date > three_months).order_by(Job.job_id.desc()).all()
    print("number of jobs: "+str(len(jobs)))
    return render_template("results.html", jobs=jobs, username=name)

## Show genome list for the user ##
@app.route('/your_genomes', methods = ['GET'])
@login_required
def show_genomes():
    print('show genomes for')
    name = get_username()
    user = current_user
    print("user name: "+name)
    app_entry = get_app()

    # get list of genomes for this user - most recently saved first
    genomes = Genome.query.filter(Genome.user_id==user.user_id).order_by(Genome.genome_id.desc()).all()
    print("number of genomes: "+str(len(genomes)))
    return render_template("your_genomes.html", genomes=genomes, username=name)


############################
##### Public Designs #######
############################

## Show public design list ##
@app.route('/public_designs', methods = ['GET'])
# @login_required - don't require a login
def show_public_designs():
    print('show public designs')
    #name = get_username()
    # check if the current user is logged in or not for navigation
    user = current_user
    loggedin = False
    try:
        if user.email:
            loggedin = True
            print("user is logged in")
    except:
        print("user is NOT logged in")

    # get list of public designs
    designs = Design.query.filter(Design.is_public == True).all()
    print("number of public designs: "+str(len(designs)))
    return render_template("public_designs.html", designs=designs, loggedin=loggedin)


# Link to download files from public designs
@app.route("/public_download/<uuid>/<file_name>", methods=['GET', 'POST'])
def getPublicFile(uuid, file_name):
    #path = app.root_path.rsplit("/",1)[0]
    #print("path: "+ path)
    #path_to_file = os.path.join(path,"public", str(uuid), file_name )
    path_to_file = os.path.join(app.config["PUBLIC_DIRECTORY"], str(uuid), file_name)
    return send_file(path_to_file, as_attachment=True)


# show public design page
@app.route('/public_design/<design_id>', methods = ['GET', 'POST'])
def show_public_design(design_id):
    # get the design object from the database
    design = Design.query.filter(Design.design_id==design_id).first()
    user = current_user
    # check if user is logged in
    loggedin = False
    try:
        if user.email:
            loggedin = True
            print("user is logged in")
    except:
        print("user is NOT logged in")
    # get json for project
    try:
        # first try loading the uuid_final.json - this is created when the cromwell wdl is done
        data_path = os.path.join(app.config['PUBLIC_DIRECTORY'], design.job_uuid)
        with open(data_path+"/"+design.job_uuid+"_final.json") as f:
            data = json.load(f)
    except:
        return "there was an error retrieving json for public design "+design_id
    # check if the user is logged in, and if they are the owner - for edit permissions
    owner = False
    if loggedin:
        try:
            if current_user.user_id == design.user.user_id:
                owner = True
        except Exception as e:
            print("could not check ownership")
    return render_template("public_design.html", data=data, design=design, owner=owner, loggedin=loggedin)


# Edit public design - you must be logged in and be the owner of the design
@app.route('/edit_public_design/<design_id>', methods=['GET', 'POST'])
@login_required
def edit_public_design(design_id):
    if request.method=='GET': # if the request is a GET we render the edit form
        # first make sure the user is the owner
        # get design object
        design = Design.query.filter(Design.design_id == design_id).first()
        if current_user.user_id == design.user.user_id:
            return render_template('edit_public_design.html', user=current_user, design=design)
    else:
        print("editing public design for")
        # TODO - process edit form submission
        # TODO - if changing from public to private, just do that and return user to public design list?
        # if the request is POST we process the edit
        # id = request.form.get('design_id')
        # no longer allowing people to revert to private
        privateCheck = False #True if request.form.get('privateCheck') else False
        print("private check: "+str(privateCheck))
        name = request.form.get('name')
        desc = request.form.get('description')
        print("desc: "+desc)
        application = request.form.get('application')
        taxon = request.form.get('taxon')

        # get design
        design = Design.query.filter(Design.design_id == design_id).first()
        # commit changes
        changeJob = None
        if privateCheck:
            # remember = True if request.form.get('remember') else False
            # make it the opposite of current setting
            if design.is_public:
                design.is_public = False
                changeJob = False
            else:
                design.is_public = True
                changeJob = True
        design.design_name = name
        design.description = desc
        design.crispr_application = application
        design.taxon_id = taxon

        db.session.commit()

        # TODO - also change the job is public ....
        if changeJob is not None:
            job = design.job
            job.is_public = changeJob
            db.session.commit()

        # then redirect to public design page
        return redirect(url_for('show_public_designs'))

# Create public design - you must be logged in and be the owner of the job
@app.route('/create_public_design/<job_id>', methods=['GET', 'POST'])
@login_required
def create_public_design(job_id):
    if request.method=='GET': # if the request is a GET we render the edit form
        # first make sure the user is the owner
        # get job object
        job = Job.query.filter(Job.job_id == job_id).first()
        go = False
        if current_user.user_id == job.user.user_id:
            go = True
            # make sure it is not public
            if job.is_public:
                message = "This design is already public"
                go = False
            else:
                # check if design is made already, but disabled
                design = Design.query.filter(Design.job_uuid == job.job_uuid).first()
                if design:
                    # redrect to edit
                    return render_template('edit_public_design.html', user=current_user, design=design)
                elif go:
                    return render_template('create_public_design.html', user=current_user, job=job)
    else:
        # TODO - process form submission
        print("creating new public design")
        # if the request is POST we process the form and create the public design
        name = request.form.get('name')
        desc = request.form.get('description')
        application = request.form.get('application')
        taxon = request.form.get('taxon')

        # need the path to the genome files and the genome uuid
        job = Job.query.filter(Job.job_id == job_id).first()
        # set job is public to true
        job.is_public = True
        db.session.commit()

        user = User.query.filter(User.user_id == job.user_id).first()
        # job_id - got from post
        # user_id
        user_id = job.user_id
        job_uuid = job.job_uuid
        print("got job and user")
        # genome_uuid from <job_uuid>_final.json in job upload file
        data_path = os.path.join(app.config['UPLOAD_FOLDER'], job.job_uuid)
        json_file_path = os.path.join(data_path, job.job_uuid+"_final.json")
        with open(json_file_path) as f:
            data = json.load(f)
        # values for genome path
        genome_uuid = data['select_genome']
        user_name = data['user_name']
        print("opened json file")

        edit_time = datetime.now()
        # get new design
        design = Design(user_id=user_id, job_id=job_id, job_uuid=job_uuid, genome_uuid=genome_uuid, taxon_id=taxon, description=desc, design_name=name, crispr_application=application, edit_time=edit_time)

        # commit changes
        design.is_public = True
        # commit to db
        db.session.add(design)
        db.session.commit()
        print("commited design to database")
        # next create the public design directory and copy files
        # in app.config['PUBLIC_DIRECTORY'] cp the genome directory ... it should be named for the job directory,
        design_dir_path = os.path.join(app.config['PUBLIC_DIRECTORY'], job_uuid)
        os.mkdir(design_dir_path)
        genome_path = os.path.join(app.config['GENOME_DIRECTORY'], user_name, genome_uuid)
        # copy gff3
        shutil.copy(os.path.join(genome_path, design.genome_uuid+".gff3"), design_dir_path)
        # copy fasta
        shutil.copy(os.path.join(genome_path, design.genome_uuid + ".fa"), design_dir_path)

        # next copy the <job_uuid>_final.json into the public directory
        shutil.copy(json_file_path, design_dir_path)
        # copy all other files that can be downloaded from design page - for those not logged in
        # design: job_uuid.csv
        shutil.copy( os.path.join(data_path, job.job_uuid+".csv"), design_dir_path)
        # target regions
        shutil.copy(os.path.join(data_path, job.job_uuid + "_regions.fa"), design_dir_path)
        # report
        shutil.copy(os.path.join(data_path, job.job_uuid + "_report.txt"), design_dir_path)
        # tar.gz
        shutil.copy(os.path.join(data_path, job.job_uuid + ".tar.gz"), design_dir_path)

        print("copied files")
        # then redirect to public design page
        return redirect(url_for('show_public_designs'))


#####  LOCAL LOGIN ####

######### Profile #################################################
@app.route('/profile') # profile page that return 'profile'
@login_required
def profile():
    return render_template('profile.html', user=current_user)


@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    if request.method=='GET': # if the request is a GET we return the login page
        return render_template('edit_profile.html', user=current_user)
    else:
        # if the request is POST the we check if the email already exists
        # must be either the same as current user or unique
        # and with te right password
        email = request.form.get('email').lower()
        name = request.form.get('name')
        # check email if changed
        if email != current_user.email:
            if not validate_email(email):  # make sure email is in the correct form
                flash('Please enter a valid email', 'danger')
                return render_template('register.html')
            else:
                # then check if it belongs to another user
                user = User.query.filter_by(email=email).first()
                if user:
                    # return with error that that email is used by another user
                    flash('Email address already used by another user', 'danger')
                    return render_template('edit_profile.html', user=current_user)
                else:
                    # set as new email
                    current_user.email = email
        # commit changes
        current_user.username = name
        db.session.commit()
        # then redirect to profile page
        return render_template('profile.html', user=current_user)


@app.route('/change_pass', methods=['GET', 'POST'])
@login_required
def change_pass():
    if request.method=='GET': # if the request is a GET we return the change password page
        return render_template('change_pass.html')
    else:
        # if the request is POST check passwords
        password0 = request.form.get('password')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')
        # first check current password - same as current logged in user?
        current_pass_hash = current_user.password
        if check_password_hash(current_pass_hash, password0):
            if check_password_strength(password1) == -1:
                # the password is not strong enough
                flash('Your password mys be at least 8 characters long and contain at least one upper and lower case letter, a number and no white space', 'danger')
            # check if new passwords match
            if password1 == password2:
                # this is ok
                # save new profile info
                current_user.password = generate_password_hash(password1, method='sha256')
                db.session.commit()
                # then redirect to profile page
                flash('Your password was changed', 'success')
                return render_template('profile.html', user=current_user)
            else:
                # return with error that that email is used by another user
                flash("Passwords don't match", "danger")
                return render_template('change_pass.html')
        else:
            flash('Your current password does not match the one entered', 'danger')
            return render_template('change_pass.html')


####################################################################
@app.route('/login', methods=['GET', 'POST']) # define login page path
def login(): # define login page function
    app.logger.info('inside login')
    if request.method=='GET': # if the request is a GET we return the login page
        return render_template('login.html')
    else:
        # if the request is POST the we check if the user exist
        # and is using the right password
        email = request.form.get('email').lower()
        password = request.form.get('password')
        remember = True if request.form.get('remember') else False
        user = User.query.filter_by(email=email).first()
        # check if the user actually exists
        # take the user-supplied password, hash it, and compare it
        # to the hashed password in the database
        if not user:
            flash('Please sign up!', category='danger')
            return render_template('register.html')
        elif not check_password_hash(user.password, password):
            flash('Please check your login details and try again.', 'danger')
            return render_template('login.html') # if the user
               #doesn't exist or password is wrong, reload the page
        # if the above check passes, then we know the user has the
        # right credentials
        login_user(user, remember=remember)
        # log login in db
        current_date = datetime.now()
        new_login = Login(user_id=user.user_id, login_time=current_date)
        db.session.add(new_login)
        db.session.commit()
        return render_template('grna.html')


####################################################################
@app.route('/register', methods=['GET', 'POST'])# we define the sign                                               # up path
def register(): # define the sign up function
    if request.method=='GET': # If the request is GET we return the
                              # sign up page and forms
        return render_template('register.html')
    else: # if the request is POST, then we check if the email
          # doesn't already exist and then we save data
        email = request.form.get('email').lower()
        name = request.form.get('name')
        password1 = request.form.get('password1')
        password2 = request.form.get('password2')
        user = User.query.filter_by(email=email).first() # if this
                              # returns a user, then the email
                              # already exists in database
        if user: # if a user is found, we want to redirect back to
                 # signup page so user can try again
            flash('Email address already exists', 'danger')
            return render_template('register.html')
        elif not validate_email(email): # make sure email is in the correct form
            flash('Please enter a valid email', 'danger')
            return render_template('register.html')
        elif check_password_strength(password1) == -1:
            # the password is not strong enough
            flash('Your password must be at least 8 characters long and contain at least one upper and lower case letter, a number and no white space', 'danger')
            return render_template('register.html')
        elif password1 == password2:
            app.logger.info('will signup...')
            # create a new user with the form data. Hash the password so
            # the plaintext version isn't saved.
            new_user = User(email=email, username=name, password=generate_password_hash(password1, method='sha256'))#add the new user to the db
            db.session.add(new_user)
            db.session.commit()
            return render_template('login.html')
        else:
            flash('Passwords do not match', 'danger')
            return render_template('register.html')


##################################### logout path
@app.route('/logout')
@login_required
def logout():
    app.logger.info("will logout user")
    logout_user()
    return render_template('index.html')



################ Reset Password if forgot ##############
def send_reset_email(user):
    print("will send password reset email")
    app.logger.info("sending password reset email")

    token = user.get_token()
    # url_for() doesn't work on VMs with proxy through gunicorn
    # reset_url = url_for('reset_password', token=token, _external=True)
    reset_url = app.config['BASE_URL']+"/reset_password/"+token
    app.logger.info(reset_url)
    print("rest url: " + reset_url)

    msg = Message('gRNA-SeqRET Password Reset Request', recipients=[user.email], sender= app.config['EMAIL_SENDER'])
    msg.body = f'''To reset your SynBioTools password, please follow the link below. 

    {reset_url}

    This link will only be valid for 1 hour. 
    If you did not request to change your password, please ignore this email.

    '''
    # send the email...
    mail.send(msg)


@app.route('/reset_request', methods=['GET', 'POST'])
def reset_request(): # define login page function
    app.logger.info('inside rest pass')
    if request.method=='GET': # if the request is a GET we return the login page
        return render_template('reset_request.html')
    else:
        email = request.form.get('email').lower()
        user = User.query.filter_by(email=email).first()
        if user:
            send_reset_email(user)
            flash('Check your email to reset your password. Token will be valid for 1 hour.','success')
            return render_template('login.html')
        else:
            flash("The email entered was not valid", 'danger')
            return render_template('reset_request.html')


@app.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    user = User.verify_token(token)
    if user == None:
        flash('The password reset token has expired', "danger")
        return redirect('reset_request')
    else:
        print("token verified")
        if request.method == 'POST':
            # see video @46 minutes
            password1 = request.form.get('password1')
            password2 = request.form.get('password2')
            if check_password_strength(password1) == -1:
                # the password is not strong enough
                flash('Your password mys be at least 8 characters long and contain at least one upper and lower case letter, a number and no white space',
                    'danger')
            elif password1 == password2:
                hashed_pass = generate_password_hash(password1, method='sha256')
                user.password = hashed_pass
                db.session.commit()
                flash("Password Changed! Please login","success" )
                return render_template('index.html')
            else:
                flash('Passwords do not match', "danger")
    return render_template('reset_password.html', token=token)


