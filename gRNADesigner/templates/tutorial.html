{% extends 'base.html' %}

{% block content %}

		<div class="container">
			<h2>Tutorial</h2>
            <p>
			The JGI's guide RNA design and Sequence Extraction Tool (gRNA-SeqRET) can be used to design
                guide RNA libraries on a genome scale or targeting specific loci. Additionally,
                the tool can be used to batch extract specific  sequences  from a given genome,
                for example for automated design of homology-based repair templates.
            </p>
            <p>
            There are three parts to the application:
            <ol>
                <li>Saving and pre-processing the genome of interest</li>
                <li>Defining the target region(s) for extraction and/or gRNA design</li>
                <li>Designing a gRNA library</li>
            </ol>
            </p>
            <p>
                <strong>1. Saving Genomes</strong><br>
                Genomes must be annotated in either Genbank or GFF3 format. GFF3 is preferable since any Genbank file
                will be transformed into a GFF3 for further processing. Genome files greater than 500Mb may experience problems.
                Please email the <a href="mailto:{{ admin_email }}">administrator</a>.
                if you need assistance. Files may be compressed for greater uploading speed (e.g. .zip, .gz). Saved genomes are private.
            </p>
            <p>
                You can download these sample files for E. coli k-12 MG1655<sup>*</sup> to use as examples for this tutorial:<br>
                <a href="{{ url_for('static', filename='files/GCF_003627195.1_ASM362719v1_genomic.fna') }}">GCF_003627195.1_ASM362719v1_genomic.fna</a>
                <br>
                <a href="{{ url_for('static', filename='files/GCF_003627195.1_ASM362719v1_genomic.gff') }}">GCF_003627195.1_ASM362719v1_genomic.gff</a>
                <br>
                <sup>*</sup><em><small>Blattner FR, Plunkett G 3rd, Bloch CA, Perna NT, Burland V, Riley M, Collado-Vides J, Glasner JD,
                Rode CK, Mayhew GF, Gregor J, Davis NW, Kirkpatrick HA, Goeden MA, Rose DJ, Mau B, Shao Y.
                The complete genome sequence of Escherichia coli K-12. Science. 1997 Sep 5;277(5331):1453-62.
                doi: 10.1126/science.277.5331.1453. PMID: 9278503.</small></em>
            </p>

            <p> <em>Steps:</em>
                <ol>
                    <li>To save a genome, click the link to "Your Genomes" at the top right navigation bar,
                    then click the "Save New Genome" button. All saved genomes are only accessible to your login and
                        will be maintained for future sessions on your account.</li>
                    <li>Next, give your genome a name. This is the name that will be used in other forms when selecting which
                    genome you want to target.</li>

                    <li>You must also specify whether your genome is a Prokaryote or a Eukaryote.
                        Currently these genomes are not treated differently, but they may be in the future.</li>

                    <li>Then select the genome sequence file. This will either be a FASTA or GenBank file.
                        If it is a FASTA file you will also be prompted to select a GFF file with the genome annotations.</li>
                </ol>
            </p>
            <p>
                Once this has been submitted, the genome will be pre-processed for later use; some of the more time
                consuming processing is done asynchronously. For small microbial genomes this can take less than a minute,
                larger eukaryotic genomes may take 10-15 minutes. To check whether the processing has completed,
                just click the "Your Genomes" from the top right to see a list of your genomes and their status.
                If a genome does not have the status "Complete", it will not yet be available for selection in other forms.
            </p>

            <p>
                <strong>2. Defining Target Regions</strong><br>
                <em>Steps:</em>
                <ol>
                    <li>Give the design a name that will be used for tracking purposes</li>
                    <li>Select the target genome for the list of your saved genomes</li>
                    <li>Define what genes/loci to target</li>
                    <li>Define the start and end points of the target regions</li>
                </ol>

            </p>
            <p>
                <em>Defining genes/loci</em>
                <ul>
                <li>Defined regions must be determined relative to features already annotated in the input file.
                    The names of available features (e.g. genes, loci ID, etc.) appear in a selection box enabling easy selection.
                    Once selected, click on the blue arrow to move them to the box on the left.
                    Alternatively you can cut-and-paste a list from a column of a text or excel file, or simply manually type in the names.

                </li>
                <li>
                The names will be validated, and the user warned if there is a misspelling, or a name is not present in the annotation.
                </li>
                <li>
                If this box is left empty, then it is assumed that you want to extract regions for all the genes in the genome.
                </li>
                </ul>
            </p>
            <p>
                <em>Defining start and end points</em><br>
                <ul>
                <li>Generally, you will know if the regions you are targeting for extraction are coding, non-coding,
                or a mixture of both, you must specify this to ensure that your sequence regions do not overlap regions
                    you do not want targeted.
                </li>
                    <ul>
                        <li>
                        For example, if you limit regions to non-coding regions only, and select a region that is upstream of a gene,
                        then the application will automatically truncate the sequence before the start of the next coding region upstream.
                        </li>
                        <li>
                        Likewise, if you limit the region to coding regions only, the application will ensure that your selected
                        region will not extend beyond the boundaries of the coding region.
                        </li>
                    </ul>
            <li>
                Next you must enter 4 parameters to define the region: a reference point and padding for both the start
                and end point of the region.
            </li>
                <ul>
                    <li>
                    A separate coordinate system is created for each point where the reference point is zero, and a negative number
                    in the padding indicates how many base pairs upstream are wanted, and a positive number indicates
                        how many base pairs are wanted downstream from the reference point.
                    <br>
                        <img height="150" src="{{ url_for('static', filename='img/refPointFig.png') }}" alt="coordinate system graphic -4 to +4 indicating upstream and downstream from zero">
                    </li>
                    <li>Depending on what features are annotated in the genome, the options for reference points will be:
                        <ul>
                            <li>the first or last base of the gene</li>
                            <li>the first or last base of the first CDS or exon of the gene</li>
                            <li>the first or last base of the last CDS or exon of the gene</li>
                        </ul>
                    </li>
                    <li>
                        The region will include both of the selected start and end points, so in the default example,
                        which shows the region 0-100 starting at the first base of the first CDS, the extracted region will
                        be 101 bases long, since it includes the base at “zero” in our coordinate system.
                    </li>
                    <li>
                        A dynamic cartoon will show what region you have
                        selected (in yellow), with an indicator of your selected reference points for both the
                        start and end of the target region.<br>
                        <img height="150" src="{{ url_for('static', filename='img/cartoon.png') }}" alt="cartoon of selected area overlayed ona gene model">
                    </li>
                    <li>An error is shown if you are violating your selected limitation of coding/noncoding regions.</li>
                </ul>
            </ul>

            <p>

            </p>

           <p>
                <strong>3. Designing gRNA Libraries</strong><br>
                Clicking "yes" to "Design gRNA for target regions?" will reveal the parameters needed for designing a gRNA library.
               <br>
               <em>Steps:</em>
               <ol>
                    <li>Select the PAM site</li>
                    <li>Enter the scaffold sequence
                        <ul>
                            <li>This is used to evaluate the predicted folding of the guide RNA when fused to the scaffold.
                            When scored, higher points are awarded to those that do not form hairpins or interfere with the folding of the scaffold</li>
                        </ul>
                    </li>
                    <li>Select the desired target site length</li>
                    <li>Select maximum mismatches in offsite targets</li>
                    <li>Enter the number of gRNAs wanted for each feature</li>
                    <li>Select what strand you want the gRNAs to be on, if desired</li>
                </ol>
            </p>

            <p><strong>Results</strong><br>
                If you are only extracting sequence regions, your results will be returned to you immediately,
                in the form of a FASTA file. Where each sequence id is the name of the gene or loci id the region was extracted relative to.
            </p>
            <p>
                Designing gRNA libraries will take much longer, depending on how many genes are being targeted.
                We suggest only selecting 2-3 genes when testing and getting familiar with the software.
            </p>
            <p>
                Clicking on "View Your Designs" from the top right navigation will display a list of all your designs and their status.
                When completed, you can review and download the results from the link in the list.
            </p>
            <p>
                The primary output from the gRNA library design process is a .csv file that has seven columns:
                <ul>
            <li>gene_gRNA name
                <ul><li>This will be the gene or loci ID and the gRNA name from CCTop, separated by an underscore</li></ul>
            </li>
                <li>four columns to indicate the location and orientation in the genome
                        <ul>
                            <li>chromosome</li>
                            <li>strand</li>
                            <li>start</li>
                            <li>end</li>
                        </ul>
                </li>
                <li>gRNA sequence</li>
                <li>PAM sequence</li>
                </ul>
            This information will also be displayed in a table at the bottom of the page.
            </p>
            <p>
                The FASTA file with the extracted sequence regions and all the files created during the design process,
            including the CCTop output and scoring logs are also available for download.

            </p>
            <p>
                If you would like to make your gRNA library design available to the public, you can use the "Make Publicly Available"
                button on the results page in the upper right corner. You will be asked to fill out some extra information
                describing the design, it's intended CRISPR application, and the target organism. Your name, or any other identifying information,
                will not be associated with the published design, which will be accessible without logging into the application.
                At any time you may revert the design to "private" and/or edit the descriptive information.
                To do this you must be logged into the application, then find your design in the "Public Designs" list.
                There will be an "Edit" button in the upper right corner of the design details page.

            </p>

		</di>

{% endblock %}