from gRNADesigner import db, login_manager, app
from flask_login import UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

class App(db.Model):
    __tablename__ = 'tbl_apps'

    app_id = db.Column(db.Integer, primary_key=True)
    app_name = db.Column(db.String(255))


    def __init__(self, app_name):
        self.app_name = app_name


    def __repr__(self):
        return '<id {}>'.format(self.app_id)


class Genome(db.Model):
    __tablename__ = 'tbl_genomes'

    genome_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('tbl_users.user_id'), nullable=False)
    genome_uuid = db.Column(db.String(255))
    genome_userdefined_id = db.Column(db.String(255))
    annotation_file_type = db.Column(db.String(25))
    annotation_file_name = db.Column(db.String(255))
    genome_status = db.Column(db.String(255))
    genome_type = db.Column(db.String(255))
    saved_date = db.Column(db.DateTime)
    user = db.relationship("User")

    def __init__(self, user, genome_uuid, genome_userdefined_id, annotation_file_type, annotation_file_name,genome_status, saved_date, genome_type):
        self.user = user
        self.genome_uuid = genome_uuid
        self.genome_userdefined_id = genome_userdefined_id
        self.annotation_file_type = annotation_file_type
        self.annotation_file_name = annotation_file_name
        self.genome_status = genome_status
        self.saved_date = saved_date
        self.genome_type = genome_type


    def __repr__(self):
        return '<id {}>'.format(self.user_id)


class User(UserMixin, db.Model):
    __tablename__ = 'tbl_users'

    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255))
    iswhitelisted = db.Column(db.Boolean)
    # new for local login
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))

    def __init__(self, email, username, password):
        self.email = email
        self.username = username
        self.password = password
        self.iswhitelisted = True


    def __repr__(self):
        return '<id {}>'.format(self.user_id)


    def get_id(self):
        return (self.user_id)


    def is_authenticated(self):
        return True


    def is_active(self):
        return True


    @login_manager.user_loader
    def load_user(id):
        #return db.session.execute(db.select(User).filter_by(user_id=id)).one()
        try:
            return User.query.filter(User.user_id == id).first()
        except:
            return None

    # for password reset
    def get_token(self, expire_sec=6000):
        serial=Serializer(app.config['SECRET_KEY'], expires_in=expire_sec)
        return serial.dumps({'user_id': self.user_id}).decode('utf-8')


    @staticmethod
    def verify_token(token):
        serial = Serializer(app.config['SECRET_KEY'])
        try:
            id = serial.loads(token)['user_id']
        except:
            return None
        return User.query.get(id)



class Job(db.Model):
    __tablename__ = 'tbl_user_jobs'

    job_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('tbl_users.user_id'), nullable=False)
    app_id = db.Column(db.Integer, db.ForeignKey('tbl_apps.app_id'), nullable=False)
    job_submission_date = db.Column(db.DateTime)
    job_end_date = db.Column(db.DateTime)
    job_type = db.Column(db.String(255))
    job_userdefined_id = db.Column(db.String(255))
    job_uuid = db.Column(db.String(255))
    job_status = db.Column(db.String(255))
    job_request_json_filename = db.Column(db.String(255))
    is_public = db.Column(db.Boolean)
    app = db.relationship("App")
    user = db.relationship("User")



    def __init__(self, user, app, job_type, job_name, uuid, status, start_time):
        self.user = user
        self.app = app
        self.job_submission_date = start_time
        self.job_type = job_type
        self.job_userdefined_id = job_name
        self.job_uuid = uuid
        self.job_status = status
        self.job_request_json_filename = uuid+".json"
        self.is_public= False


    def __repr__(self):
        return '<id {}>'.format(self.job_id)

class Design(db.Model):
    __tablename__ = 'tbl_designs'

    design_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('tbl_users.user_id'), nullable=False)
    edit_date = db.Column(db.DateTime)
    job_id = db.Column(db.Integer, db.ForeignKey('tbl_user_jobs.job_id'), nullable=False)
    job_uuid = db.Column(db.String(50))
    genome_uuid = db.Column(db.String(50))
    is_public = db.Column(db.Boolean)
    design_name = db.Column(db.String(150))
    taxon_id = db.Column(db.String(50))
    crispr_application = db.Column(db.String(255))
    description = db.Column(db.String(255))
    user = db.relationship("User")
    job = db.relationship("Job")



    def __init__(self, user_id, job_id, job_uuid, genome_uuid, taxon_id, description, design_name, crispr_application, edit_time):
        self.user_id = user_id
        self.job_id = job_id
        self.edit_date = edit_time
        self.job_uuid = job_uuid
        self.genome_uuid = genome_uuid
        self.taxon_id = taxon_id
        self.description = description
        self.design_name= design_name
        self.crispr_application = crispr_application


    def __repr__(self):
        return '<id {}>'.format(self.design_id)

class Login(db.Model):
    __tablename__ = 'tbl_user_logins'

    login_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('tbl_users.user_id'), nullable=False)
    app_id = db.Column(db.Integer, db.ForeignKey('tbl_apps.app_id'), nullable=False)
    login_time = db.Column(db.DateTime)
    app = db.relationship("App")
    user = db.relationship("User")

    def __init__(self, user_id, login_time):
        self.user_id = user_id
        self.app_id = 3
        self.login_time = login_time

    # TODO - method to update status, and end_time

    def __repr__(self):
        return '<id {}>'.format(self.login_id)