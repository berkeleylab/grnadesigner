$(document).ready(function() {

	// check if this ORCA instance is deployed at JGI
	if(is_jgi_grna()) {

		// if it is deployed at JGI, then check the
		// session and the user's JWT
		if(!(has_session() && has_jwt())) {

			// if there's no session or no JWT, then
			// redirect the user to the index.html
			redirect_user('/');
		}
	}
});

//-------------------------
//LOGOUT BUTTON
//-------------------------
/*$('#btnLogout').click(function() {
	logout_user();
	console.log("logout button clicked...");
});*/

function logout(sso_logout_url) {
	//alert("logging out");
	console.log("logging out user");

	if(has_session() && has_jwt()) {
         console.log("has session/cookie");
		// delete the ORCA JWT cookie
		destroy_jwt();

		// set the jgi_return  cookie
		destroy_session(sso_logout_url);

		//re-direct to index.html
		//redirect_user('/');
	}else{
		console.log("user has no session/cookie");
	}
}
//------------------------------------------------------------------------------------

//
function add_JWT_to_headers() {

	var token = get_jwt();
	var headers = {
        "GRNA-authorization": token
    };

	return headers;
}

function generate_ORCA_headers() {

    var token = get_jwt();
    var headers = {
        "Accept" : "application/json;charset=utf-8",
        "Content-Type": "application/json;charset=utf-8",
        "GRNA-authorization": token
    };

    //console.log(headers);

    return headers;
}