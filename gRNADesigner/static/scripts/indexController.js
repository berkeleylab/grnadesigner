 $(document).ready(function() {

	// check if this ORCA instance is deployed at JGI
	if(is_jgi_grna()) {

		// if so, then we have to do some session and JWT verification

		// check if a session exists
		if(has_session()) {

			// check if the user has a JWT
			if(has_jwt()) {
				redirect_user('/grna.html');

			} else {
				// get the user's JWT from the back-end
				authenticate_client();
			}
		}

	} else {

		// if this ORCA instance is deployed anywhere but at JGI,
		// then we let the user enter
		redirect_user('/grna.html');

	}

});

function authenticate_client() {

	var resource_path = '/auth';

	$.ajax({
		url     	: resource_path,
		type    	: 'POST',
		dataType	: "json",
		async   	: false,
		success 	:
		function(data, textStatus, xhr) {

			console.log(data);

			if('grna-jwt' in data) {

				set_jwt(data['grna-jwt']);

				// redirect
				redirect_user('/grna.html');
			}
		},

	    error:
	    function (jqXHR, textStatus, errorThrown) {
	    		console.log(errorThrown);
        }
	});
}


//-------------------------------------------------------
// Login Button
/*
$('#btnLogin').click(function() {

	// set the SSO 'jgi_return' cookie
	set_return_cookie();

	// forward the user to JGI SSO
	window.location.replace('https://signon.jgi.doe.gov/signon');
});*/

// TODO re-write login button handler as a function to remove hard coded paths
function login(sso_url, return_url, domain){
	// set the SSO 'jgi_return' cookie
	set_return_cookie(return_url, domain);

	// forward the user to JGI SSO
	window.location.replace(sso_url);
}

function set_return_cookie(return_url, domain) {
	$.cookie(
			'jgi_return', return_url,
			{
				path: '/',
				domain: domain
			});
}
//-------------------------------------------------------




//-------------------------------------------------------
// Sign-Up Button
/*
$('#btnSignUp').click(function () {
	window.open('https://contacts.jgi.doe.gov/registration/new');
});*/

function signup(registration_url){
	window.open(registration_url);
}
//-------------------------------------------------------


function generateHeaders() {
	var headers = {
        "Accept" : "application/json;charset=utf-8",
        "Content-Type": "application/json;charset=utf-8"
    };
	return headers;
}

// Error Handler
function reportError(msg) {
	alert('error: ' + msg);
}