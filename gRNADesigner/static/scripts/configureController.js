$(document).ready(function() {
	$.ajax({
		type	: 	'GET',
		url		: 	'../rest/configs',	
		success : 	function(response) {
			initialize_configs(response);
		},
		error	: 	function(response) {
			alert('error!');
		}
	});
});

function initialize_configs(response) {
	response_json = JSON.parse(response);

	//--------------------------------
	// PACBIO
	$('#pacbio_setup_path').val(response_json['pacbio']['setup-path']);
	$('#pacbio_execution_path').val(response_json['pacbio']['execution-path']);
	$('#pacbio_webapp_path').val(response_json['pacbio']['webapp-path']);
	$('#pacbio_analysis_url').val(response_json['pacbio']['analysis-url']);
	//--------------------------------

	//--------------------------------
	// PACBIO AND BINNING
	$('#pacbio_and_binning_setup_path').val(response_json['pacbio-and-binning']['setup-path']);
	$('#pacbio_and_binning_execution_path').val(response_json['pacbio-and-binning']['execution-path']);
	$('#pacbio_and_binning_webapp_path').val(response_json['pacbio-and-binning']['webapp-path']);
	$('#pacbio_and_binning_analysis_url').val(response_json['pacbio-and-binning']['analysis-url']);
	//--------------------------------

	//--------------------------------
	// MISEQ
	$('#miseq_setup_path').val(response_json['miseq']['setup-path']);
	$('#miseq_execution_path').val(response_json['miseq']['execution-path']);
	$('#miseq_webapp_path').val(response_json['miseq']['webapp-path']);
	$('#miseq_analysis_url').val(response_json['miseq']['analysis-url']);
	$('#miseq_bin_path').val(response_json['miseq']['bin-path']);
	//--------------------------------
}

//--------------------------------
// "SAVE" button
$('#btnSave').click(function() {
	
	// build the request JSON
	var request_json = {};
	
	var pacbio_configs = {};
	pacbio_configs['setup-path'] = $('#pacbio_setup_path').val();
	pacbio_configs['execution-path'] = $('#pacbio_execution_path').val();
	pacbio_configs['webapp-path'] = $('#pacbio_webapp_path').val();
	pacbio_configs['analysis-url'] = $('#pacbio_analysis_url').val();
	
	var pacbio_and_binning_configs = {};
	pacbio_and_binning_configs['setup-path'] = $('#pacbio_and_binning_setup_path').val();
	pacbio_and_binning_configs['execution-path'] = $('#pacbio_and_binning_execution_path').val();
	pacbio_and_binning_configs['webapp-path'] = $('#pacbio_and_binning_webapp_path').val();
	pacbio_and_binning_configs['analysis-url'] = $('#pacbio_and_binning_analysis_url').val();

	var miseq_configs = {};
	miseq_configs['setup-path'] = $('#miseq_setup_path').val();
	miseq_configs['execution-path'] = $('#miseq_execution_path').val();
	miseq_configs['webapp-path'] = $('#miseq_webapp_path').val();
	miseq_configs['analysis-url'] = $('#miseq_analysis_url').val();
	miseq_configs['bin-path'] = $('#miseq_bin_path').val();
	
	request_json['pacbio'] = pacbio_configs;
	request_json['pacbio-and-binning'] = pacbio_and_binning_configs;
	request_json['miseq'] = miseq_configs;

	submit_data(request_json);
});

//--------------------------------------------------
// send POST request to REST endpoint
//
function submit_data(request_json) {
	
	clear_message();
	
	$.ajax({
		type	: 	'POST',
		url		: 	'../rest/configure',	
		contentType: 'application/json',
		data	:	JSON.stringify(request_json),
		success : function(response) {
			
			var response_json = JSON.parse(response)

			if(response_json['status'] === 'ok') {
				
				show_message('Saved!', false);
				
			} else {
				
				// build the error message
				var error_message = '';
				$.each(response_json['error'], function( index, value ) {
					error_message += value + '<br/>';
				});
				
				show_message(error_message, true);
			}
		},
		error : function(response) {
			var response_json = JSON.parse(response.responseText);
			show_message(response_json['status-message'], true);
		}
	});
}
//--------------------------------------------------

//--------------------------------------------------
// ERROR MESSAGES
//
function show_message(message, is_error) {
	
	$('#div_message').html(message);

	if(is_error !== true) {
		$('#div_message').attr('class', 'alert alert-success');
		$('#div_message').show().delay(500).fadeOut();
	} else {
		$('#div_message').attr('class', 'alert alert-danger');
		$('#div_message').show();
	}
}

function clear_message() {
	$('#div_message').removeAttr('class');
	$('#div_message').html('');
	$('#div_message').hide();
}
//--------------------------------------------------
