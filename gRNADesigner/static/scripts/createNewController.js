$(document).ready(function() {
    // read features and fill in options for first genome in list, if exists
    var uuid = $('#select_genome').val();
    if (uuid !== 'undefined') {
        selectedGenome();
    }
    else{
        //TODO disable submit button
        $('#btn_submit').attr('disabled',true);

    }

});

// the global variables below are populated for each genome selectd,
// based on the contents of the features.json and genes.json files
var GENES = [];
var FEATURETYPES=[];
var FEATURES = {};

function validateGenes(){
    /* This function is triggered when the user changes the target_genes textarea,
     * it removes all commas, and puts each word on a separate line after validating
     * that the word is a gene or locus ID in the genome */

    console.log("validating genes...");
    //clear any previous gene errors
    clear_gene_errors()

    //get the genes entered and check each to
    // make sure they are in GENES list
    // else error and don't submit
    var genes = $('#target_genes').val();
    //strip out any commas
    genes = genes.replace (/,/g, "");
    //split on white space
    var g_list = genes.split(/\s+/);
    var bad_genes = "";
    var good_genes = "";
    g_list.forEach((entry) => {
        console.log(entry);
        if (GENES.indexOf(entry) < 0) {
            //gene not found - put in list of bad genes
            if (bad_genes === ""){
                bad_genes = entry;
            }else {
                bad_genes = bad_genes + ", " + entry;
            }
        }else{
            if (good_genes === ""){
                good_genes = entry;
            }else{
                good_genes = good_genes+"\n"+entry;
            }
        }
    });
    //replace contents with good genes only
    //this will also remove any commas if someone puts them in
    $('#target_genes').val(good_genes);

    if (bad_genes !== "") {
        // since I am fixing the input, don't disable the submit button
        // the genes are also checked again and duplicates removed after submission
        //make error message
        var error_msg = "These gene/loci IDs were removed because they are not annotated in the selected genome: " + bad_genes
        show_gene_errors(error_msg);
    }

}

function moveID(){
    /* This function copies the selected "available target IDs" into the target_genes text area */
    console.log("moving IDs...");
    //get which genes are selected
    var s = $('#avail_genes option:selected').toArray().map(item => item.value);
    console.log("selected: "+s);
    var good_genes= "";
    s.forEach((entry) => {
        console.log(entry);
        if (good_genes === "") {
            good_genes = entry;
        } else {
            good_genes = good_genes + "\n" + entry;
        }
    });
    //append to value to genes text area
    var existing = $('#target_genes').val();
    if (existing !== ""){
        good_genes = existing +"\n"+good_genes;
    }
    $('#target_genes').val(good_genes);

}


function selectedGenome() {
    /* This function is called when the user changes the genome selected.
    * It gets the features.json and genes.json files
    * and populated the defaults for the target selection
    * including the start and end point feature reference options and available genes,
    * and draws the cartoon and displays the default selected target region */

    console.log("checking genome selected...");
    //show spinner - this is hidden at the end of the populateAvailGenes() function
    $('.modal').modal('show');

    var uuid = $('#select_genome').val();
    console.log("uuid: "+uuid);
    var g_name = $('#select_genome :selected').text();
    console.log("g_name: "+g_name)
    //get features from file and create FEATURES dict
    var genome_dir=$('#select_genome').val();

    //set hidden input for genome_name
    $('#genome_name').val(g_name);

    //get the json for the features and genes for the selected genome
    console.log(genome_dir);
    var json_file = "genomes/"+UserName.replace("@", "__")+"/"+genome_dir+"/features.json";
    $.getJSON(json_file, function(data) {
        console.log("getting features.json");
        FEATURES = data.features;
        FEATURETYPES = data.featuretypes;

        if($('#gene_features').is(':checked')){
            console.log("gene_features selected...");
            create_cartoon("genes");
            //also populate the options for gene - if gene features is selected
            populateFeatureOptions(FEATURES);
        }

    }).fail(function(){
            console.log("An error has occurred reading the features.json file.");
    });

    //get genes list as GENES to be used for validating genes
    json_file = "genomes/"+UserName.replace("@", "__")+"/"+genome_dir+"/genes.json";
    $.getJSON(json_file, function(data) {
        console.log("getting genes.json");
        all_genes = data;
        GENES = data.genes;
        //console.log(GENES);
        populateAvailGenes();


    }).fail(function(){
            console.log("An error has occurred reading the genes.json file.");
            // TODO - give user feedback so they know they need to reload the geneome
            // TODO disable the submit button also...
    });


    $('#btn_submit').attr('disabled',false);
}

function populateAvailGenes(){
    /* This function is called wne a new genome is selected.
    * It populated the "available Target IDs" selection options with
    * the gene/locus IDs for the selected genome - based on the gene list in the genes.json file */

    console.log("populating available genes selection");
    //TODO clear target_genes text area
    $('#target_genes').val('');

    var $select = $('#avail_genes');
    //remove old options
    $select.find('option').remove();

    // for each gene in GENES list add a line to the avail_genes select options
    $.each(GENES, function(value) {
            var key_value = GENES[value];
            $('<option>').val(key_value).text(key_value).appendTo($select);
        });
    //hide spinner
    console.log("hide modal!");
    $('.modal').modal('hide');

}

/* The svg_features hash is used to validate the regions selected in the displayRegions() function,
 * If the cartoon drawing is changed, these numbers and those in the validation code must also be changed */
var svg_features={"gene_0":280, "gene_$":550, "CDS_F_0": 280, "exon_F_0": 280, "CDS_F_$": 330, "exon_F_$": 330,
                    "CDS_L_0":500, "exon_L_0": 500, "CDS_L_$":550, "exon_L_$": 550, "Custom_0": 380, "Custom_$": 460}

function selectedStartPoint(){
    /* This function is triggered when the user changes the selected start reference
    * The svg cartoon is updated, as is the displayed region (which is also validated),
    * and a hidden field with the human readable value of the selected start reference is also populated
    * It is used for the results display to remind the user what was selected. */

    // get the selected point text and set the value of the hidden start_name
    var s_name = $('#start_origin :selected').text();
    var s_value = $('#start_origin :selected').val();
    console.log("start selected: "+s_value);
    $('#start_name').val(s_name);

    var s_value = $('#start_origin :selected').val();
    var center_x = svg_features[s_value];
    var y = 50;
    var arrow_coords = [center_x-5,y, center_x+5, y, center_x, y+5];
    var mycolor="MediumSeaGreen";

    //change start coordinates over svg -
    // first remove the old one
    d3.select("#s_coordinate_line").remove();
    d3.select("#s_tick-0").remove();
    d3.select("#s_arrow1").remove();
    d3.select("#s_arrow2").remove();
    d3.select("#s_coordinate_text").remove();
    d3.select("#s_label_text").remove();

    //Then create new
    const svg = d3.select('#viz');
    var line= svg.append('line')
        .style("stroke", mycolor)
        .style("stroke-width", 2)
        .attr("id","s_coordinate_line")
        .attr("x1", center_x - 50)
        .attr("y1", y)
        .attr("x2", center_x + 50)
        .attr("y2", y);
    // tick 1
    svg.append('line')
        .style("stroke", mycolor)
        .style("stroke-width", 2)
        .attr("id","s_tick-0")
        .attr("x1", center_x )
        .attr("y1", y-5)
        .attr("x2", center_x )
        .attr("y2", y+15);
    // arrow at end of tick-0
    svg.append('line')
        .style("stroke", mycolor)
        .style("stroke-width", 2)
        .attr("id","s_arrow1")
        .attr("x1", center_x - 5)
        .attr("y1", y+5)
        .attr("x2", center_x )
        .attr("y2", y+15);
    // arrow at end of tick-0
    svg.append('line')
        .style("stroke", mycolor)
        .style("stroke-width", 2)
        .attr("id","s_arrow2")
        .attr("x1", center_x + 5)
        .attr("y1", y+5)
        .attr("x2", center_x )
        .attr("y2", y+15);
    //text
    svg.append('text')
        .style("stroke", mycolor)
        //.style("stroke-width", 1)
        .style("font-size", 10)
        .attr('id', 's_coordinate_text')
        .attr('text-anchor', 'middle')
        .attr("x", center_x)
        .attr("y", y-10)
        .text('...-2 -1 0 1 2 3...');
    // text above coordinate text
    svg.append('text')
        .style("stroke", mycolor)
        //.style("stroke-width", 1)
        .style("font-size", 10)
        .attr('id', 's_label_text')
        .attr('text-anchor', 'middle')
        .attr("x", center_x)
        .attr("y", y-25)
        .text('starting reference point');

    displayRegion();
}

function selectedEndPoint(){
    /* This function is triggered when the user changes the selected end reference
    * The svg cartoon is updated, as is the displayed region (which is also validated),
    * and a hidden field with the human readable value of the selected end reference is also populated
    * It is used for the results display to remind the user what was selected. */

    // get the selected point text and set the value of the hidden start_name
    var e_name = $('#end_origin :selected').text();
    console.log("end selected: "+e_name);
    $('#end_name').val(e_name);

    var e_value = $('#end_origin :selected').val();
    var center_x = svg_features[e_value];
    var y = 150;
    var arrow_coords = [center_x-5,y, center_x+5, y, center_x, y-5];
    var mycolor = "red";

    //change start coordinates over svg -
    // first remove the old one
    d3.select("#e_coordinate_line").remove();
    d3.select("#e_tick-0").remove();
    d3.select("#e_arrow1").remove();
    d3.select("#e_arrow2").remove();
    d3.select("#e_coordinate_text").remove();
    d3.select("#e_label_text").remove();

    //Then create new
    const svg = d3.select('#viz');
    var line= svg.append('line')
        .style("stroke", mycolor)
        .style("stroke-width", 2)
        .attr("id","e_coordinate_line")
        .attr("x1", center_x - 50)
        .attr("y1", y)
        .attr("x2", center_x + 50)
        .attr("y2", y);
    // tick 1
    svg.append('line')
        .style("stroke", mycolor)
        .style("stroke-width", 2)
        .attr("id","e_tick-0")
        .attr("x1", center_x )
        .attr("y1", y-15)
        .attr("x2", center_x )
        .attr("y2", y+5);
    // arrow at end of tick-0
    svg.append('line')
        .style("stroke", mycolor)
        .style("stroke-width", 2)
        .attr("id","e_arrow1")
        .attr("x1", center_x)
        .attr("y1", y-15)
        .attr("x2", center_x -5 )
        .attr("y2", y-5);
    // arrow at end of tick-0
    svg.append('line')
        .style("stroke", mycolor)
        .style("stroke-width", 2)
        .attr("id","e_arrow2")
        .attr("x1", center_x)
        .attr("y1", y-15)
        .attr("x2", center_x + 5 )
        .attr("y2", y - 5);
    //text
    svg.append('text')
        .style("stroke", mycolor)
        .style("font-size", 10)
        .attr('id', 'e_coordinate_text')
        .attr('text-anchor', 'middle')
        .attr("x", center_x)
        .attr("y", y+15)
        .text('...-2 -1 0 1 2 3...');
    // text above coordinate text
    svg.append('text')
        .style("stroke", mycolor)
        .style("font-size", 10)
        .attr('id', 'e_label_text')
        .attr('text-anchor', 'middle')
        .attr("x", center_x)
        .attr("y", y+30)
        .text('ending reference point');

    displayRegion();
}

function create_cartoon(feature_type){
    /* This function draws the svg cartoon appropriate to the genome annotations of the selected genome
     * The "magic" numbers used for placing the cartoon components are also used to validate the selected region
      * so if these are changed the nubers used in the validation done in the displayRegions() function
      * also must be changed. */

    //feature type will either be "genes" or "custom"
    //TODO first remove old contents
    const svg = d3.select('#viz');
    svg.selectAll("*").remove();

    // NOTE: all these "magic" numbers are important for the proper display of all the features
    // they are used for the placement of the svg drawing ans well as the validation of the selected region
    // changing the drawing may change the validation output

    if (feature_type == "genes"){

        //TODO - check the features available from GFF, draw different cartoons based on features available
        // If "CDS" features are available - draw the original cartoon (this is regardless of if there are genes or exons)(cartoon_type'= "CDS")
        // If no CDS, but exons are present - same cartoon, but adjust the polygons to be rectangles (cartoon_type'= "exon")
        // If only genes are available - same cartoon except merge the 2 CDS polygons into one and remove the "First" and "last" text(cartoon_type'= "gene")
        var cartoon_type = "CDS"; //this is the default
        if( FEATURETYPES.indexOf("CDS") > -1 ) {
            cartoon_type = "CDS";
        }else if(FEATURETYPES.indexOf("exon") > -1 ) {
            cartoon_type = "exon";
        }else if(FEATURETYPES.indexOf("gene") > -1 ) {
            cartoon_type = "gene";
        }

        //first dotted line
        svg.append('line')
            .style("stroke", "LightGray")
            .style("stroke-width", 3)
            .style("stroke-linecap","round")
            .style("stroke-dasharray","1, 8")
            .attr("x1", 20)
            .attr("y1", 100)
            .attr("x2", 80)
            .attr("y2", 100);
        //first dummy CDS
        if (cartoon_type == "exon"){
            //draw a rectangle if exons
            svg.append('rect')
            .attr('x', 80)
            .attr('y', 90)
            .attr('width', 50)
            .attr('height', 20)
            .attr('fill', 'LightGray');
        }else {
            //draw polygon for CDS r gene
            svg.append('polygon')
                .attr('points', '80 100, 90 90, 130 90, 130 110, 90 110')
                .style('fill', 'LightGray');
        }
        //solid line
        svg.append('line')
            .style("stroke", "Gray")
            .attr("x1", 130)
            .attr("y1", 100)
            .attr("x2", 280)
            .attr("y2", 100);
        //first coding start
        if(cartoon_type == "exon"){
            //draw a rectangle if exons
            svg.append('rect')
                .attr('x', 280)
                .attr('y', 90)
                .attr('width', 50)
                .attr('height', 20)
                .attr('fill', 'Gray');
            svg.append('text')
                .style("stroke", "black")
                .style("fill", "white")
                .style("stroke-width", ".1")
                .style("font-size", 12)
                .attr("x", 285)
                .attr("y", 105)
                .text('First');

        }else if(cartoon_type == "CDS") {
            //first CDS polygon: start x=280, end x=330
            svg.append('polygon')
                .attr('points', '280 90, 320 90, 330 100, 320 110, 280 110')
                .style('fill', 'Gray');
            svg.append('text')
                .style("stroke", "black")
                .style("fill", "white")
                .style("stroke-width", ".1")
                .style("font-size", 12)
                .attr("x", 285)
                .attr("y", 105)
                .text('First');
        }else{
            //for gene - create one big polygon- no text
            svg.append('polygon')
                .attr('points', '280 90, 540 90, 550 100, 540 110, 280 110')
                .style('fill', 'Gray');
        }
        if (cartoon_type != "gene") {
            //solid line:<line x1="320" y1="100" x2="380" y2="100" stroke="gray" />
            svg.append('line')
                .style("stroke", "Gray")
                .attr("x1", 320)
                .attr("y1", 100)
                .attr("x2", 380)
                .attr("y2", 100);
            //dotted line:<line x1="380" y1="100" x2="440" y2="100" stroke="gray"  stroke-width="3" stroke-linecap="round" stroke-dasharray="1, 8"/>
            svg.append('line')
                .style("stroke", "Gray")
                .style("stroke-width", 3)
                .style("stroke-linecap", "round")
                .style("stroke-dasharray", "1, 8")
                .attr("x1", 380)
                .attr("y1", 100)
                .attr("x2", 440)
                .attr("y2", 100);
            //solid line: <line x1="440" y1="100" x2="500" y2="100" stroke="gray" />
            svg.append('line')
                .style("stroke", "Gray")
                .attr("x1", 440)
                .attr("y1", 100)
                .attr("x2", 500)
                .attr("y2", 100);
        }
        //last coding region
        if (cartoon_type == "exon"){
            //draw a rectangle if exons
            svg.append('rect')
                .attr('x', 500)
                .attr('y', 90)
                .attr('width', 50)
                .attr('height', 20)
                .attr('fill', 'Gray');
            svg.append('text')
                .style("stroke", "black")
                .style("fill", "white")
                .style("stroke-width", ".1")
                .style("font-size", 12)
                .attr("x", 505)
                .attr("y", 105)
                .text('Last');

        }else if(cartoon_type == "CDS") {
            //<!-- last CDS polygon: start x=500, end x=550 -->
            //<polygon points="500 90, 540 90, 550 100, 540 110, 500 110" stroke="gray" fill="gray" stroke-width="3" />
            svg.append('polygon')
                .attr('points', '500 90, 540 90, 550 100, 540 110, 500 110')
                .style('fill', 'Gray');
            svg.append('text')
                .style("stroke", "black")
                .style("fill", "white")
                .style("stroke-width", ".1")
                .style("font-size", 12)
                .attr("x", 505)
                .attr("y", 105)
                .text('Last');
        }
        //solid line: <line x1="550" y1="100" x2="660" y2="100" stroke="gray" />
        svg.append('line')
            .style("stroke", "Gray")
            .attr("x1", 550)
            .attr("y1", 100)
            .attr("x2", 660)
            .attr("y2", 100);
        //last dummy CDS
        if (cartoon_type == "exon"){
            //draw a rectangle if exons
            svg.append('rect')
            .attr('x', 660)
            .attr('y', 90)
            .attr('width', 50)
            .attr('height', 20)
            .attr('fill', 'LightGray');
        }else {
            //draw polygon for CDS r gene
            //<polygon points="660 90, 700 90, 710 100, 700 110, 660 110" stroke="LightGray" fill="LightGray" stroke-width="3" />
            svg.append('polygon')
                .attr('points', '660 90, 700 90, 710 100, 700 110, 660 110')
                .style('fill', 'LightGray');
        }
        //dotted line: <line x1="710" y1="100" x2="780" y2="100" stroke="LightGray"  stroke-width="3" stroke-linecap="round" stroke-dasharray="1, 8"/>
        svg.append('line')
            .style("stroke", "LightGray")
            .style("stroke-width", 3)
            .style("stroke-linecap","round")
            .style("stroke-dasharray","1, 8")
            .attr("x1", 710)
            .attr("y1", 100)
            .attr("x2", 780)
            .attr("y2", 100);
        //<!-- text "ATG..-->
        // <text x="277" y="80" font-size="12" fill="black" stroke="black" stroke-width=".1">ATG...</text>
        svg.append('text')
            .style("stroke", "black")
            .style("fill", "black")
            .style("stroke-width", ".1")
            .style("font-size", 12)
            .attr("x", 277)
            .attr("y", 80)
            .text('ATG...');
        svg.append('text')
            .style("stroke", "black")
            .style("fill", "black")
            .style("stroke-width", ".1")
            .style("font-size", 10)
            .attr("x", 277)
            .attr("y", 125)
            .text('(START)');
        //<!-- text "...TAG-->
        //<text x="520" y="80" font-size="12" fill="black" stroke="black" stroke-width=".1">...TAG</text>
        svg.append('text')
            .style("stroke", "black")
            .style("fill", "black")
            .style("stroke-width", ".1")
            .style("font-size", 12)
            .attr("x", 520)
            .attr("y", 80)
            .text('...TAG');
        svg.append('text')
            .style("stroke", "black")
            .style("fill", "black")
            .style("stroke-width", ".1")
            .style("font-size", 10)
            .attr("x", 520)
            .attr("y", 125)
            .text('(STOP)');

    }else{
        //for custom features
        //first dotted line
        svg.append('line')
            .style("stroke", "LightGray")
            .style("stroke-width", 3)
            .style("stroke-linecap","round")
            .style("stroke-dasharray","1, 8")
            .attr("x1", 40)
            .attr("y1", 100)
            .attr("x2", 150)
            .attr("y2", 100);
        //first dummy CDS
        svg.append('polygon')
            .attr('points', '150 100, 160 90, 210 90, 210 110, 160 110, 150 100')
            .style('fill', 'LightGray');
        //solid line
        svg.append('line')
            .style("stroke", "Gray")
            .attr("x1", 210)
            .attr("y1", 100)
            .attr("x2", 380)
            .attr("y2", 100);
        //feature polygon: start x=380, end x=420
        svg.append('polygon')
            .attr('points', '380 90, 450 90, 460 100, 450 110, 380 110')
            .style('fill', 'Gray');
        svg.append('text')
            .style("stroke", "black")
            .style("fill", "white")
            .style("stroke-width", ".1")
            .style("font-size", 12)
            .attr("x", 385)
            .attr("y", 105)
            .text('Feature');
        svg.append('line')
            .style("stroke", "Gray")
            .attr("x1", 460)
            .attr("y1", 100)
            .attr("x2", 600)
            .attr("y2", 100);
        svg.append('polygon')
            .attr('points', '600 90, 650 90, 660 100, 650 110, 600 110')
            .style('fill', 'LightGray');
        svg.append('line')
            .style("stroke", "LightGray")
            .style("stroke-width", 3)
            .style("stroke-linecap","round")
            .style("stroke-dasharray","1, 8")
            .attr("x1", 660)
            .attr("y1", 100)
            .attr("x2", 750)
            .attr("y2", 100);

    }

}

function displayRegion(){
    /* This function displays the selected region on the cartoon,
    * It also validates the region with respect to the "limit" selected (coding, non-coding, or none),
    * and ensures that people select a logical start and end reference point with respect to each other
    * for example, the start must be before the end.
    * If there are issues, the error message is displayed and the submit button is disabled
    * Note - that much of the validation is based on "magic" numbers - these numbers are not magic,
    * they are the boundaries of the coding and non-coding areas of the svg cartoon drawing
    * and are used as a proxy for the actual regions that will be selected.
     * Therefore if the cartoon drawing is changed these numbers will also need to be changed */

    console.log("displaying select region...");
    var error = false;
    // remove any error messages
    clear_and_hide_errors();
    // first remove the old one
    d3.select("#region").remove();

    // get start point and padding
    var s_ref = $('#start_origin :selected').val();
    console.log("s_ref: "+svg_features[s_ref]);
    var s_pad = $('#start_padding').val();
    //get end_point and padding
    var e_ref = $('#end_origin :selected').val();
    console.log("e_ref: "+svg_features[e_ref]);
    var e_pad = $('#end_padding').val();

    //first check if either start or end padding is empty
    // force to zero if empty
    if (s_pad === ""){
        //force to zero
        s_pad = "0";
        $('#start_padding').val(s_pad);
    }
    if (e_pad === ""){
        //force to zero
        e_pad = "0";
        $('#end_padding').val(e_pad);
    }

    var s_x = 0;
    var e_x = 0;
    var offset = 20;
    if (parseInt(s_pad) == 0) {
        //if the padding = zero
        //set the s_x to the svg pixel from s_ref
        s_x = svg_features[s_ref];
    }else if(parseInt(s_pad) < 0){
        //special case if it is close to zero
        if (parseInt(s_pad) > -10){
           s_x = svg_features[s_ref] - 2;
        }else {
            s_x = svg_features[s_ref] - offset;
        }
    }else{
        //s_pad is > 0
        //special case if it is close to zero
        if (parseInt(s_pad) < 10){
           s_x = svg_features[s_ref] + 2;
        }else {
            s_x = svg_features[s_ref] + offset;
        }
    }

      if (parseInt(e_pad) == 0){
        //if the padding = zero
        //set the s_x to the svg pixel from s_ref
        e_x = svg_features[e_ref];
    }else if(parseInt(e_pad) < 0){
          //special case if close to zero
          if (parseInt(e_pad) > -10){
              e_x = svg_features[e_ref]-2;
          }else {
              e_x = svg_features[e_ref] - offset;
          }
    }else{
        //s_pad is > 0
          //special case if close to zero
          if (parseInt(e_pad) < 10){
              e_x = svg_features[e_ref] + 2;
          }else {
              e_x = svg_features[e_ref] + offset;
          }
    }

      console.log("s_x: "+s_x+", e_x: "+e_x);

    //if start ref are the same and both padding are neg or pos, need different padding_x to be separated visually
    if ( s_ref == e_ref ){
        if(s_x == e_x)  {
            if (parseInt(s_pad) > 0) {
                console.log("padding is positive")
                //if the padding is "+"
                if (parseInt(s_pad) < parseInt(e_pad)) {
                    console.log("s_pad < e_pad")
                    // s is before start - this is good
                    //subtract another offset from start x
                    e_x = e_x + offset;
                } else {
                    console.log("s_pad > e_pad")
                    //this is bad because start is less than end, so create an error message
                    //by subtracting the offset from e_x
                    e_x = e_x - offset;
                }
            }else{
                console.log("padding is negative")
                //if the padding is "-"
                if (parseInt(s_pad) < parseInt(e_pad)) {
                    console.log("s_pad < e_pad")
                    //s is before start - this is good
                    //subtract another offset from start x
                    s_x = s_x - offset;
                } else {
                    console.log("s_pad > e_pad")
                    //this is bad because start is less than end, so create an error message
                    //by subtracting the offset from e_x
                    e_x = e_x - offset;
                }

            }
        }
    }

    console.log("new s_x: "+s_x+", e_x: "+e_x);
      //check sanity - start must be before end
    if (e_x > s_x) {
        //this is what we want
        //draw box
        var w = e_x - s_x;
        const svg = d3.select('#viz');
        svg.append('rect')
            .attr("id", "region")
            .attr('x', s_x)
            .attr('y', 85)
            .attr('width', w)
            .attr('height', 30)
            .attr("fill-opacity","0.6")
            .attr('fill', 'yellow');


    }
    else{
        error = true;
        var  error_msg = "ERROR: The start of a region must be before the end region!";
      //not good show error message instead
        show_errors(error_msg);

    }
    // check selected  "limits"
    // only check the limits if feature_type is "gene_features" for custom features we don't know...
    var featureType = $('input[name="feature_type"]:checked').val();
    if (featureType == "gene_features"){
        //var limit = $('#limit_regions :selected').val();
        var limit = $('input[name="limit_regions"]:checked').val();
        console.log("limit: "+limit);
        if (limit == "coding"){
            //make sure that the area is within a coding region
            //check that the area is within the the first cds or exon
            if((s_x >= 280) && (e_x <= 330)){
                //this is good  no error
                console.log("coding only: region in the first CDS or exon");
            }else if((s_x >= 500) && (e_x <= 550)){
                console.log("coding only: region in the last CDS or exon");
            }else if((s_x >= 280) && (e_x <= 550)){
                //this is only good if there are only genes- no exons or CDS
                console.log("coding only: region in gene");
                if ((FEATURETYPES.indexOf("CDS") > -1) || (FEATURETYPES.indexOf("exon") > -1)){
                    //this means there are CDS and exons, so it is an error
                    error = true;
                    var  error_msg = "ERROR: The defined region includes non-coding regions!";
                    show_errors(error_msg);
                }
            }else{
               error = true;
               var  error_msg = "ERROR: The defined region includes non-coding regions!";
               show_errors(error_msg);
            }
        }else if (limit == "non-coding") {
            //make sure that the area is not within a non-coding region
            //check that the area is within the the first cds or exon
            if (e_x < 280) {
                console.log("non-coding only: region upstream of gene");
            } else if ((s_x >= 330) && (e_x <= 500)) {
                console.log("non-coding only: region between CDS/exons");
            } else if (s_x >= 550) {
                //this is only good if there are only genes- no exons or CDS
                console.log("non-coding only: region downstream of gene");
            } else {
                error = true;
                var error_msg = "ERROR: The defined region includes coding regions!";
                show_errors(error_msg);
            }
        }
    }
    //disable submit button if errors
    if (error){
        console.log("disabling submit button");
        $('#btn_submit').addClass("disabled");
        $('#btn_submit').attr("disabled", "disabled");
    }else{
        console.log("remove disable on submit button");
       $('#btn_submit').removeAttr("disabled");
       $('#btn_submit').removeClass("disabled");
    }

}


function populateFeatureOptions(features){
    /* This function populates the feature selection options from the list that was in the genomes features.json file
    * These options are based on the features annotated in each individual genome */
     console.log("populating feature options...");
        //fill in start origin options
        var $select = $('#start_origin');
        //remove old options
        $select.find('option').remove();

        //make new ones
        var first = true;
        $.each(features, function(value) {
            console.log(features[value]);
            var key_value = features[value];
            $('<option>').val(key_value[0]).text(key_value[1]).appendTo($select);
            if (first){
                $('#start_name').val(key_value[1]);
                first = false;
            }
        });

        //fill in end origin options
        var $select = $('#end_origin');
        //remove old options
        $select.find('option').remove();

        //make new ones
        first = true;
        $.each(features, function(value) {
            var key_value = features[value];
            $('<option>').val(key_value[0]).text(key_value[1]).appendTo($select);
            if (first){
                $('#end_name').val(key_value[1]);
                first = false;
            }
        });

        //now display default region selection in svg
        console.log("will set endpoints and display selections next..");
        selectedStartPoint();
        selectedEndPoint();
        displayRegion();

    }


function selectedFeatureOptions(type) {
    /* This function changes the display depending on what feature type the user has selected,
    * and draws the cartoon associated with selected type (genes or custom)*/
    create_cartoon(type);

    //check file extension - we will just test if it starts with a "g" or an "f"
    //at this point we assume that people are being smart
    if (type === 'genes'){
        console.log("gene options selected");
        $('#gene_feature_options').removeClass("d-none");
        //hide custom feature options
        $('#custom_feature_options').addClass("d-none");
        $('#custom_features').removeAttr('required', "");

        // fill in options for start_origin and end_origin
        populateFeatureOptions(FEATURES);
    }
    else if(type === 'custom'){
        console.log("custom options selected");
        $('#custom_feature_options').removeClass("d-none");
        $('#custom_features').attr('required', "");
        //hide gene feature options
        $('#gene_feature_options').addClass("d-none");

        //make new ones
        origins = [["Custom_0", "First Base of Custom Feature"], ["Custom_$", "Last Base of Custom Feature"]];
        populateFeatureOptions(origins);

    }

}

function design_gRNA(option) {
    /* This function displays or hides the gRNA design form */
    //clear error message - in case they got one prior to this change
    //clear_and_hide_errors();

    //check file extension - we will just test if it starts with a "g" or an "f"
    //at this point we assume that people are being smart
    if (option === 'yes'){
        console.log("yes - design gRNA ");
        $('#gRNA_options').removeClass("d-none");
    }
    else if(option === 'no'){
        console.log("no= don't design gRNA");
        $('#gRNA_options').addClass("d-none")
    }

}


function show_errors(errors_text) {
    /* This function shows the error message generated by the display_regions() function
    *  when the regions are either improperly selected or violate the limits selected */
	$('#div_message').attr('class', 'alert alert-danger');

	$('#div_message').html(errors_text);
	$('#div_message').removeClass('d-none');
}

function clear_and_hide_errors() {
    /* This hides the error message generated by the display_regions() function
    *  when the regions are either improperly selected or violate the limits selected */
	$('#div_message').html();
	$('#div_message').addClass('d-none');
}

function show_gene_errors(errors_text) {
    /* This function shows the warnings when generated when target gene/loci IDs are entered that are not IDs in the genome */
	$('#gene_message').attr('class', 'alert alert-warning');

	$('#gene_message').html(errors_text);
	$('#gene_message').removeClass('d-none');
}

function clear_gene_errors() {
    /* This function hides the warnings generated when target gene/loci IDs are entered that are not IDs in the genome */
	$('#gene_message').html();
	$('#gene_message').addClass('d-none');
}





