//------------------------------------------------------------------------------------
//JGI SSO "SESSION" COOKIE
function has_session() {
	var test = ($.cookie('jgi_session') !== undefined);
	console.log("has session? "+test);
	return ($.cookie('jgi_session') !== undefined);
}

function get_session() {
	return $.cookie('jgi_session');
}

function destroy_session(sso_logout_url, return_url, domain) {
	// set the return cookie
	set_return_cookie();

	// redirect the user to the signon destroy page
    //TODO - put hard coded paths in config
	redirect_user(sso_logout_url);
}

function set_return_cookie(return_url, domain) {
    //TODO - put hard coded paths in config
	$.cookie(
			'jgi_return', return_url,
			{
				path: '/',
				domain: domain
			});
}
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
//gRNA COOKIE -- JWT
function has_jwt() {
	var test = ($.cookie('grna-jwt') !== undefined);
	console.log("has_jwt? "+test)
	return ($.cookie('grna-jwt') !== undefined);
}

function set_jwt(jwt) {
	// set the JWT cookie
	set_cookie('grna-jwt', jwt);
}

function get_jwt() {
	return $.cookie('grna-jwt');
}

function destroy_jwt() {
	if(has_jwt()) {
		destroy_cookie('grna-jwt');
	}
}
//------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------
// gRNA - DEPLOYMENT LOCATION
function is_jgi_grna() {

	let return_value = false;

	// get the host-name from the back-end
	$.ajax({
		url : '/is_jgi_grna',
		type : 'GET',
		async : false,
		success : function(data, textStatus, jqXHR) {

			console.log('is_jgi_grna');
			console.log(data);

			if('is_jgi_grna' in data) {
				return_value = data['is_jgi_grna'];

				// TEST --> python boolean (True/False) to JS boolean (true/false)
				console.log('return_value: ' + return_value);
				if(return_value === false) {
					console.log('NO JGI gRNA');
				} else {
					console.log('JGI gRNA');
				}

				// if this ORCA instance is not deployed at JGI,
				// then we get a dummy JWT as a response as well,
				// which we need to store as a cookie
				if('dummy-jwt' in data) {
					set_jwt(data['dummy-jwt']);
				}

			} else {
				return_value = false;
			}
		},

		error : function(jqXHR, textStatus, errorThrown) {
			return_value = false;
		}
	});

	return return_value;
}

function redirect_user(site) {
	window.location.replace(site);
}

//------------------------------------------------------------------------------------
// COOKIE MANAGEMENT
function has_cookie(cookie_name) {
	return ($.cookie(cookie_name) !== undefined);
}

function set_cookie(cookie_name, cookie_value) {
	console.log('set_cookie(' + cookie_name + ', ' + cookie_value + ')');
	$.cookie(cookie_name, cookie_value);
}

function destroy_cookie(cookie_name) {
	$.removeCookie(cookie_name, { path: '/' });
}
//------------------------------------------------------------------------------------


//----------------------------
// Generate Unique ID for user's submission
//----------------------------
function generateUUID () { // Public Domain/MIT
	var d = Date.now();
	if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
	    d += performance.now();
	}
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
	    var r = (d + Math.random() * 16) % 16 | 0;
	    d = Math.floor(d / 16);
	    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
	});
}

