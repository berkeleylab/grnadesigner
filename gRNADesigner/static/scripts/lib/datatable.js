$(document).ready(function() {

	var selected = [];
	
    $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/get_table",
            "type": "POST"
        },
        "columnDefs": [ 
        	{
            "orderable": false,
            "className": 'select-checkbox',
            "targets":   0
        }],
        "searching" : false,
        "select": true,
        "ordering": false,
        "rowCallback": function( row, data ) {
        	
        		var is_in_array = $.inArray(data.DT_RowId, selected);
        	    console.log('data.DT_RowId: ' + data.DT_RowId + ' in [' + selected + '] --> ' + is_in_array);
        	    
            if ( is_in_array !== -1 ) {
            		$(row).addClass('selected');
            }
        }
    } );
	
    $('#example tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );

});