
/* check file extensions on genome sequence upload,
 * the acceptable formats are : fasta (.fa, .fas, .fasta) and Genbank (.gb, .gbk)
 * and they can be compressed with zip (.Zip) or gunzip (.gz)
 *
 * This function will only look at the extensions, later the back end will validate
 * the file formats by looking at the file contents.
 *
 * This function is merely used to know if we need a second annotation file, if a fasta is uploaded
 *
 * */
function need_annotation_file(filename) {
    //clear error message - in case they got one prior to this change
    clear_and_hide_errors();
    //make sure save butten starts disabled
    $('#save_genome_btn').prop('disabled', true);

    //set yes to false- if they uploaded something other than a genbank or fasts, will want to stay the same, and show an error message
	var yes = false;
	console.log("checking sequence file format: "+filename);
	// split the filename on dots
    var parts = filename.split(".");
    //find file extenstion
    var etx = "";
    var end = parts[parts.length -1];
    if (end.endsWith("zip") || end.endsWith("gz") ){
        // this is compressed, so look at the next token
        var ext = parts[parts.length -2];

    }else{
        ext = end;
    }
    console.log("ext: " +ext);
    //check file extension - we will just test if it starts with a "g" or an "f"
    //at this point we assume that people are being smart
    if (ext.startsWith("fa") || ext.startsWith("fna")){
        // this will be a fasta file, and we need an annotation file
        //display the annotation file input
        console.log("fasta file uploaded..");
        $('#upload_annotation').removeClass("d-none");
        $('#genome_annotation').attr('required', "");

    } else if (ext.startsWith("gb")){
        //we don't need a separate annotation file for a Genbank file
        // do nothing
        //or hide annotation input
        $('#upload_annotation').addClass("d-none");
        $('#genome_annotation').removeAttr('required', "");
        // can activate the save button
        $('#save_genome_btn').prop('disabled', false);
    }else{
        //they uploaded something wrong
        //display error message
        show_errors("The sequence file must be either a Genbank or Fasta file. These can be compressed with zip or gz.")

    }

}

/*
This functions checks if the annotation file is a gff variant,
and will activate the save button - or send a error message
 */
function check_annotation_file(filename) {
    //clear error message - in case they got one prior to this change
    clear_and_hide_errors();

	console.log("checking annotation file format: "+filename);
	// split the filename on dots
    var parts = filename.split(".");
    //find file extenstion
    var etx = "";
    var end = parts[parts.length -1];
    if (end.endsWith("zip") || end.endsWith("gz") ){
        // this is compressed, so look at the next token
        var ext = parts[parts.length -2];

    }else{
        ext = end;
    }
    //check file extension - we will just test if it starts with a "gff"
    //at this point we assume that people are being smart
    if (ext.startsWith("gff")){
        // this will be a gff or gff3 file, and we need an annotation file
        //display the annotation file input
        console.log("fasta file uploaded..");
        $('#upload_annotation').removeClass("d-none");
        $('#genome_annotation').attr('required', "");
        // enable save button
        $('#save_genome_btn').prop('disabled', false);

    }else{
        //they uploaded something wrong
        //disable save button
        $('#save_genome_btn').prop('disabled', true);
        //display error message
        show_errors("The annotation file must be either a GFF or GFF3 file. These can be compressed with zip or gz.")

    }

}

/*
Hide the genome selection form and display the save new genome form
 */
function show_upload_form() {
    //make the genome selection form invisible
    $('#div_select_genome').addClass('d-none');
    //make the upload new genome form visable
    $('#div_upload_genome').removeClass("d-none");

}

/*
Hide the save genome form and display the select genome form
 */
function show_select_form() {
    //make the genome selection form visible
    $('#div_select_genome').removeClass('d-none');
    //make the upload new genome form invisable
    $('#div_upload_genome').addClass("d-none");

}

function show_errors(errors_text) {
	$('#div_message').attr('class', 'alert alert-danger');

	$('#div_message').html(errors_text);
	$('#div_message').removeClass('d-none');
}

function clear_and_hide_errors() {
	$('#div_message').html();
	$('#div_message').addClass('d-none');
}

function onSubmit(){
    $('.modal').modal('show');
}