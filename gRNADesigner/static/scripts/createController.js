// --------------------------------------
// UI LISTENERS
			
$(document).ready(function() {

	/*$('#div_enter_data').show();

		$('#div_pacbio_libraries').show();
		$('#div_miseq_uploads').hide();
	
		$('#div_barcodes').hide();
	
	$('#div_review_data').hide();
	$('#div_execute').hide();*/

	//------------------------------------------------------------
	// set the current date in the "name of analysis" input field
	var date_and_time = new Date();
	// -- day
	var day = date_and_time.getDate();
	if(day < 10) {
		day = '0' + day;
	}
	// -- month
	var month = (date_and_time.getMonth() + 1);
	if(month < 10) {
		month = '0' + month;
	}
	// -- year
	var year = date_and_time.getFullYear();
	// do the formatting
	var formatted_current_date = 'P' + year + month + day;
	// set the value of the input field
	$('#name_of_run').val(formatted_current_date);
	//------------------------------------------------------------

});

$('#analysis_type').change(function() {
	var selected_type = $('#analysis_type').val();
	
	if(selected_type === 'miseq') {
		// MISEQ
		$('#div_miseq_uploads').show();

		$('#div_pacbio_libraries').hide();
		$('#div_reference_sequences').hide();
		$('#div_parameters').hide();
		$('#div_barcodes').hide();

	} else if(selected_type === 'pacbio_and_binning') {
		// PACBIO + BINNING
		$('#div_barcodes').show();
		
		$('#div_reference_sequences').hide();
	
	} else {
		
		// standard PACBIO
		$('#div_pacbio_libraries').show();
		$('#div_parameters').show();
		$('#div_reference_sequences').show();
		
		$('#div_barcodes').hide();
		$('#div_miseq_uploads').hide();		
	}
	
	clear_message();
	clear_and_hide_sanitize_errors();

});



// --------------------------------------


var review_data = {};

// $('#btnSubmit').click(function() {
//
// 	clear_message();
// 	//clear_and_hide_sanitize_errors();
// 	//clear_analysis_name();
// 	//clear_overview_table();
// 	//clear_config_xml();
// 	//console.log("submitted new design...");
// 	var request_json = {};
//
// 	// get form values and put in json
// 	request_json['design_name'] = $('#design_name').val();
// 	request_json['region_start'] = $('#region_start').val();
// 	request_json['region_end'] = $('#region_end').val();
// 	request_json['number_wanted'] = $('#number_wanted').val();
// 	request_json['PAM'] = $('#PAM').val();
// 	request_json['scaffold_seq'] = $('#scaffold_seq').val();
// 	request_json['target_length'] = $('#target_length').val();
// 	request_json['number_MM'] = $('#number_MM').val();
// 	request_json['email'] = $('#email').val();
//
// 	//validate all genes or gene list
// 	var all_genes = $('#cb_all_genes').val();
// 	var gene_names = $('#genes_names').val();
//
//
// 	//var analysis_type = $('#analysis_type').val();
// 	//request_json['analysis-type'] = analysis_type;
//
// 	//request_json['analysis-name'] = $('#name_of_run').val();
//
// 	//var mapping_file = undefined;
// 	//var zip_file = undefined;
// 	//var barcode_file = undefined;
// 	// if('pacbio' === analysis_type || 'pacbio_denovo' == analysis_type) {
// 	// 	mapping_file = $('#input_mapping_file')[0].files[0];
// 	// 	request_json['reference-sequences'] = $('#reference_sequences').val();
// 	//
// 	// 	// the min. length of the subreads
// 	// 	request_json['min-length'] = $('#min_length').val();
// 	//
// 	// } else if ('pacbio_and_binning' === analysis_type) {
// 	// 	mapping_file = $('#input_mapping_file')[0].files[0];
// 	// 	barcode_file = $('#input_barcodes_file')[0].files[0];
// 	// } else if ('miseq' === analysis_type) {
// 	// 	zip_file = $('#input_miseq_zip_file')[0].files[0];
// 	// }
// 	var genome_file = undefined;
// 	genome_file = $('#genome')[0].files[0];
//
//
//
// 	//----------------------------------------------
// 	// put the entered information into a FormData
// 	var formData = new FormData();
//
// 	// -- FILES
// 	if(genome_file !== undefined) {
// 		formData.append('genome_file', genome_file);
// 	}
//
// 	// -- JSON information
// 	formData.append('json', JSON.stringify(request_json));
// 	//----------------------------------------------
//
// 	// clear some previous preview data
// 	review_data = {};
//
// 	// submit the request to the back-end
// 	$.ajax({
// 		type		: 	'POST',
// 		//url			: 	'../rest/create_pipeline',
// 		url			: 	'../rest/ack',
//
// //		contentType	: 	'application/json; charset=utf-8',
// //		data		:	JSON.stringify(request_json),
//
// 		contentType	:	false,
// 		processData	:	false,
// 		data		:	formData,
//
// 		success : function(response) {
//
// 			var response_json = JSON.parse(response)
//
// 			if(response_json['status'] === 'ok') {
// 				alert('yes!');
//
// 				/*$('#div_enter_data').hide();
// 				$('#div_review_data').show();
// 				$('#div_execute').hide();
//
// 				// ANALYSIS TYPE
// 				// literal ... the human-readable version of the type
// 				if('analysis-type' in response_json && 'analysis-type-literal' in response_json) {
// 					visualize_analysis_type(response_json['analysis-type-literal']);
// 					review_data['analysis-type'] = response_json['analysis-type'];
// 					review_data['analysis-type-literal'] = response_json['analysis-type-literal'];
// 				}
//
// 				// ANALYSIS NAME
// 				if('analysis-name' in response_json) {
// 					visualize_analysis_name(response_json['analysis-name']);
// 					review_data['analysis-name'] = response_json['analysis-name'];
// 				}*/
//
// 				/*// Reference sequences
// 				if('reference-sequences' in response_json &&
// 					'reference-sequences-ids' in response_json) {
//
// 					review_data['reference-sequences'] = response_json['reference-sequences'];
// 					review_data['reference-sequences-ids'] = response_json['reference-sequences-ids'];
//
//  					if ('mapping' in response_json) {
//
//  						var overview_table_headers = undefined;
// //
// // 						console.log(response_json['mapping'])
// //
//  						// visualize the mapping table
//  						if('pacbio' === response_json['analysis-type'] || 'pacbio_and_binning' === response_json['analysis-type']) {
//
//  							// convert the received data for visualization
//  							converted_data = [];
//  							$.each(response_json['mapping'], function(key, value) {
//
//  								var library_info = response_json['mapping'][key];
//
//  								let array = ['', '', ''];
//  								array[0] = library_info[0];
//  								array[1] = library_info[1];
//  								array[2] = key;
//
//  								converted_data.push(array);
//  							});
//
//  							console.log(converted_data);
//
//  							visualize_mapping_table(['Pool', 'HAR#', 'Library'], converted_data,
//  									response_json['reference-sequence-ids']);
//
//  						} else if('miseq' === response_json['analysis-type']) {
//
//  							visualize_mapping_table(['Library', 'LIMS ID', 'Index'], response_json['mapping'],
//  									response_json['reference-sequence-ids']);
//  						}
// 						review_data['mapping'] = response_json['mapping'];
//
//
//  						// visualize the overview (reference-sequences/pool)
// 						overview_table_headers = get_values_of_arrays_at_index(response_json['mapping'], 0);
// 						visualize_overview_table(overview_table_headers, response_json['reference-sequences-ids']);
//
//  					} else {
//  						hide_mapping_div();
//  					}
//
// 				}*/
//
// 				/*if('min-length' in response_json) {
// 					visualize_min_length(response_json['min-length']);
// 					review_data['min-length'] = response_json['min-length'];
// 				}*/
//
// 				if('email' in response_json) {
// 					visualize_emails(response_json['email']);
// 					review_data['email'] = response_json['email'];
// 				}
//
//
//
// 			} else if(response_json['status'] === 'reference sequence issues') {
//
// 				show_sanitize_errors(response_json['sanitize-results']);
//
// 			} else if(response_json['status'] == 'not-ready') {
//
// 				alert('analysis is not ready!');
//
// 			} else {
//
// 				$('#div_enter_data').show();
// 				$('#div_review_data').hide();
// 				$('#div_execute').hide();
//
// 				show_message(JSON.parse(response_json['error']), true);
// 			}
// 		},
// 		error : function(jqXHR, textStatus, errorThrown) {
// 			show_message(jqXHR['responseText'], true);
// 		}
// 	});
// });

function get_pool_names(mappings) {
	var libraries = [];
	$.each(mappings, function(index, value) {
		libraries.push(value[0]);
	});
	return libraries;
}


function get_values_of_arrays_at_index(array_of_arrays, index) {
	var values = [];
	$.each(array_of_arrays, function(i, array) {
		if(index >=0 && index < array.length) {
			values.push(array[index])
		}
	});
	return values;
}

//------------------------------------------
// OUTCOMES OF SANITIZATION PROCESS
function show_sanitize_errors(errors_json) {
	$('#div_sanitize_errors').attr('class', 'alert alert-danger');
	
	$('#div_sanitize_errors').html(JSON.stringify(errors_json));
	$('#div_sanitize_errors').show();
}

function clear_and_hide_sanitize_errors() {
	$('#div_sanitize_errors').html();
	$('#div_sanitize_errors').hide();
}
//------------------------------------------

//--------------------------------------------------
// ANALYSIS NAME
function visualize_analysis_name(analysis_name) {
	var html = generate_header('Analysis Name') + '<h4 style="text-align:center">' + analysis_name + '</h4>';
	
	$('#div_analysis_name').html(html);
	$('#div_analysis_name').show();
}

function clear_analysis_name() {
	$('#div_analysis_name').html();
	$('#div_analysis_name').hide();
}
//--------------------------------------------------


//--------------------------------------------------
// LIBRARY NAMES
function visualize_libraries(libraries) {
	var html = generate_header('Library Names') + '<h4 style="text-align:center">';
	$.each(libraries, function(index, value) {
		html += value;
		if(index < libraries.length - 1) {
			html += ', ';
		}
	});
	html += '</h4>';

	$('#div_libraries').html(html);
	$('#div_libraries').show();

}

function clear_libraries() {
	$('#div_libraries').html();
	$('#div_libraries').hide();
}
//--------------------------------------------------

//--------------------------------------------------
// IDs of REFERENCE SEQUENCES
function visualize_sequences(reference_sequences) {
	var html = generate_header('Reference Sequences ID') + '<h4 style="text-align:center">';
	
	$.each(reference_sequences, function(index, value) {
		html += value;
		if(index < reference_sequences.length - 1) {
			html += ', ';
		}
	});
	html += '</h4>';
	
	$('#div_sequences').html(html);
	$('#div_sequences').show();

}

function clear_sequences() {
	$('#div_sequences').html();
	$('#div_sequences').hide();
}
//--------------------------------------------------

//--------------------------------------------------
// ANALYSIS TYPE
function visualize_analysis_type(analysis_type) {
	var html = generate_header('Type of Analysis') + '<h4 style="text-align:center">' + analysis_type + '</h4>';
	$('#div_analysis_type').html(html);
	$('#div_analysis_type').show();
}

function clear_analysis_name() {
	$('#div_analysis_type').html();
	$('#div_analysis_type').hide();
}
//--------------------------------------------------

//--------------------------------------------------
// ANALYSIS DATA PATH
function visualize_analysis_data_path(analysis_data_path) {
	$('#div_analysis_data_path').html('<h4 style="text-align:center"><strong>Path of analysis data:</strong>&nbsp;' + analysis_data_path + '</h4>');
	$('#div_analysis_data_path').show();
}

function clear_analysis_data_path() {
	$('#div_analysis_data_path').html();
	$('#div_analysis_data_path').hide();
}
//--------------------------------------------------

//--------------------------------------------------
// ANALYSIS WEBAPP PATH
function visualize_analysis_webapp_path(analysis_webapp_path) {
	$('#div_analysis_webapp_path').html('<h4 style="text-align:center"><strong>Path of analysis webapp:</strong>&nbsp;' + analysis_webapp_path + '</h4>');
	$('#div_analysis_webapp_path').show();
}

function clear_analysis_webapp_path() {
	$('#div_analysis_webapp_path').html();
	$('#div_analysis_webapp_path').hide();
}
//--------------------------------------------------

//--------------------------------------------------
// ANALYSIS URL
function visualize_analysis_url(analysis_url) {
	$('#div_analysis_url').html('<h4 style="text-align:center"><strong>URL of analysis results:</strong>&nbsp;' + analysis_url + '</h4>');
	$('#div_analysis_url').show();
}

function clear_analysis_url() {
	$('#div_analysis_url').html();
	$('#div_analysis_url').hide();
}
//--------------------------------------------------

//--------------------------------------------------
// MIN. LENGTH
function visualize_min_length(min_length) {
	var html = generate_header('Min. Length') + '<h4 style="text-align:center">' + min_length + '</h4>';

	$('#div_min_length').html(html);
	$('#div_min_length').show();
}

function clear_min_length() {
	$('#div_min_length').html();
	$('#div_min_length').hide();
}
//--------------------------------------------------

//--------------------------------------------------
// E-MAILs
function visualize_emails(emails) {
	var html = generate_header('E-Mail Addresses') + '<h4 style="text-align:center">';
	
	$.each(emails, function(index, value) {
		html += value; 
		if(index < emails.length - 1) {
			html += ', ';
		}
	});
	html += '</h4>';
	
	$('#div_emails').html(html);
	$('#div_emails').show();
}

function clear_emails() {
	$('#div_emails').html();
	$('#div_emails').hide();
}
//--------------------------------------------------

//--------------------------------------------------
// MAPPING TABLE
function hide_mapping_div() {
	$('#div_mapping').hide();
}

function visualize_mapping_table(headers, mappings, sequence_ids) {

	// -- generate table header
	var mapping_table_html = generate_header('Pool - HAR# - Library') + 
		'<table id="table_mapping" class="tablesorter" style="text-align:center;">';
	
	var table_header = '<tr>';
	$.each(headers, function(index, header) {
		table_header += '<th style="text-align:center;">' + header + '</th>';
	});
	table_header += '</tr>';
	
	
	var table_body = '';
	$.each(mappings, function(index, value) {
		table_body += '<tr>';
		$.each(value, function(i, v) {
			table_body += '<td style="text-align:center;">' + v + '</td>';
		})
		table_body += '</tr>';
	});
	
	mapping_table_html += 
		'<thead>'			+
			table_header 	+ 
		'</thead>' 			+
		'<tbody>' 			+ 
			table_body 		+ 
		'</tbody></table>';


	$('#div_mapping').html(mapping_table_html);
	$('#div_mapping').show();
	
	$('#table_mapping').tablesorter();
}
//--------------------------------------------------

//--------------------------------------------------
// OVERVIEW TABLE
function visualize_overview_table(libraries, sequences) {
	
	// -- generate table header
	var overview_table_html = generate_header('Reference Sequences - Pools') + 
		'<table id="table_overview" class="tablesorter" style="text-align:center;">';
	
	var table_header = '<tr>';
	table_header += '<th style="text-align:center;">Reference Sequence ID</th>';
	var nr_of_libraries = libraries.length;
	$.each(libraries, function(index, value) {
		table_header += '<th style="text-align:center;">' + value + '</th>';
	});
	table_header += '</tr>';
	
	var table_body = '';
	$.each(sequences, function(index, value) {
		table_body += '<tr>';
		table_body += '<td style="text-align:center;">' + value + '</td>';
		
		// radio-buttons for each reference sequence in each library
		for (i = 0; i < nr_of_libraries; i++) { 
			table_body += '<td>' + '<input type="checkbox" id="cbox_' + value + '_' + libraries[i] + '" value="" checked></td>';
		}
		
		table_body += '</tr>';
	});
	overview_table_html += 
		'<thead>'			+
			table_header 	+ 
		'</thead>' 			+
		'<tbody>' 			+ 
			table_body 		+ 
		'</tbody></table>';

	
	$('#div_overview_table').html(overview_table_html);
	$('#div_overview_table').show();
	
	$('#table_overview').tablesorter();
}

function clear_overview_table() {
	$('#div_overview_table').html();
	$('#div_overview_table').hide();
}
//--------------------------------------------------


//--------------------------------------------------
// ERROR MESSAGES
function show_message(message, is_error) {
	if(is_error !== true) {
		$('#div_message').attr('class', 'alert alert-success');
	} else {
		$('#div_message').attr('class', 'alert alert-danger');
	}
	
	$('#div_message').html(message);
	$('#div_message').show();
}

function clear_message() {
	$('#div_message').removeAttr('class');
	$('#div_message').html('');
	$('#div_message').hide();
}
//--------------------------------------------------

//--------------------------------------------------
// START the pipeline
$('#btnStart').click(function() {
	
	$('#div_enter_data').hide();
	$('#div_review_data').hide();
	$('#div_execute').show();

	
	var request_json = {};
	request_json['analysis-type'] = review_data['analysis-type'];
	request_json['analysis-name'] = review_data['analysis-name'];
	if('libraries' in review_data) {
		request_json['libraries'] = review_data['libraries'];
	} 
	if('mapping' in review_data) {
		request_json['mapping'] = review_data['mapping'];
	}
	
	
	request_json['reference-sequences'] = review_data['reference-sequences'];
	request_json['reference-sequences-ids'] = review_data['reference-sequences-ids'];
	request_json['min-length'] = review_data['min-length'];
	request_json['emails'] = review_data['emails'];

	$.ajax({
		type		: 	'POST',
		url			: 	'../rest/start_pipeline',
		//url			: '../rest/ack',
		contentType	: 	'application/json',
		data		:	JSON.stringify(request_json),
		success : function(response) {
			
			var response_json = JSON.parse(response)

			if(response_json['status'] === 'ok') {
				
				// the reference sequences
				if('reference-sequences' in response_json) {
					visualize_execution_reference_sequences(response_json['reference-sequences']);
				}

				// the config.xml
				if('config-xml' in response_json) {
					visualize_config_xml(response_json['config-xml']);
				}
				
				// the execution protocol
				if('execution-protocol' in response_json) {
					visualize_execution_protocol(response_json['execution-protocol']);
				}
				
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			show_message(jqXHR['responseText'], true);
		}
	});
});
//--------------------------------------------------

//--------------------------------------------------
//CONFIG.XML
function visualize_config_xml(config_xml) {
	
	var html = 
		'<div id="div_config_xml" class="form-group">' +
			generate_header('config.xml') +
			'<textarea id="config_xml" class="form-control" rows="20">' + config_xml + '</textarea>' +
		'</div>';
	
	$('#div_config_xml').html(html);
	$('#div_config_xml').show();
}

function clear_config_xml() {
	$('#div_config_xml').html();
	$('#div_config_xml').hide();
}
//--------------------------------------------------

//--------------------------------------------------
//EXECUTION PROTOCOL
function visualize_execution_protocol(execution_protocol) {
	
	var html = 
		'<div id="div_reference_sequences" class="form-group">' +
			generate_header('Execution Protocol') + 
			'<textarea id="execution_protocol" class="form-control" rows="20">' + execution_protocol + '</textarea>' +
		'</div>';
	
	$('#div_execution_protocol').html(html);
	$('#div_execution_protocol').show();
}

function clear_execution_protocol() {
	$('#div_execution_protocol').html();
	$('#div_execution_protocol').hide();
}
//--------------------------------------------------

//--------------------------------------------------
// REFERENCE SEQUENCES
function visualize_execution_reference_sequences(reference_sequences) {

	var html = 
		'<div id="div_execute_reference_sequences" class="form-group">' +
			generate_header('Reference Sequences') + 
			'<textarea id="execution_reference_sequences" class="form-control" rows="20">' + reference_sequences + '</textarea>' +
		'</div>';
	
	$('#div_execution_reference_sequences').html(html);
	$('#div_execution_reference_sequences').show();
}

function clear_execution_reference_sequences() {
	$('#div_execution_reference_sequences').html();
	$('#div_execution_reference_sequences').hide();
}
//--------------------------------------------------

//--------------------------------------------------
// CANCEL
$('#btnCancel').click(function() {
	window.location.replace('create.html');
});
$('#btnCancelExecution').click(function() {
	window.location.replace('create.html');
});
//--------------------------------------------------

$('#btnStartExecution').click(function() {
	alert('starting the pipeline...');
	
	// TODO:
	// submit analysis name to back-end which keeps track
	// of running pipelines
	
	window.location.replace('results.html');
});


function generate_header(header) {
	var header =  
		'<hr>' +
		'<h4 style="text-align:center">' + header + '</h4>';
	return header;
}
