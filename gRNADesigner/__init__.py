from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mail import Mail
import logging

app = Flask(__name__)

# for logging
formatter = "[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s"
logging.basicConfig(filename='grna.log', level=logging.DEBUG, format=formatter)

# below reads the settings in the config.py one directory up
app.config.from_object('config')

login_manager = LoginManager(app)
login_manager.login_view = 'login'

# to send reset password email and token set up flask-mail
mail = Mail(app)

# set database in app
db = SQLAlchemy(app)

# Set the directory where files will be uploaded
UPLOAD_FOLDER = './uploads/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Set the directory where genomes will be saved
GENOME_FOLDER = './genomes/'
app.config['GENOME_FOLDER'] = GENOME_FOLDER

from gRNADesigner import views, routes, models