from gRNADesigner import app
from flask import render_template, send_from_directory, jsonify
import json, os


@app.route('/favicon.ico')
def favicon():
    return send_from_directory('static', 'images/favicon.ico', mimetype='images/favicon.ico')

@app.route('/', defaults={'path': 'index.html'})
@app.route('/<path:path>')
def navigate_to(path):
    print('rendering ', path)
    # This is for reading genome json files
    if path.startswith('genomes') and path.endswith(".json"):
        parts = path.split("/")
        # example: /genomes/username/uuid/file.json
        # TODO- make sure that there are 4 parts
        requested_file = os.path.join(app.config['GENOME_FOLDER'], parts[1].replace('@', '__'), parts[2], parts[3])
        # open file as json, and return the json
        with open(requested_file) as f:
            data = json.load(f)
        return jsonify(data)
    else:
        return render_template(path)


